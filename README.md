# BlocFX API

### Before start:

1. You need to create .env file:
```
# Debug
DEBUG='server:*,router:*'

# Server
PORT=3000
BASE_URL = https://jigzo-sandbox.ett-global.com/api

# Backoffice
API_KEY =
API_SECRET =
```
