/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/client/apply.client.js":
/*!************************************!*\
  !*** ./src/client/apply.client.js ***!
  \************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.applyClient = void 0;\n\nvar _axios = _interopRequireDefault(__webpack_require__(/*! axios */ \"axios\"));\n\nvar _helpers = __webpack_require__(/*! ../helpers */ \"./src/helpers/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// @ts-check\n// Core\n// Instruments\nconst applyClient = _axios.default.create({\n  baseURL: (0, _helpers.getEnv)('APPLY_API_URL')\n});\n\nexports.applyClient = applyClient;\napplyClient.interceptors.response.use(response => {\n  const {\n    data\n  } = response;\n\n  if ((data === null || data === void 0 ? void 0 : data.status) === 'FAIL') {\n    return Promise.reject(new _helpers.APIError((data === null || data === void 0 ? void 0 : data.comment) || _helpers.errorMessages.applyError));\n  }\n\n  return response;\n}, error => {\n  var _data$error;\n\n  const {\n    data,\n    status\n  } = error.response;\n  return Promise.reject(new _helpers.APIError((data === null || data === void 0 ? void 0 : (_data$error = data.error) === null || _data$error === void 0 ? void 0 : _data$error.message) || _helpers.errorMessages.applyError, status));\n});\n\n//# sourceURL=webpack://blocfx-backend/./src/client/apply.client.js?");

/***/ }),

/***/ "./src/client/backOffice.client.js":
/*!*****************************************!*\
  !*** ./src/client/backOffice.client.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _request = _interopRequireDefault(__webpack_require__(/*! request */ \"request\"));\n\nvar _helpers = __webpack_require__(/*! ../helpers */ \"./src/helpers/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nclass BackOfficeClient {\n  constructor() {\n    this.API_URL = (0, _helpers.getEnv)('BACK_OFFICE_URL');\n    this.login = (0, _helpers.getEnv)('BACK_OFFICE_USERNAME');\n    this.password = (0, _helpers.getEnv)('BACK_OFFICE_PASSWORD');\n    this.Cookie = null;\n    this.CookieDate = null;\n    this._loadingSession = null;\n  }\n\n  getSession = async () => {\n    if (!this._loadingSession) {\n      if (this.CookieDate && this.CookieDate + 300000 >= Date.now()) {\n        this._loadingSession = Promise.resolve(this.Cookie);\n      } else {\n        this._loadingSession = new Promise((resolve, reject) => {\n          const sessionOptions = {\n            method: 'POST',\n            url: `${this.API_URL}/shared/login_check.php`,\n            headers: {\n              Cookie: this.Cookie || ''\n            },\n            formData: {\n              loginName: this.login,\n              password: this.password,\n              code: '',\n              type: 'B'\n            }\n          };\n          (0, _request.default)(sessionOptions, (errorSession, responseSession) => {\n            if (errorSession) reject(errorSession);\n            this.Cookie = responseSession.headers['set-cookie'] || responseSession.request.originalCookieHeader;\n\n            if (this.Cookie) {\n              this.CookieDate = Date.now();\n              resolve(this.Cookie);\n            } else reject();\n          });\n        });\n      }\n    }\n\n    const data = await this._loadingSession;\n    this._loadingSession = null;\n    return data;\n  };\n  send = async (options = {}) => {\n    const Cookie = await this.getSession();\n    const payload = {\n      method: 'GET',\n      ...options,\n      url: `${this.API_URL}${options.url}`,\n      headers: {\n        Cookie,\n        ...(options.headers || {})\n      }\n    };\n    return await new Promise((resolve, reject) => {\n      (0, _request.default)(payload, (error, data) => {\n        if (error) reject(error);\n        resolve(data);\n      });\n    });\n  };\n}\n\nvar _default = BackOfficeClient;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/client/backOffice.client.js?");

/***/ }),

/***/ "./src/client/clickSend.client.js":
/*!****************************************!*\
  !*** ./src/client/clickSend.client.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.clickSendClient = void 0;\n\nvar _axios = _interopRequireDefault(__webpack_require__(/*! axios */ \"axios\"));\n\nvar _helpers = __webpack_require__(/*! ../helpers */ \"./src/helpers/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// @ts-check\n// Core\n// Instruments\nconst basicToken = Buffer.from(`${(0, _helpers.getEnv)('CLICKSEND_USERNAIM')}:${(0, _helpers.getEnv)('CLICKSEND_API_KEY')}`).toString('base64');\n\nconst clickSendClient = _axios.default.create({\n  baseURL: (0, _helpers.getEnv)('CLICKSEND_API_URL'),\n  headers: {\n    Accept: 'application/json',\n    'Content-Type': 'application/json',\n    Authorization: `Basic ${basicToken}`\n  }\n});\n\nexports.clickSendClient = clickSendClient;\n\n//# sourceURL=webpack://blocfx-backend/./src/client/clickSend.client.js?");

/***/ }),

/***/ "./src/client/currencyCloud.client.js":
/*!********************************************!*\
  !*** ./src/client/currencyCloud.client.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.currencyCloudClient = void 0;\n\nvar _axios = _interopRequireDefault(__webpack_require__(/*! axios */ \"axios\"));\n\nvar _helpers = __webpack_require__(/*! ../helpers */ \"./src/helpers/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// @ts-check\n// Core\n// Instruments\nconst currencyCloudClient = _axios.default.create({\n  baseURL: (0, _helpers.getEnv)('CURRENCY_CLOUD_API_URL'),\n  headers: {\n    Accept: 'application/json',\n    'Content-Type': 'application/json'\n  }\n});\n\nexports.currencyCloudClient = currencyCloudClient;\n\n//# sourceURL=webpack://blocfx-backend/./src/client/currencyCloud.client.js?");

/***/ }),

/***/ "./src/client/ett.client.js":
/*!**********************************!*\
  !*** ./src/client/ett.client.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.ettClientAuthToken = exports.ettClient = void 0;\n\nvar _axios = _interopRequireDefault(__webpack_require__(/*! axios */ \"axios\"));\n\nvar _helpers = __webpack_require__(/*! ../helpers */ \"./src/helpers/index.js\");\n\nvar _auth = _interopRequireDefault(__webpack_require__(/*! ../core/auth */ \"./src/core/auth/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// Core\n// Instruments\n// eslint-disable-next-line import/no-cycle\nconst ettClientDefaultOptions = {\n  baseURL: (0, _helpers.getEnv)('BASE_API_URL'),\n  headers: {\n    Accept: 'application/json',\n    'Content-Type': 'application/json'\n  }\n};\n\nconst interceptorsDefaultResponse = response => {\n  var _response$data;\n\n  // Status Code isn't a success code - throw error\n  if (((_response$data = response.data) === null || _response$data === void 0 ? void 0 : _response$data.statusCode) !== 0) {\n    var _response$data2;\n\n    throw new _helpers.APIError((_response$data2 = response.data) === null || _response$data2 === void 0 ? void 0 : _response$data2.errorMessage, 400);\n  }\n\n  return response.data;\n};\n\nconst ettClient = _axios.default.create(ettClientDefaultOptions);\n\nexports.ettClient = ettClient;\nettClient.interceptors.response.use(interceptorsDefaultResponse);\n\nconst ettClientAuthToken = (req, handleApiError = true) => {\n  const authEttClient = _axios.default.create({ ...ettClientDefaultOptions,\n    headers: { ...ettClientDefaultOptions.headers,\n      Authorization: `Basic ${req.ettToken}`\n    }\n  });\n\n  authEttClient.interceptors.response.use(async response => {\n    const {\n      token,\n      ...data\n    } = response === null || response === void 0 ? void 0 : response.data;\n\n    if (token && req.ettUserTokenId) {\n      data.authToken = await _auth.default.refreshEttToken('token', req.ettUserTokenId, req.customer);\n    }\n\n    if (handleApiError) {\n      interceptorsDefaultResponse(response);\n    }\n\n    return data;\n  });\n  return authEttClient;\n};\n\nexports.ettClientAuthToken = ettClientAuthToken;\n\n//# sourceURL=webpack://blocfx-backend/./src/client/ett.client.js?");

/***/ }),

/***/ "./src/client/shuftipro.client.js":
/*!****************************************!*\
  !*** ./src/client/shuftipro.client.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.validateSignature = exports.shuftiproClient = void 0;\n\nvar _axios = _interopRequireDefault(__webpack_require__(/*! axios */ \"axios\"));\n\nvar _crypto = _interopRequireDefault(__webpack_require__(/*! crypto */ \"crypto\"));\n\nvar _helpers = __webpack_require__(/*! ../helpers */ \"./src/helpers/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// @ts-check\n// Core\n// Instruments\nconst token = Buffer.from(`${(0, _helpers.getEnv)('SHUFTIPRO_CLIENT_ID')}:${(0, _helpers.getEnv)('SHUFTIPRO_SECRET_KEY')}`).toString('base64');\n\nconst shuftiproClient = _axios.default.create({\n  baseURL: (0, _helpers.getEnv)('SHUFTIPRO_API_URL'),\n  headers: {\n    Accept: 'application/json',\n    'Content-Type': 'application/json',\n    Authorization: `Basic ${token}`\n  }\n});\n\nexports.shuftiproClient = shuftiproClient;\n\nconst validateSignature = ({\n  data,\n  signature\n}) => {\n  const stringData = JSON.stringify(data).replace(/\\//g, '\\\\/');\n\n  const hash = _crypto.default.createHash('sha256').update(`${stringData}${(0, _helpers.getEnv)('SHUFTIPRO_SECRET_KEY')}`).digest('hex');\n\n  return hash === signature;\n};\n\nexports.validateSignature = validateSignature;\nshuftiproClient.interceptors.response.use(response => {\n  const {\n    data,\n    headers\n  } = response;\n\n  if (!validateSignature({\n    data,\n    signature: headers === null || headers === void 0 ? void 0 : headers.signature\n  })) {\n    var _response$data, _response$data$error;\n\n    throw new _helpers.APIError(((_response$data = response.data) === null || _response$data === void 0 ? void 0 : (_response$data$error = _response$data.error) === null || _response$data$error === void 0 ? void 0 : _response$data$error.message) || _helpers.errorMessages.shuftiProSignature, 400);\n  }\n\n  return response;\n}, error => {\n  var _data$error;\n\n  const {\n    data,\n    status\n  } = error.response;\n  return Promise.reject(new _helpers.APIError((data === null || data === void 0 ? void 0 : (_data$error = data.error) === null || _data$error === void 0 ? void 0 : _data$error.message) || 'ShuftiPro response error', status));\n});\n\n//# sourceURL=webpack://blocfx-backend/./src/client/shuftipro.client.js?");

/***/ }),

/***/ "./src/client/zenDesk.client.js":
/*!**************************************!*\
  !*** ./src/client/zenDesk.client.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _axios = _interopRequireDefault(__webpack_require__(/*! axios */ \"axios\"));\n\nvar _helpers = __webpack_require__(/*! ../helpers */ \"./src/helpers/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst zenDeskClient = _axios.default.create({\n  baseURL: (0, _helpers.getEnv)('ZENDESK_API_URL')\n});\n\nvar _default = zenDeskClient;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/client/zenDesk.client.js?");

/***/ }),

/***/ "./src/controllers/accounts/index.js":
/*!*******************************************!*\
  !*** ./src/controllers/accounts/index.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _ett = __webpack_require__(/*! ../../client/ett.client */ \"./src/client/ett.client.js\");\n\nvar _response = __webpack_require__(/*! ../../helpers/response */ \"./src/helpers/response/index.js\");\n\nvar _models = __webpack_require__(/*! ../../models */ \"./src/models/index.js\");\n\nvar _regexp = _interopRequireDefault(__webpack_require__(/*! ../../helpers/regexp */ \"./src/helpers/regexp/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// import apply from '../../core/apply';\nconst ETT_URL = 'account';\nconst paymentAccountController = {\n  index: async (req, res) => {\n    // const { accounts, ...responseData } = await ettClientAuthToken(req).get(ETT_URL);\n    // const accountsNumber = accounts.map(({ accountNumber }) => accountNumber);\n    // const paymentAccounts = await PaymentAccount.find({\n    //   accountNumber: {\n    //     $in: accountsNumber,\n    //   },\n    // }).lean();\n    // const mergeAccounts = accounts.map((item) => {\n    //   const paymentAccount = paymentAccounts.find(({ accountNumber }) => accountNumber === item.accountNumber);\n    //   const name = paymentAccount?.name || item?.name || '';\n    //   return { ...paymentAccount, ...item, name };\n    // });\n    // return successResponse(res, {\n    //   data: mergeAccounts,\n    //   ...responseData,\n    // });\n    const paymentAccounts = await _models.PaymentAccount.find();\n    return (0, _response.successResponse)(res, {\n      data: paymentAccounts,\n      total: paymentAccounts.length\n    });\n  },\n  show: async (req, res) => {\n    const {\n      accounts\n    } = await (0, _ett.ettClientAuthToken)(req).get(`${ETT_URL}/${req.params.accNumber}`);\n    const ettAcc = (accounts === null || accounts === void 0 ? void 0 : accounts[0]) || {};\n    let paymentAccount = {};\n\n    if (ettAcc.accountNumber) {\n      paymentAccount = (await _models.PaymentAccount.findOne({\n        accountNumber: ettAcc.accountNumber\n      }).lean()) || {};\n    }\n\n    return (0, _response.successResponse)(res, {\n      data: { ...paymentAccount,\n        ...ettAcc\n      }\n    });\n  },\n  create: async (req, res) => {\n    const {\n      country = '',\n      currency = '',\n      // name = '',\n      bic = '',\n      sortCode = '',\n      aba = '',\n      bankAddress = '',\n      accountType = '',\n      bankName = ''\n    } = req.body;\n    let {\n      iban = '',\n      accountNumber = ''\n    } = req.body;\n    const isInvalidBank = (0, _response.bankValidator)(req, res);\n    if (isInvalidBank) return false;\n\n    if (!iban && accountNumber && _regexp.default.iban.test(accountNumber)) {\n      iban = accountNumber;\n      accountNumber = accountNumber.substr(14);\n    } else if (accountNumber.length < 7 && iban) {\n      const accountNumberIntoIban = iban.substr(14);\n\n      if (/^[0-9]{7,24}$/.test(accountNumberIntoIban)) {\n        accountNumber = accountNumberIntoIban;\n      }\n    } // *** APPLY VALIDATOR IBAN AND ACCOUNT_NUMBER *** //\n    // const dataApply = await apply.verification(country, {\n    //   accountNumber: iban || accountNumber,\n    //   sortCode,\n    //   bic,\n    // });\n    // if (dataApply.status !== 'PASS') {\n    //   throw new Error('bank data is invalid');\n    // }\n    // *** APPLY VALIDATOR IBAN AND ACCOUNT_NUMBER *** //\n    // const ettAccountRes = await ettClientAuthToken(req).post(ETT_URL, {\n    //   currency,\n    //   name: name || bankName,\n    //   accountNumber: iban || accountNumber,\n    // });\n    // *** payload Data format *** //\n\n\n    const funding = {\n      country\n    }; // ! ONLY INTERNATIONAL\n\n    if (bic) funding.bic = bic;\n    if (iban) funding.iban = iban; // ! ONLY LOCAL\n    // *** local validation *** //\n\n    const isLikelyLocal = country === currency.substr(0, 2);\n    const isLikelySortCode = isLikelyLocal && ['GB'].includes(country);\n    const isLikelyABA = isLikelyLocal && ['US'].includes(country);\n\n    const currentSortCode = _regexp.default.sortCode.test(sortCode);\n\n    const currentAba = _regexp.default.aba.test(aba); // *** local validation *** //\n\n\n    if (accountNumber) funding.accountNumber = accountNumber;\n    if (isLikelySortCode && currentSortCode && sortCode) funding.sortCode = sortCode;\n    if (isLikelyABA && currentAba && aba) funding.aba = aba;\n    if (accountType && isLikelyLocal) funding.accountType = accountType; // OTHER BANK DATA\n\n    if (bankName) funding.bankName = bankName;\n    if (bankAddress) funding.bankAddress = bankAddress; // *** payload Data format *** //\n\n    const paymentAccount = await _models.PaymentAccount.create({\n      reference: req.customer.reference,\n      accountNumber: 'FAKE',\n      // accountNumber: ettAccountRes.accountNumber,\n      funding,\n      currency\n    });\n    return (0, _response.successResponse)(res, {\n      data: paymentAccount\n    });\n  },\n  update: async (request, response) => {\n    const {\n      accountNumber\n    } = request.params;\n    const {\n      name\n    } = request.body;\n    const {\n      accounts\n    } = await (0, _ett.ettClientAuthToken)(request).get(`${ETT_URL}/${accountNumber}`);\n    const newAccountData = { ...accounts,\n      name\n    };\n    const account = await _models.PaymentAccount.findOneAndUpdate({\n      accountNumber\n    }, newAccountData, {\n      upsert: true\n    });\n    return response.json({\n      account\n    });\n  }\n};\nvar _default = paymentAccountController;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/accounts/index.js?");

/***/ }),

/***/ "./src/controllers/apply/index.js":
/*!****************************************!*\
  !*** ./src/controllers/apply/index.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _response = __webpack_require__(/*! ../../helpers/response */ \"./src/helpers/response/index.js\");\n\nvar _apply = _interopRequireDefault(__webpack_require__(/*! ../../core/apply */ \"./src/core/apply/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar _default = {\n  findBankDetails: async (req, res) => {\n    const {\n      countryCode,\n      bankName,\n      pageSize,\n      address,\n      bic8\n    } = req.query;\n    const data = await _apply.default.findBankDetails(countryCode, bankName, address, bic8, pageSize);\n    return (0, _response.successResponse)(res, {\n      data\n    });\n  },\n  code: async (req, res) => {\n    const {\n      countryCode,\n      nationalId,\n      accountNumber\n    } = req.query;\n    const data = await _apply.default.code(countryCode, nationalId, accountNumber);\n    return (0, _response.successResponse)(res, {\n      data\n    });\n  }\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/apply/index.js?");

/***/ }),

/***/ "./src/controllers/auth/index.js":
/*!***************************************!*\
  !*** ./src/controllers/auth/index.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _promises = _interopRequireDefault(__webpack_require__(/*! fs/promises */ \"fs/promises\"));\n\nvar _path = _interopRequireDefault(__webpack_require__(/*! path */ \"path\"));\n\nvar _process = _interopRequireDefault(__webpack_require__(/*! process */ \"process\"));\n\nvar _auth = _interopRequireDefault(__webpack_require__(/*! ../../core/auth */ \"./src/core/auth/index.js\"));\n\nvar _response = __webpack_require__(/*! ../../helpers/response */ \"./src/helpers/response/index.js\");\n\nvar _ett = __webpack_require__(/*! ../../client/ett.client */ \"./src/client/ett.client.js\");\n\nvar _shuftiPro = _interopRequireDefault(__webpack_require__(/*! ../../core/shuftiPro */ \"./src/core/shuftiPro/index.js\"));\n\nvar _config = __webpack_require__(/*! ../../helpers/config */ \"./src/helpers/config/index.js\");\n\nvar _utils = __webpack_require__(/*! ../../helpers/utils */ \"./src/helpers/utils/index.js\");\n\nvar _models = __webpack_require__(/*! ../../models */ \"./src/models/index.js\");\n\nvar _upload = _interopRequireDefault(__webpack_require__(/*! ../../helpers/upload */ \"./src/helpers/upload/index.js\"));\n\nvar _regexp = _interopRequireDefault(__webpack_require__(/*! ../../helpers/regexp */ \"./src/helpers/regexp/index.js\"));\n\nvar _clickSend = _interopRequireDefault(__webpack_require__(/*! ../../core/clickSend */ \"./src/core/clickSend/index.js\"));\n\nvar _zenDesk = _interopRequireDefault(__webpack_require__(/*! ../../services/zenDesk */ \"./src/services/zenDesk.js\"));\n\nvar _generator = __webpack_require__(/*! ../../helpers/generator */ \"./src/helpers/generator/index.js\");\n\nvar _helpers = __webpack_require__(/*! ../../helpers */ \"./src/helpers/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* eslint-disable no-plusplus */\n\n/* eslint-disable no-restricted-globals */\n\n/* eslint-disable no-return-assign */\n\n/* eslint-disable no-param-reassign */\n\n/* eslint-disable radix */\nconst zenDeskService = new _zenDesk.default();\nconst shuftiPro = new _shuftiPro.default();\nvar _default = {\n  login: async (request, response) => {\n    const {\n      login,\n      password\n    } = request.body;\n\n    try {\n      const authToken = await _auth.default.login({\n        login,\n        password\n      });\n      return (0, _response.successResponse)(response, {\n        authToken\n      });\n    } catch (error) {\n      if (error.status === 400) {\n        throw new Error('ETT is no connect!');\n      } else {\n        throw error;\n      }\n    }\n  },\n  logout: async (req, res) => {\n    await _auth.default.logout(req.ettUserTokenId);\n    return (0, _response.successResponse)(res, true);\n  },\n  me: async (req, res) => {\n    const {\n      user,\n      ...other\n    } = await (0, _ett.ettClientAuthToken)(req).get('/user/me');\n    return (0, _response.successResponse)(res, {\n      data: user,\n      ...other\n    });\n  },\n  customer: async (request, response) => {\n    const {\n      customer\n    } = request;\n    return (0, _response.successResponse)(response, {\n      data: customer\n    });\n  },\n  passwordReset: async (req, res) => {\n    const encodeMail = encodeURIComponent(req.body.email);\n    const responseData = await _ett.ettClient.get(`/password/reset/${encodeMail}`);\n    return (0, _response.successResponse)(res, responseData);\n  },\n  passwordConfirm: async (req, res) => {\n    const responseData = await _ett.ettClient.post('password/confirm', req.body);\n    return (0, _response.successResponse)(res, responseData);\n  },\n  passwordChange: async (req, res) => {\n    const responseData = await _ett.ettClient.post('/password', req.body);\n    return (0, _response.successResponse)(res, responseData);\n  },\n  createCustomer: async (req, res) => {\n    const {\n      country = '',\n      email = '',\n      phone = ''\n    } = req.body;\n    if (!_regexp.default.countryCode.test(country)) throw new Error('Country code is invalid');\n    if (!_regexp.default.phone.test(phone)) throw new Error('Phone is invalid');\n    if (!_regexp.default.email.test(email)) throw new Error('Email is invalid');\n    const customerData = { ...req.body,\n      roles: [{\n        role: 'signatory'\n      }]\n    };\n    const data = JSON.stringify({\n      country,\n      stakeholders: [customerData]\n    });\n    const ettResponseData = await _ett.ettClient.get('/application.php', {\n      params: {\n        request: 'new',\n        type: 'personal',\n        key: (0, _config.getEnv)('BASE_API_KEY'),\n        hash: (0, _utils.createHashSha1)(`personal${data}`.replace(/\\//g, '\\\\/')),\n        data\n      }\n    });\n    await _models.EttCustomer.create({ ...customerData,\n      ettAppId: ettResponseData.reference\n    });\n    return (0, _response.successResponse)(res, ettResponseData);\n  },\n  verificationConfirm: async (req, response) => {\n    const {\n      nonce\n    } = req.body;\n    const {\n      application\n    } = await _ett.ettClient.post('/application/confirm', {\n      nonce\n    });\n    const stakeholderDocumentData = await _models.StakeholderDocumentData.findOne({\n      nonce\n    }).populate('stakeholderDocumentFiles');\n    application.stakeholders[0].requirements = await Promise.all(application.stakeholders[0].requirements.map(async ({\n      id: requirementId,\n      types: requirementTypes,\n      ...requirement\n    }) => {\n      requirementTypes = await Promise.all(requirementTypes.map(async ({\n        id: typeId,\n        ...requirementType\n      }) => {\n        var _stakeholderDocumentD;\n\n        const {\n          fileUrl = null\n        } = (stakeholderDocumentData === null || stakeholderDocumentData === void 0 ? void 0 : (_stakeholderDocumentD = stakeholderDocumentData.stakeholderDocumentFiles) === null || _stakeholderDocumentD === void 0 ? void 0 : _stakeholderDocumentD.find(({\n          requirement: requirementDBId,\n          type: typeDBId\n        }) => requirementDBId === requirementId && typeDBId === typeId)) || {\n          fileUrl: null\n        };\n\n        if (fileUrl) {\n          requirementType.fileUrl = fileUrl;\n        }\n\n        return {\n          id: typeId,\n          ...requirementType\n        };\n      }));\n      return { ...requirement,\n        id: requirementId,\n        types: requirementTypes\n      };\n    }));\n    const data = {\n      application\n    };\n    return response.json({\n      data\n    });\n  },\n  uploadDocument: async (request, response) => {\n    const {\n      file\n    } = request;\n    if (!file) throw new Error('file is required');\n    const {\n      nonce = '',\n      mode = '',\n      stakeholder: rawStakeholder = 0,\n      requirement: rawRequirement = 0,\n      type: rawType = 0\n    } = request.body;\n    if (!nonce.length) throw new Error('Nonce is required');\n    if (!mode.length) throw new Error('Mode is required');\n    const rawIDSData = {\n      rawRequirement,\n      rawType,\n      rawStakeholder\n    };\n    const [requirement, type, stakeholder] = Object.keys(rawIDSData).reduce((accumulator = [], key = '') => {\n      const numberFormat = parseInt(rawIDSData[key]);\n\n      if (isNaN(numberFormat)) {\n        throw new Error(`Invalid ${key} format!`);\n      }\n\n      return accumulator = [...accumulator, numberFormat];\n    }, []);\n    const {\n      application: {\n        stakeholders\n      }\n    } = await _ett.ettClient.post('/application/confirm', {\n      nonce\n    });\n    const foundStakeholderIntoEtt = stakeholders.find(({\n      id\n    }) => id === stakeholder);\n    if (!foundStakeholderIntoEtt) throw new Error('Invalid stakeholder');\n    const foundRequirementsIntoEtt = foundStakeholderIntoEtt.requirements.find(({\n      id\n    }) => id === requirement);\n    if (!foundRequirementsIntoEtt) throw new Error('Invalid requirement');\n    if (foundRequirementsIntoEtt.types.some(({\n      id\n    }) => id !== type)) throw new Error('Invalid type');\n    const rawNameFileArray = file.originalname.split('.');\n    const fileTypePath = rawNameFileArray[rawNameFileArray.length - 1];\n    const uploadPath = `${nonce}/${requirement}`;\n    const fileName = `${type}.${fileTypePath}`;\n    const filePathPromise = (0, _upload.default)({\n      uploadPath,\n      name: fileName,\n      file\n    });\n    const stakeholderDocumentDataPayload = {\n      stakeholder,\n      nonce\n    };\n    let stakeholderDocumentData = await _models.StakeholderDocumentData.findOne(stakeholderDocumentDataPayload);\n\n    if (!stakeholderDocumentData) {\n      stakeholderDocumentData = new _models.StakeholderDocumentData({ ...stakeholderDocumentDataPayload,\n        stakeholderDocumentFiles: []\n      });\n    }\n\n    const stakeholderDocumentFilePayload = {\n      stakeholderDocumentData,\n      requirement\n    };\n    let stakeholderDocumentFile = await _models.StakeholderDocumentFile.findOne(stakeholderDocumentFilePayload);\n\n    if (!stakeholderDocumentFile) {\n      stakeholderDocumentFile = new _models.StakeholderDocumentFile({ ...stakeholderDocumentFilePayload,\n        type,\n        mode\n      });\n    }\n\n    await filePathPromise;\n    stakeholderDocumentFile.fileUrl = `uploads/${uploadPath}/${fileName}`;\n    stakeholderDocumentData.stakeholderDocumentFiles = [...stakeholderDocumentData.stakeholderDocumentFiles.filter( // filter duplicate\n    filteredDocumentFilesIntoData => {\n      var _stakeholderDocumentF, _stakeholderDocumentF2;\n\n      return filteredDocumentFilesIntoData.toString() !== (((_stakeholderDocumentF = stakeholderDocumentFile) === null || _stakeholderDocumentF === void 0 ? void 0 : (_stakeholderDocumentF2 = _stakeholderDocumentF._id) === null || _stakeholderDocumentF2 === void 0 ? void 0 : _stakeholderDocumentF2.toString()) || stakeholderDocumentFile.toString());\n    }), stakeholderDocumentFile];\n    const rawDocumentDBData = [stakeholderDocumentData, stakeholderDocumentFile];\n    await Promise.all(rawDocumentDBData.map(documentDBData => documentDBData.validate()));\n    await Promise.all(rawDocumentDBData.map(documentDBData => documentDBData.save()));\n    return response.send();\n  },\n  verificationDocument: async (request, response) => {\n    const {\n      nonce = ''\n    } = request.body;\n    if (!nonce) throw new Error('Nonce is required');\n    const {\n      application: {\n        stakeholders\n      }\n    } = await _ett.ettClient.post('/application/confirm', {\n      nonce\n    });\n    const stakeholder = stakeholders[0];\n    const {\n      requirements\n    } = stakeholder;\n    if (requirements.length === 0) return response.send();\n    const stakeholderDocumentSchema = await _models.StakeholderDocumentData.findOne({\n      nonce,\n      stakeholder: stakeholder.id\n    }).populate('stakeholderDocumentFiles');\n    if (!stakeholderDocumentSchema) throw new Error('Documents not found!');\n    const {\n      stakeholderDocumentFiles\n    } = stakeholderDocumentSchema;\n    const isFullUploadedRequirements = requirements.every(({\n      id: requirementId,\n      types,\n      name\n    }) => stakeholderDocumentFiles // is found requirement\n    .some(({\n      type,\n      mode,\n      requirement\n    }) => requirement === requirementId && mode === name && types // is found type into requirement\n    .some(({\n      id: typeId\n    }) => typeId === type)));\n    if (!isFullUploadedRequirements) throw new Error('Not all required documents have been downloaded!');\n    const rawPayloadShiftyProDataDocuments = await Promise.all(stakeholderDocumentFiles.map(async ({\n      mode,\n      fileUrl\n    }) => {\n      const fullPath = _path.default.resolve(_process.default.env.PWD, fileUrl);\n\n      const fileBuffer = await _promises.default.readFile(fullPath);\n      const fileBase64 = fileBuffer.toString('base64');\n      const pathArray = fileUrl.split('/');\n      const fileName = pathArray[pathArray.length - 1];\n      return [mode, {\n        fileBase64,\n        fileName\n      }];\n    }));\n    const payloadShiftyProDataDocuments = Object.fromEntries(rawPayloadShiftyProDataDocuments.map(array => [array[0], {\n      proof: array[1].fileBase64\n    }]));\n    const {\n      verification_data: verificationData = {},\n      verification_result: verificationResult = {},\n      declined_reason: declinedReason = ''\n    } = await shuftiPro.getDataInfoIntoShufiPro({\n      reference: +new Date() + nonce,\n      ...payloadShiftyProDataDocuments\n    });\n    let isFailVerification = false;\n    const valuesVerificationResult = Object.values(verificationResult);\n\n    for (let i = 0; i < valuesVerificationResult.length; i++) {\n      const result = valuesVerificationResult[i];\n\n      if (typeof result === 'string' || typeof result === 'number') {\n        isFailVerification = +result === 0;\n      } else if (typeof result === 'object') {\n        if (Array.isArray(result)) {\n          isFailVerification = result.includes(0) || result.includes('0');\n        } else {\n          const valuesIntoResult = Object.values(result);\n          isFailVerification = valuesIntoResult.includes(0) || valuesIntoResult.includes('0');\n        }\n      }\n\n      if (isFailVerification) break;\n    }\n\n    if (!isFailVerification) {\n      const payloadETTDataDocuments = Object.fromEntries(rawPayloadShiftyProDataDocuments);\n      const payloadDataETT = requirements.reduce((accumulator, {\n        id: requirement,\n        types,\n        name\n      }) => {\n        const payload = types.map(({\n          id: type\n        }) => ({\n          nonce,\n          type,\n          requirement,\n          stakeholder: stakeholder.id,\n          name: payloadETTDataDocuments[name].fileName,\n          data: payloadETTDataDocuments[name].fileBase64\n        }));\n        accumulator = [...accumulator, ...payload];\n        return accumulator;\n      }, []);\n      await Promise.all(payloadDataETT.map(payload => _ett.ettClient.post('/application/document', payload)));\n    }\n\n    return (0, _response.successResponse)(response, {\n      verificationData,\n      verificationResult,\n      declinedReason\n    });\n  },\n  deleteDocument: async (req, res) => {\n    const {\n      nonce,\n      id\n    } = req.params;\n    await _ett.ettClient.post('/application/confirm', {\n      nonce\n    });\n    const document = await _models.EttCustomerDocument.findById(id);\n\n    if (document.ettId) {\n      await _ett.ettClient.delete(`/application/document/${nonce}/${document.ettId}`);\n    }\n\n    return (0, _response.successResponse)(res, await document.delete());\n  },\n  verificationCustomer: async (req, res) => {\n    const {\n      copyPassport,\n      selfiePassport,\n      addressPassport,\n      ...other\n    } = req.body;\n    return (0, _response.successResponse)(res, await shuftiPro.sendVerification({ ...other,\n      files: {\n        copyPassport,\n        selfiePassport,\n        addressPassport\n      }\n    }));\n  },\n  checkEmailIsExist: async (req, res) => {\n    try {\n      const {\n        email\n      } = req.body;\n      if (!email || !/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$/.test(email)) throw new Error('email is invalid');\n      const id = '0';\n      const {\n        customers\n      } = await _ett.ettClient.get('/customer.php', {\n        params: {\n          request: 'customer',\n          key: (0, _config.getEnv)('BASE_API_KEY'),\n          email,\n          id,\n          hash: (0, _utils.createHashSha1)(id + email)\n        }\n      });\n      return (0, _response.successResponse)(res, {\n        data: (customers === null || customers === void 0 ? void 0 : customers.length) > 0\n      });\n    } catch (e) {\n      return (0, _response.successResponse)(res, {\n        data: false\n      });\n    }\n  },\n  phoneChange: async (request, response) => {\n    const {\n      phone,\n      password\n    } = request.body;\n    if (!phone || !_regexp.default.phone.test(phone)) throw new Error('invalid phone number');\n    const {\n      customer\n    } = request;\n\n    try {\n      await _auth.default.login({\n        login: customer.email,\n        password\n      });\n    } catch {\n      throw new Error('invalid password!');\n    }\n\n    const code = (0, _generator.getRandomStringNumber)(0, 999999);\n\n    const savePhonePromise = _models.PhoneConfirm.findOneAndUpdate({\n      id: customer.id\n    }, {\n      id: customer.id,\n      phone,\n      code\n    }, {\n      upsert: true\n    });\n\n    const sendMessagePromise = _clickSend.default.sendSms(code, phone);\n\n    await Promise.all([savePhonePromise, sendMessagePromise]);\n    return response.send();\n  },\n  phoneConfirm: async (request, response) => {\n    const {\n      code\n    } = request.query;\n    if (!_regexp.default.digitalCode.test(code)) throw new Error('invalid digital code');\n    const {\n      customer\n    } = request;\n    const data = await _models.PhoneConfirm.findOneAndRemove({\n      id: customer.id,\n      code\n    });\n    if (!data) throw new Error('invalid digital code');\n\n    try {\n      await zenDeskService.changePhone(customer, data.phone);\n    } catch (error) {\n      throw new Error('Service is disconnected! Please try again later!');\n    }\n\n    return response.send();\n  },\n  emailChange: async (request, response) => {\n    const {\n      email,\n      password\n    } = request.body;\n    if (!_regexp.default.email.test(email)) throw new Error('Invalid email!');\n    const {\n      customer\n    } = request;\n\n    try {\n      await _auth.default.login({\n        login: customer.email,\n        password\n      });\n    } catch {\n      throw new Error('invalid password!');\n    }\n\n    const code = (0, _generator.getRandomStringNumber)(0, 999999);\n\n    const saveEmailPromise = _models.EmailConfirm.findOneAndUpdate({\n      id: customer.id\n    }, {\n      id: customer.id,\n      email,\n      code\n    }, {\n      upsert: true\n    });\n\n    const sendEmailPromise = (0, _helpers.sendMail)({\n      to: email,\n      subject: 'Confirm new email',\n      templateName: 'confirm-email',\n      params: {\n        code\n      }\n    });\n    await Promise.all([saveEmailPromise, sendEmailPromise]);\n    return response.send();\n  },\n  emailConfirm: async (request, response) => {\n    const {\n      code\n    } = request.query;\n    if (!_regexp.default.digitalCode.test(code)) throw new Error('invalid digital code');\n    const {\n      customer\n    } = request;\n    const data = await _models.EmailConfirm.findOneAndRemove({\n      id: customer.id,\n      code\n    });\n    if (!data) throw new Error('invalid digital code');\n\n    try {\n      await zenDeskService.changeEmail(customer, data.email);\n    } catch (error) {\n      throw new Error('Service is disconnected! Please try again later!');\n    }\n\n    return response.send();\n  }\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/auth/index.js?");

/***/ }),

/***/ "./src/controllers/backOffice/index.js":
/*!*********************************************!*\
  !*** ./src/controllers/backOffice/index.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _backOffice = _interopRequireDefault(__webpack_require__(/*! ../../services/backOffice */ \"./src/services/backOffice.js\"));\n\nvar _backOffice2 = _interopRequireDefault(__webpack_require__(/*! ../../client/backOffice.client */ \"./src/client/backOffice.client.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst backOfficeService = new _backOffice.default(new _backOffice2.default());\nvar _default = {\n  fees: async (request, response) => {\n    const data = await backOfficeService.fees();\n    return response.json(data);\n  },\n  fields: async (request, response) => {\n    const {\n      countryCode\n    } = request.query;\n    const data = await backOfficeService.fields(countryCode);\n    return response.json(data);\n  },\n  purposes: async (request, response) => {\n    const data = await backOfficeService.purposes();\n    return response.json(data);\n  },\n  verificationBeneficiary: async (beneficiary, customerID) => {\n    await backOfficeService.verificationBeneficiary(beneficiary, customerID);\n  }\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/backOffice/index.js?");

/***/ }),

/***/ "./src/controllers/beneficiary/index.js":
/*!**********************************************!*\
  !*** ./src/controllers/beneficiary/index.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _promises = _interopRequireDefault(__webpack_require__(/*! fs/promises */ \"fs/promises\"));\n\nvar _process = _interopRequireDefault(__webpack_require__(/*! process */ \"process\"));\n\nvar _aes = _interopRequireDefault(__webpack_require__(/*! crypto-js/aes */ \"crypto-js/aes\"));\n\nvar _encUtf = _interopRequireDefault(__webpack_require__(/*! crypto-js/enc-utf8 */ \"crypto-js/enc-utf8\"));\n\nvar _ett = __webpack_require__(/*! ../../client/ett.client */ \"./src/client/ett.client.js\");\n\nvar _response = __webpack_require__(/*! ../../helpers/response */ \"./src/helpers/response/index.js\");\n\nvar _models = __webpack_require__(/*! ../../models */ \"./src/models/index.js\");\n\nvar _index = _interopRequireDefault(__webpack_require__(/*! ../backOffice/index */ \"./src/controllers/backOffice/index.js\"));\n\nvar _shuftiPro = _interopRequireDefault(__webpack_require__(/*! ../../core/shuftiPro */ \"./src/core/shuftiPro/index.js\"));\n\nvar _helpers = __webpack_require__(/*! ../../helpers */ \"./src/helpers/index.js\");\n\nvar _clickSend = _interopRequireDefault(__webpack_require__(/*! ../../core/clickSend */ \"./src/core/clickSend/index.js\"));\n\nvar _sendMail = _interopRequireDefault(__webpack_require__(/*! ../../helpers/sendMail */ \"./src/helpers/sendMail/index.js\"));\n\nvar _regexp = _interopRequireDefault(__webpack_require__(/*! ../../helpers/regexp */ \"./src/helpers/regexp/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* eslint-disable import/no-unresolved */\n\n/* eslint-disable camelcase */\n// import apply from '../../core/apply';\n// import { APIError } from '../../helpers/errors';\nconst shuftiPro = new _shuftiPro.default();\nconst prefix = '/beneficiary';\nvar _default = {\n  index: async (request, response) => {\n    const foundBeneficiaries = await _models.Beneficiary.find({});\n    return (0, _response.successResponse)(response, {\n      data: foundBeneficiaries,\n      total: foundBeneficiaries.length\n    }); // const { start, limit = 20 } = request.query;\n    // try {\n    //   const { beneficiary, ...responseData } = await ettClientAuthToken(request).get(prefix, {\n    //     params: {\n    //       limit, start,\n    //     },\n    //   });\n    //   const foundBeneficiaries = await Beneficiary.find({ ettId: beneficiary.map(({ id }) => id) });\n    //   const mergeBeneficiary = beneficiary.map((acc) => {\n    //     acc.name = foundBeneficiaries.find(({ ettId }) => ettId === acc.id)?.name || acc.name;\n    //     return acc;\n    //   });\n    //   return successResponse(response, { data: mergeBeneficiary, ...responseData });\n    // } catch (e) {\n    //   if (e.message === 'No Records') return successResponse(response, { data: [] });\n    //   return errorResponse(response, new APIError(e?.message, 400));\n    // }\n  },\n  show: async (req, res) => {\n    const resEtt = await (0, _ett.ettClientAuthToken)(req).get(`${prefix}/${req.params.id}`);\n    return (0, _response.successResponse)(res, resEtt);\n  },\n  create: async (req, res) => {\n    const isInvalidBank = (0, _response.bankValidator)(req, res);\n    const {\n      name,\n      currencyCode = '',\n      country = '',\n      type,\n      isLocal = false,\n      phone = '',\n      email = '',\n      aba = '',\n      bic = '',\n      sortCode = '',\n      accountNumber = '',\n      accountType,\n      bankName,\n      bankAddress\n    } = req.body;\n    let {\n      iban = ''\n    } = req.body;\n    if (isInvalidBank) return false;\n    const isLikelyLocal = isLocal && country === currencyCode.substr(0, 2);\n    const isLikelySortCode = isLikelyLocal && ['GB'].includes(country);\n    const isLikelyABA = isLikelyLocal && ['US'].includes(country);\n\n    const currentSortCode = _regexp.default.sortCode.test(sortCode);\n\n    const currentAba = _regexp.default.aba.test(aba);\n\n    if (accountNumber && _regexp.default.iban.test(accountNumber) && !iban) {\n      iban = accountNumber;\n    } // const dataApply = await apply.verification(country, {\n    //   accountNumber: iban || accountNumber,\n    //   sortCode,\n    //   bic,\n    // });\n    // if (dataApply.status !== 'PASS') {\n    //   throw new Error('bank data is invalid');\n    // }\n\n\n    const reqData = {\n      country,\n      bankcountry: country,\n      currencycode: currencyCode,\n      name,\n      bic: bic || isLikelySortCode && currentSortCode && sortCode || isLikelyABA && currentAba && aba || ''\n    };\n    if (iban) reqData.iban = iban;\n    if (accountNumber) reqData.accountnumber = accountNumber;\n    if (type) reqData.type = type;\n    if (phone) reqData.phone = phone;\n    if (email) reqData.email = email;\n    if (accountType) reqData.accounttype = accountType;\n    if (bankName) reqData.bankname = bankName;\n    if (bankAddress) reqData.bankaddress = bankAddress;\n    const {\n      id\n    } = await (0, _ett.ettClientAuthToken)(req).post(prefix, reqData);\n    const beneficiary = await _models.Beneficiary.create({\n      ettId: id,\n      ...req.body\n    });\n    return (0, _response.successResponse)(res, {\n      data: beneficiary\n    });\n  },\n  update: async (request, response) => {\n    const {\n      id\n    } = request.params;\n    const {\n      name\n    } = request.body;\n    const {\n      beneficiaries\n    } = await (0, _ett.ettClientAuthToken)(request).get(`${prefix}/${id}`);\n    const beneficiary = await _models.Beneficiary.findOneAndUpdate({\n      ettId: beneficiaries[0].id\n    }, { ...beneficiaries[0],\n      name\n    });\n    beneficiary.id = beneficiaries[0].id;\n    return response.json({\n      beneficiary\n    });\n  },\n  delete: async (request, response) => {\n    const {\n      id\n    } = request.params;\n    const result = await (0, _ett.ettClientAuthToken)(request).delete(`${prefix}/${id}`);\n    return (0, _response.successResponse)(response, { ...result,\n      id: +id\n    });\n  },\n  sendVerificationLink: async (request, response) => {\n    const {\n      id\n    } = request.params;\n    const {\n      email = '',\n      phone = ''\n    } = request.body;\n\n    if (!email && !phone) {\n      return response.status(400).json({\n        message: 'Phone or email is required'\n      });\n    }\n\n    const {\n      customer\n    } = request;\n    await _models.Beneficiary.updateOne({\n      ettId: id\n    }, {\n      verifyID: customer.id\n    });\n    const payloadKey = JSON.stringify({\n      customerID: customer.id,\n      beneficiaryID: id\n    });\n\n    const crypt = _aes.default.encrypt(payloadKey, (0, _helpers.getEnv)('JWT_AUTHORIZATION_SECRET')).toString();\n\n    const baseFEURL = new URL((0, _helpers.getEnv)('FE_APP_URL'));\n    const verificationUrl = `${baseFEURL.href}recipients/${id}/verification/?key=${crypt}`;\n\n    if (phone) {\n      if (!_regexp.default.phone.test(phone)) {\n        return response.status(400).json({\n          message: 'Phone is invalid'\n        });\n      }\n\n      let body = 'Please open on the link below to verify your identity on ShuftiPro:\\n\\n';\n      body += 'If you have not submitted a request for \"blocFX\" please ignore this message.\\n\\n\\n';\n      body += verificationUrl;\n      body += '\\n\\n\\nThank you for your continued loyalty.'; // eslint-disable-next-line no-unused-vars\n\n      body += '\\n\\nBlocFX.';\n\n      _clickSend.default.sendSms(body, phone);\n    }\n\n    if (email) {\n      if (!_regexp.default.email.test(email)) {\n        return response.status(400).json({\n          message: 'Email is invalid'\n        });\n      }\n\n      (0, _sendMail.default)({\n        to: email,\n        subject: 'Verification account',\n        templateName: 'shuftipro-beneficiary',\n        params: {\n          appUrl: '',\n          verificationUrl\n        }\n      });\n    }\n\n    const beneficiary = await _models.Beneficiary.findOne({\n      ettId: id\n    });\n    return response.json({\n      beneficiary,\n      customer,\n      verificationUrl\n    });\n  },\n  verificationDocument: async (request, response) => {\n    const {\n      files\n    } = request;\n    const {\n      id\n    } = request.params;\n    const file = files[0];\n\n    if (!file) {\n      return response.status(400).json({\n        message: 'File is required'\n      });\n    }\n\n    if (file.size < 8192) {\n      return response.status(403).json({\n        message: 'Bad quality file!'\n      });\n    }\n\n    if (file.size >= 16777216) {\n      return response.status(413).json({\n        message: 'The maximum file size must not exceed 16 megabytes!'\n      });\n    }\n\n    if (!['image/png', 'image/jpg', 'image/jpeg'].includes(file.mimetype)) {\n      return response.status(406).json({\n        message: 'File format must be is jpg or png!'\n      });\n    }\n\n    const documentTypes = (request.body.supportedTypes || 'id_card,passport').split(',');\n    const {\n      fullName = '',\n      dob = '',\n      issueDate = '',\n      expiryDate = '',\n      documentNumber = '',\n      country = '',\n      email = ''\n    } = request.body;\n\n    const dirname = _process.default.mainModule.path.replace('/src', '').replace('/build', '');\n\n    const uploadsDir = `${dirname}/uploads`;\n    const filePath = `${uploadsDir}/${file.originalname}`;\n\n    try {\n      await _promises.default.access(uploadsDir);\n    } catch {\n      await _promises.default.mkdir(uploadsDir);\n    }\n\n    await _promises.default.writeFile(filePath, file.buffer, 'binary');\n    const baseFEURL = new URL((0, _helpers.getEnv)('FE_APP_URL'));\n    const payload = {\n      reference: `beneficiary/${Date.now()}`,\n      callback_url: baseFEURL.href,\n      email,\n      country,\n      language: 'EN',\n      redirect_url: baseFEURL.href,\n      verification_mode: 'any',\n      document: {\n        supported_types: documentTypes,\n        name: {\n          full_name: fullName\n        },\n        dob,\n        issue_date: issueDate,\n        expiry_date: expiryDate,\n        document_number: documentNumber,\n        proof: file.buffer.toString('base64')\n      }\n    };\n    const data = await shuftiPro.getDataInfoIntoShufiPro(payload);\n    const verificationResult = data.verification_result;\n    const verificationData = data.verification_data.document;\n\n    if (verificationResult) {\n      const statusVerificationSum = Object.values(verificationResult.document) // eslint-disable-next-line no-return-assign\n      .reduce((accumulator, currentValue) => {\n        const check = typeof currentValue === 'number';\n\n        if (check) {\n          // eslint-disable-next-line no-param-reassign\n          accumulator += currentValue;\n        }\n\n        return accumulator;\n      }, 0);\n\n      const toCapitalize = (string = '') => {\n        const [firstLetter = '', ...nextLetters] = string.split('');\n        return firstLetter.toUpperCase() + [...nextLetters].join('').toLowerCase();\n      };\n\n      const firstname = toCapitalize(verificationData.name.first_name || verificationData.name.full_name.split(' ')[0]);\n      const name = toCapitalize(verificationData.name.last_name || verificationData.name.full_name.split(' ')[1]);\n      const payloadData = {\n        isCorrectVerify: statusVerificationSum >= 9,\n        firstname,\n        name,\n        dob: new Date(verificationData.dob),\n        expiryDate: new Date(verificationData.expiry_date),\n        issueDate: new Date(verificationData.issue_date),\n        documentNumber: verificationData.document_number\n      };\n      await _models.Beneficiary.updateOne({\n        ettId: id\n      }, payloadData);\n    }\n\n    return response.status(200).json({\n      data\n    });\n  },\n  verificationBeneficiary: async (request, response) => {\n    const {\n      id\n    } = request.params;\n    const {\n      key\n    } = request.body;\n\n    if (!key) {\n      return response.status(403).json({\n        message: 'invalid token'\n      });\n    } // decrypt key\n\n\n    const binary = _aes.default.decrypt(key, (0, _helpers.getEnv)('JWT_AUTHORIZATION_SECRET'));\n\n    const {\n      customerID,\n      beneficiaryID\n    } = JSON.parse(binary.toString(_encUtf.default));\n\n    if (beneficiaryID !== id) {\n      return response.status(403).json({\n        message: 'invalid beneficiary'\n      });\n    }\n\n    if (!customerID) {\n      return response.status(403).json({\n        message: 'invalid customer'\n      });\n    } // check verify\n\n\n    const beneficiary = await _models.Beneficiary.findOne({\n      ettId: id,\n      verifyID: customerID\n    });\n    if (!(beneficiary !== null && beneficiary !== void 0 && beneficiary.isCorrectVerify)) throw new Error('Verification failed'); // verify\n\n    const result = await _index.default.verificationBeneficiary(beneficiary, customerID);\n\n    if (result.status === 'ok' && id === beneficiaryID) {\n      return response.json({\n        beneficiary\n      });\n    }\n\n    return response.status(503).json({\n      message: 'Ett is disconnect'\n    });\n  }\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/beneficiary/index.js?");

/***/ }),

/***/ "./src/controllers/currencyCloud/index.js":
/*!************************************************!*\
  !*** ./src/controllers/currencyCloud/index.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _response = __webpack_require__(/*! ../../helpers/response */ \"./src/helpers/response/index.js\");\n\nvar _currencyCloud = _interopRequireDefault(__webpack_require__(/*! ../../core/currencyCloud */ \"./src/core/currencyCloud/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar _default = {\n  findBankDetails: async (req, res) => {\n    const {\n      identifierType,\n      identifierValue\n    } = req.query;\n    const data = await _currencyCloud.default.findBankDetails(identifierType, identifierValue);\n    return (0, _response.successResponse)(res, {\n      data\n    });\n  }\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/currencyCloud/index.js?");

/***/ }),

/***/ "./src/controllers/email/index.js":
/*!****************************************!*\
  !*** ./src/controllers/email/index.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _response = __webpack_require__(/*! ../../helpers/response */ \"./src/helpers/response/index.js\");\n\nvar _helpers = __webpack_require__(/*! ../../helpers */ \"./src/helpers/index.js\");\n\nvar _default = {\n  send: async (req, res) => {\n    const {\n      templateName,\n      params,\n      subject\n    } = req.body;\n    await (0, _helpers.sendMail)({\n      to: req.customer.email,\n      subject,\n      templateName,\n      params\n    });\n    return (0, _response.successResponse)(res, true);\n  }\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/email/index.js?");

/***/ }),

/***/ "./src/controllers/hooks/index.js":
/*!****************************************!*\
  !*** ./src/controllers/hooks/index.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _response = __webpack_require__(/*! ../../helpers/response */ \"./src/helpers/response/index.js\");\n\nvar _ws = _interopRequireDefault(__webpack_require__(/*! ../../ws */ \"./src/ws.js\"));\n\nvar _models = __webpack_require__(/*! ../../models */ \"./src/models/index.js\");\n\nvar _hook = _interopRequireDefault(__webpack_require__(/*! ../../services/hook */ \"./src/services/hook.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst hookService = new _hook.default();\nvar _default = {\n  external: async (req, res) => {\n    var _req$body;\n\n    if (req !== null && req !== void 0 && (_req$body = req.body) !== null && _req$body !== void 0 && _req$body.customer) {\n      const uId = req.body.customer;\n      const {\n        transactionId,\n        status\n      } = req.body;\n      const response = {\n        title: 'Transaction status',\n        date: Date.now(),\n        body: `Transaction #${transactionId}: is ${status.toLowerCase()}`,\n        uId\n      };\n\n      _models.Notification.create(response);\n\n      (0, _ws.default)().emit(uId)('notification')(response);\n    }\n\n    return (0, _response.successResponse)(res, {\n      success: true\n    });\n  },\n  internal: async (req, res) => {\n    console.log(req.query, req.params, req.body, 'internal');\n    return (0, _response.successResponse)(res, {\n      success: true\n    });\n  },\n  deposit: async (req, res) => {\n    console.log(req.query, req.params, req.body, 'deposit');\n    return (0, _response.successResponse)(res, {\n      success: true\n    });\n  },\n  zendeskMessage: async (request, response) => {\n    const {\n      zendesk\n    } = request.body;\n    hookService.zendeskMessage(zendesk);\n    return (0, _response.successResponse)(response, {});\n  }\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/hooks/index.js?");

/***/ }),

/***/ "./src/controllers/index.js":
/*!**********************************!*\
  !*** ./src/controllers/index.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"auth\", ({\n  enumerable: true,\n  get: function () {\n    return _auth.default;\n  }\n}));\nObject.defineProperty(exports, \"beneficiary\", ({\n  enumerable: true,\n  get: function () {\n    return _beneficiary.default;\n  }\n}));\nObject.defineProperty(exports, \"paymentAccountController\", ({\n  enumerable: true,\n  get: function () {\n    return _accounts.default;\n  }\n}));\nObject.defineProperty(exports, \"transferController\", ({\n  enumerable: true,\n  get: function () {\n    return _transfers.default;\n  }\n}));\nObject.defineProperty(exports, \"shuftiProCallback\", ({\n  enumerable: true,\n  get: function () {\n    return _shuftiPro.default;\n  }\n}));\nObject.defineProperty(exports, \"email\", ({\n  enumerable: true,\n  get: function () {\n    return _email.default;\n  }\n}));\nObject.defineProperty(exports, \"hooks\", ({\n  enumerable: true,\n  get: function () {\n    return _hooks.default;\n  }\n}));\nObject.defineProperty(exports, \"notification\", ({\n  enumerable: true,\n  get: function () {\n    return _notification.default;\n  }\n}));\nObject.defineProperty(exports, \"apply\", ({\n  enumerable: true,\n  get: function () {\n    return _apply.default;\n  }\n}));\nObject.defineProperty(exports, \"currencyCloud\", ({\n  enumerable: true,\n  get: function () {\n    return _currencyCloud.default;\n  }\n}));\nObject.defineProperty(exports, \"backOffice\", ({\n  enumerable: true,\n  get: function () {\n    return _backOffice.default;\n  }\n}));\n\nvar _auth = _interopRequireDefault(__webpack_require__(/*! ./auth */ \"./src/controllers/auth/index.js\"));\n\nvar _beneficiary = _interopRequireDefault(__webpack_require__(/*! ./beneficiary */ \"./src/controllers/beneficiary/index.js\"));\n\nvar _accounts = _interopRequireDefault(__webpack_require__(/*! ./accounts */ \"./src/controllers/accounts/index.js\"));\n\nvar _transfers = _interopRequireDefault(__webpack_require__(/*! ./transfers */ \"./src/controllers/transfers/index.js\"));\n\nvar _shuftiPro = _interopRequireDefault(__webpack_require__(/*! ./shuftiPro */ \"./src/controllers/shuftiPro/index.js\"));\n\nvar _email = _interopRequireDefault(__webpack_require__(/*! ./email */ \"./src/controllers/email/index.js\"));\n\nvar _hooks = _interopRequireDefault(__webpack_require__(/*! ./hooks */ \"./src/controllers/hooks/index.js\"));\n\nvar _notification = _interopRequireDefault(__webpack_require__(/*! ./notification */ \"./src/controllers/notification/index.js\"));\n\nvar _apply = _interopRequireDefault(__webpack_require__(/*! ./apply */ \"./src/controllers/apply/index.js\"));\n\nvar _currencyCloud = _interopRequireDefault(__webpack_require__(/*! ./currencyCloud */ \"./src/controllers/currencyCloud/index.js\"));\n\nvar _backOffice = _interopRequireDefault(__webpack_require__(/*! ./backOffice */ \"./src/controllers/backOffice/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/index.js?");

/***/ }),

/***/ "./src/controllers/notification/index.js":
/*!***********************************************!*\
  !*** ./src/controllers/notification/index.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _response = __webpack_require__(/*! ../../helpers/response */ \"./src/helpers/response/index.js\");\n\nvar _models = __webpack_require__(/*! ../../models */ \"./src/models/index.js\");\n\nvar _ws = _interopRequireDefault(__webpack_require__(/*! ../../ws */ \"./src/ws.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar _default = {\n  getAll: async (request, response) => {\n    const {\n      customer\n    } = request;\n    const uId = customer.id;\n    const notifications = await _models.Notification.find({\n      uId\n    });\n    return (0, _response.successResponse)(response, notifications);\n  },\n  save: async (request, response) => {\n    (0, _ws.default)().emit('external')(request.body);\n\n    _models.Notification.create({\n      title: 'test Title',\n      date: Date.now(),\n      body: 'test text notification',\n      uId: 32\n    });\n\n    return (0, _response.successResponse)(response, {\n      success: true\n    });\n  },\n  delete: async (request, response) => {\n    const {\n      customer\n    } = request;\n    const uId = customer.id;\n    await _models.Notification.remove({\n      uId\n    });\n    return (0, _response.successResponse)(response, []);\n  }\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/notification/index.js?");

/***/ }),

/***/ "./src/controllers/quote/index.js":
/*!****************************************!*\
  !*** ./src/controllers/quote/index.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nclass QuoteController {\n  constructor(service) {\n    this.service = service;\n  }\n\n  create = async (request, response) => {\n    const quote = await this.service.create(request);\n    return response.status(201).json(quote);\n  };\n  findById = async (request, response) => {\n    const {\n      quoteId\n    } = request.params;\n    const quote = await this.service.findById(quoteId);\n    return response.status(200).json(quote);\n  };\n}\n\nvar _default = QuoteController;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/quote/index.js?");

/***/ }),

/***/ "./src/controllers/shuftiPro/index.js":
/*!********************************************!*\
  !*** ./src/controllers/shuftiPro/index.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _helpers = __webpack_require__(/*! ../../helpers */ \"./src/helpers/index.js\");\n\nvar _models = __webpack_require__(/*! ../../models */ \"./src/models/index.js\");\n\nvar _shuftiPro = _interopRequireDefault(__webpack_require__(/*! ../../core/shuftiPro */ \"./src/core/shuftiPro/index.js\"));\n\nvar _ett = __webpack_require__(/*! ../../client/ett.client */ \"./src/client/ett.client.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar _default = {\n  verificationCustomer: async (req, res) => {\n    var _event$split, _documents$find;\n\n    const {\n      reference,\n      event,\n      email\n    } = req.body;\n    const eventStatus = (event === null || event === void 0 ? void 0 : (_event$split = event.split('.')) === null || _event$split === void 0 ? void 0 : _event$split[1]) || event || 'invalid';\n    const documents = await _models.EttCustomerDocument.find({\n      reference\n    });\n    const {\n      nonce\n    } = (documents === null || documents === void 0 ? void 0 : (_documents$find = documents.find) === null || _documents$find === void 0 ? void 0 : _documents$find.call(documents, doc => !!doc.nonce)) || '';\n\n    if (documents.length) {\n      var _documents$;\n\n      await Promise.all(documents.map(async doc => {\n        // eslint-disable-next-line no-param-reassign\n        doc.status = eventStatus;\n        await doc.save();\n      }));\n      const ettAppId = documents === null || documents === void 0 ? void 0 : (_documents$ = documents[0]) === null || _documents$ === void 0 ? void 0 : _documents$.ettAppId;\n      const documentsSubmit = await _models.EttCustomerDocument.find({\n        ettAppId,\n        status: {\n          $nin: ['rejected', 'declined']\n        }\n      });\n\n      if (eventStatus === 'accepted' && documentsSubmit.length === 3) {\n        await _ett.ettClient.post('application/submit', {\n          nonce\n        });\n      }\n    }\n\n    if (eventStatus === 'declined') {\n      const {\n        params,\n        ...other\n      } = _shuftiPro.default.createPayloadEmailError({\n        email\n      });\n\n      await (0, _helpers.sendMail)({ ...other,\n        params: { ...params,\n          nonce,\n          errorMessage: req.body.declined_reason || 'Verification is invalid!'\n        }\n      });\n    }\n\n    return (0, _helpers.successResponse)(res);\n  }\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/shuftiPro/index.js?");

/***/ }),

/***/ "./src/controllers/transfers/index.js":
/*!********************************************!*\
  !*** ./src/controllers/transfers/index.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _ett = __webpack_require__(/*! ../../client/ett.client */ \"./src/client/ett.client.js\");\n\nvar _response = __webpack_require__(/*! ../../helpers/response */ \"./src/helpers/response/index.js\");\n\nvar _sendMail = _interopRequireDefault(__webpack_require__(/*! ../../helpers/sendMail */ \"./src/helpers/sendMail/index.js\"));\n\nvar _models = __webpack_require__(/*! ../../models */ \"./src/models/index.js\");\n\nvar _transfer = _interopRequireDefault(__webpack_require__(/*! ../../services/transfer */ \"./src/services/transfer.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* eslint-disable max-len */\nconst getLocalDate = () => (Date.now() / 1000).toFixed(0);\n\nconst transferService = new _transfer.default(_models.Transfer);\nconst transferController = {\n  createTransfer: async (request, res) => {\n    const {\n      amount,\n      narrative = 'blockFx transfer',\n      forex\n    } = request.body;\n    const {\n      account = {},\n      beneficiary = {}\n    } = request.meta;\n    if (amount && +amount < 0) throw new Error('amount must be positive!');\n    const foundQuote = await _models.Quote.findOne({\n      id: forex\n    });\n    if (amount && foundQuote.sellamount !== amount) throw new Error('Invalid amount transfer!');\n    const counterpart = {\n      id: beneficiary.id,\n      name: beneficiary.name,\n      currencyCode: beneficiary.currencyCode,\n      country: beneficiary.country,\n      displayAccountNo: beneficiary.displayAccountNo || beneficiary.iban || beneficiary.accountNumber\n    };\n    if (beneficiary.iban) counterpart.iban = beneficiary.iban;\n    if (beneficiary.accountNumber) counterpart.accountNumber = beneficiary.accountNumber;\n    if (beneficiary.bic) counterpart.bic = beneficiary.bic;\n    const payloadTransferData = {\n      accountNumber: account.accountNumber,\n      requestcurrency: account.currency,\n      requestamount: foundQuote.sellamount,\n      counterpart,\n      narrative,\n      status: 'cache',\n      quote: foundQuote\n    };\n\n    if (account.available >= payloadTransferData.requestamount) {\n      const ettRes = await transferService.createTransferEtt(request, payloadTransferData, account.accountNumber, forex);\n      payloadTransferData.id = ettRes.id;\n      payloadTransferData.status = ettRes.status;\n    }\n\n    await transferService.createTransfer(payloadTransferData);\n\n    if (beneficiary.email) {\n      await (0, _sendMail.default)({\n        to: beneficiary.email,\n        subject: 'Transaction open',\n        templateName: 'transaction-details-open',\n        params: {\n          amount: payloadTransferData.requestamount,\n          currency: payloadTransferData.requestcurrency,\n          from: account.name,\n          status: payloadTransferData.status\n        }\n      });\n    }\n\n    return (0, _response.successResponse)(res, payloadTransferData);\n  },\n  historyTransfer: async (request, response) => {\n    const transfers = await _models.Transfer.find();\n    return response.json({\n      data: transfers,\n      total: transfers.length\n    }); // let {\n    //   accountNumber = '' || [],\n    // } = request.query;\n    // const { accounts } = await ettClientAuthToken(request).get('account');\n    // if (typeof accountNumber === 'string') accountNumber = [accountNumber];\n    // if (!accountNumber?.length) {\n    //   accountNumber = accounts.map((account) => account.accountNumber);\n    // } else {\n    //   accountNumber = accountNumber.filter((account) => accounts\n    //     .some((accountNumberIntoQuery) => account.accountNumber === accountNumberIntoQuery));\n    // }\n    // if (accountNumber.length === 0) return response.json({ data: [], total: 0 });\n    // let foundTransfers = await Transfer.find({ accountNumber }).populate('quote').lean();\n    // if (foundTransfers.length) {\n    //   foundTransfers = foundTransfers.map(async (foundTransfer) => {\n    //     if (foundTransfer.status === 'cache') {\n    //       const foundAccount = accounts\n    //         .find((filteredAccount) => filteredAccount.accountNumber === foundTransfer.accountNumber);\n    //       const foundTransferInspectedAmount = await transferService\n    //         .inspectAmountOrCreateTransferEtt(request, foundTransfer, foundAccount);\n    //       return foundTransferInspectedAmount;\n    //     }\n    //     return foundTransfer;\n    //   });\n    //   foundTransfers = await Promise.all(foundTransfers);\n    //   response.json({\n    //     data: foundTransfers,\n    //     total: foundTransfers.length,\n    //   });\n    // }\n    // const params = {\n    //   limit: 10000,\n    //   order: 'desc',\n    //   fromtime: request.query.fromtime || getLocalDate(),\n    //   totime: getLocalDate(),\n    // };\n    // const SendResponseToETT = (accountId, path = '') => ettClientAuthToken(request, false)\n    //   .get(`/account/${accountId}/transfer${path}`, { params });\n    // foundTransfers.forEach((transfer) => {\n    //   const transferTime = (Date.parse(transfer.updatedAt) / 1000).toFixed();\n    //   if (params.fromtime > transferTime) params.fromtime = transferTime;\n    // });\n    // const payloadAccounts = accountNumber.reduce((accumulator, accountId) => {\n    //   if (!accumulator[accountId]) {\n    //     accumulator[accountId] = [\n    //       SendResponseToETT(accountId),\n    //       SendResponseToETT(accountId, '/pending'),\n    //     ];\n    //   }\n    //   return accumulator;\n    // }, {});\n    // let transfersIntoEtt = [];\n    // const payloadPromises = Object.keys(payloadAccounts).map(async (key) => {\n    //   const [dataIntoEtt, dataPendingIntoEtt] = await Promise.all(payloadAccounts[key]);\n    //   const accountTransfersIntoEtt = [...dataIntoEtt.transactions, ...dataPendingIntoEtt.transactions];\n    //   transfersIntoEtt = [...transfersIntoEtt, ...accountTransfersIntoEtt];\n    //   const payloadPromisesPart = accountTransfersIntoEtt.map(async (transferIntoEtt) => {\n    //     const foundTransfer = foundTransfers.find(({ id }) => id === transferIntoEtt.id);\n    //     if (foundTransfer) {\n    //       if (\n    //         foundTransfer.status !== transferIntoEtt.status\n    //         || foundTransfer.queue !== transferIntoEtt.queue\n    //         || ((!foundTransfer.rule && transferIntoEtt.rule) || (foundTransfer.rule && !transferIntoEtt.rule))\n    //       ) {\n    //         return await Transfer.findOneAndUpdate({ id: foundTransfer.id }, { ...foundTransfer, ...transferIntoEtt });\n    //       }\n    //     } else {\n    //       return await transferService.createTransfer({ ...transferIntoEtt, accountNumber: key });\n    //     }\n    //     return null;\n    //   });\n    //   return await Promise.all(payloadPromisesPart);\n    // });\n    // await Promise.all(payloadPromises);\n    // if (!foundTransfers.length) {\n    //   return response.json({\n    //     data: transfersIntoEtt,\n    //     total: transfersIntoEtt.length,\n    //   });\n    // }\n    // return null;\n  },\n  showTransfer: async (request, response) => {\n    const {\n      account,\n      transfer\n    } = request.meta;\n    const inspectedTransfer = await transferService.inspectAmountOrCreateTransferEtt(request, transfer, account);\n    return (0, _response.successResponse)(response, {\n      data: inspectedTransfer\n    });\n  },\n  setReason: async (request, response) => {\n    const {\n      transferId,\n      accountId\n    } = request.params;\n    const {\n      reason\n    } = request.body;\n    if (!reason) throw new Error('Reason is required!');\n    const data = await (0, _ett.ettClientAuthToken)(request).post(`/account/${accountId}/transfer/${transferId}/confirm`, {\n      reason,\n      time: getLocalDate()\n    });\n    return (0, _response.successResponse)(response, {\n      data: data.transactions\n    });\n  },\n  attachDocument: async (request, response) => {\n    const {\n      transferId,\n      accountId\n    } = request.params;\n    const {\n      file\n    } = request;\n    if (!file) throw new Error('file is required!');\n    const {\n      requirement,\n      type\n    } = request.body;\n    const base64 = file.buffer.toString('base64');\n    const data = await (0, _ett.ettClientAuthToken)(request).post(`/account/${accountId}/transfer/${transferId}/document`, {\n      requirement,\n      type,\n      time: getLocalDate(),\n      name: file.originalname,\n      data: base64\n    });\n    return (0, _response.successResponse)(response, {\n      data\n    });\n  },\n  submitDocument: async (request, response) => {\n    const {\n      transferId,\n      accountId\n    } = request.params;\n    const data = await (0, _ett.ettClientAuthToken)(request).post(`/account/${accountId}/transfer/${transferId}/submit`, {\n      time: getLocalDate()\n    });\n    return (0, _response.successResponse)(response, {\n      data\n    });\n  }\n};\nvar _default = transferController;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/controllers/transfers/index.js?");

/***/ }),

/***/ "./src/core/apply/index.js":
/*!*********************************!*\
  !*** ./src/core/apply/index.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _qs = _interopRequireDefault(__webpack_require__(/*! qs */ \"qs\"));\n\nvar _apply = __webpack_require__(/*! ../../client/apply.client */ \"./src/client/apply.client.js\");\n\nvar _config = __webpack_require__(/*! ../../helpers/config */ \"./src/helpers/config/index.js\");\n\nvar _regexp = _interopRequireDefault(__webpack_require__(/*! ../../helpers/regexp */ \"./src/helpers/regexp/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nclass Apply {\n  constructor() {\n    this.token = null;\n    this.username = (0, _config.getEnv)('APPLY_USERNAME');\n    this.password = (0, _config.getEnv)('APPLY_PASSWORD');\n    this.cacheBank = {};\n  }\n\n  _formatBankData(data) {\n    const office = data.branchDetails || [data.paymentBicDetails] || [data.headOfficeDetails];\n    const formatData = {\n      branchDetails: office // eslint-disable-next-line no-shadow\n      === null || office // eslint-disable-next-line no-shadow\n      === void 0 ? void 0 : office // eslint-disable-next-line no-shadow\n      .map(({\n        bankName,\n        street,\n        codeDetails\n      }) => ({\n        bankName,\n        street,\n        codeDetails\n      }))\n    };\n    if (data.accountNumber) formatData.accountNumber = data.accountNumber;\n    if (data.recommendedNatId) formatData.recommendedNatId = data.recommendedNatId;\n    if (data.recommendedBIC) formatData.recommendedBIC = data.recommendedBIC;\n    if (data.bic8) formatData.bic8 = data.bic8;\n    return formatData;\n  }\n\n  async getApplyToken() {\n    if (this.token) return Promise.resolve(this.token);\n    this.token = await this._getToken();\n    setTimeout(() => {\n      this.token = null;\n    }, 60000);\n    return Promise.resolve(this.token);\n  }\n\n  async _getToken() {\n    const {\n      data\n    } = await _apply.applyClient.post('authenticate', _qs.default.stringify({\n      username: this.username,\n      password: this.password\n    }), {\n      headers: {\n        'Content-Type': 'application/x-www-form-urlencoded'\n      }\n    });\n    return data.token;\n  }\n\n  async verification(countryCode, {\n    accountNumber = '',\n    sortCode = '',\n    aba = ''\n  }) {\n    if (!_regexp.default.countryCode.test(countryCode)) throw new Error('invalid country code');\n    if (!_regexp.default.accountNumber.test(accountNumber)) throw new Error('invalid account');\n    const token = await this.getApplyToken();\n    const params = {\n      token,\n      countryCode,\n      accountNumber\n    };\n    if (sortCode) params.nationalId = sortCode;else if (aba) params.nationalId = aba;\n    const {\n      data\n    } = await _apply.applyClient.get('convert/1.0.1', {\n      params\n    });\n    return data;\n  }\n\n  async code(countryCode = '', nationalId = '', accountNumber = '') {\n    if (!_regexp.default.countryCode.test(countryCode)) throw new Error('invalid country code');\n    if (!nationalId && !accountNumber) throw new Error('invalid params');\n    if (!nationalId && !_regexp.default.accountNumber.test(accountNumber)) throw new Error('invalid account number');\n\n    if (!(_regexp.default.sortCode.test(nationalId) || _regexp.default.aba.test(nationalId)) && !accountNumber) {\n      throw new Error('invalid national code');\n    }\n\n    const token = await this.getApplyToken();\n    const params = {\n      token,\n      countryCode\n    };\n    if ([6, 9].includes(nationalId.length)) params.nationalId = nationalId;\n    if (accountNumber.length > 6 && accountNumber.length < 35) params.accountNumber = accountNumber;\n    const {\n      data\n    } = await _apply.applyClient.get('convert/1.0.1', {\n      params\n    });\n    return this._formatBankData(data);\n  }\n\n  async findBankDetails(countryCode, bankName, address, bic8, pageSize = 10) {\n    if (!_regexp.default.countryCode.test(countryCode)) throw new Error('invalid country code');\n    let isPreventLoad = false;\n\n    if (bankName === 'B' && !bic8) {\n      if (this.cacheBank[countryCode]) {\n        return this.cacheBank[countryCode];\n      }\n\n      isPreventLoad = true;\n    }\n\n    const token = await this.getApplyToken();\n    const {\n      data\n    } = await _apply.applyClient.get('convert/1.0.2', {\n      params: {\n        token,\n        pageSize,\n        countryCode,\n        bankName,\n        address,\n        bic8,\n        page: 1\n      }\n    });\n\n    const formatData = this._formatBankData(data);\n\n    if (isPreventLoad) this.cacheBank[countryCode] = formatData;\n    return formatData;\n  }\n\n}\n\nconst apply = new Apply();\nvar _default = apply;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/core/apply/index.js?");

/***/ }),

/***/ "./src/core/auth/index.js":
/*!********************************!*\
  !*** ./src/core/auth/index.js ***!
  \********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _dayjs = _interopRequireDefault(__webpack_require__(/*! dayjs */ \"dayjs\"));\n\nvar _jsonwebtoken = _interopRequireDefault(__webpack_require__(/*! jsonwebtoken */ \"jsonwebtoken\"));\n\nvar _models = __webpack_require__(/*! ../../models */ \"./src/models/index.js\");\n\nvar _helpers = __webpack_require__(/*! ../../helpers */ \"./src/helpers/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// eslint-disable-next-line import/no-cycle\n// import { ettClient, ettClientAuthToken } from '../../client/ett.client';\nclass Auth {\n  async login({\n    login,\n    password\n  }) {\n    // const { token: ettToken } = await ettClient.post('/session', { login, password, time: dayjs().unix() });\n    // const { customer } = await ettClientAuthToken({ ettToken }).get('/customer');\n    if (password !== 'Qwer123!') throw new Error('Invalid password!'); // ! FAKE\n\n    const customer = {\n      id: 0,\n      reference: 'Fake',\n      email: 'Fake@email.fake',\n      phone: '+000000000',\n      firstName: 'Fake',\n      lastName: 'Fake',\n      name: 'Fake',\n      signatory: 'Fake'\n    };\n    const ettToken = password;\n    const ettUserToken = await _models.EttUserToken.create({\n      ettAppId: customer.reference,\n      ettToken,\n      confirm: true,\n      type: 'auth'\n    });\n    return this.encodeToken({\n      customer: { ...customer,\n        email: login\n      },\n      ettUserTokenId: ettUserToken._id\n    });\n  }\n\n  async getEttToken(ettUserTokenId) {\n    const {\n      ettToken\n    } = await _models.EttUserToken.findById(ettUserTokenId);\n    return ettToken;\n  }\n\n  async refreshEttToken(ettToken, ettUserTokenId, customer) {\n    await _models.EttUserToken.findOneAndUpdate({\n      _id: ettUserTokenId\n    }, {\n      ettToken\n    });\n    return this.encodeToken({\n      customer,\n      ettUserTokenId\n    });\n  }\n\n  async logout(ettUserTokenId) {\n    await this._deleteTokensCode([ettUserTokenId]);\n  }\n\n  async _deleteTokensCode(ids = []) {\n    await _models.EttUserToken.deleteMany({\n      $or: [{\n        $and: [{\n          createdAt: {\n            $lt: (0, _dayjs.default)().subtract(10, 'm').toISOString()\n          }\n        }, {\n          confirm: false\n        }, {\n          type: 'auth'\n        }]\n      }, {\n        _id: {\n          $in: ids\n        }\n      }, {\n        updatedAt: {\n          $lt: (0, _dayjs.default)().subtract(65, 'm').toISOString()\n        }\n      }]\n    });\n  }\n\n  encodeToken(data, secret = (0, _helpers.getEnv)('JWT_AUTHORIZATION_SECRET')) {\n    return _jsonwebtoken.default.sign({\n      exp: (0, _dayjs.default)().add(65, 'm').unix(),\n      ...data\n    }, secret);\n  }\n\n  decodeToken(jwtToken, secret = (0, _helpers.getEnv)('JWT_AUTHORIZATION_SECRET')) {\n    return _jsonwebtoken.default.verify(jwtToken, secret);\n  }\n\n}\n\nconst auth = new Auth();\nvar _default = auth;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/core/auth/index.js?");

/***/ }),

/***/ "./src/core/clickSend/index.js":
/*!*************************************!*\
  !*** ./src/core/clickSend/index.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _clickSend = __webpack_require__(/*! ../../client/clickSend.client */ \"./src/client/clickSend.client.js\");\n\nclass ClickSend {\n  async sendSms(body, to, from = 'BlocFX') {\n    try {\n      const data = await _clickSend.clickSendClient.post('/sms/send', {\n        messages: [{\n          body,\n          to,\n          from\n        }]\n      });\n      return data;\n    } catch (e) {\n      console.log(e);\n      throw e;\n    }\n  }\n\n}\n\nconst clickSend = new ClickSend();\nvar _default = clickSend;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/core/clickSend/index.js?");

/***/ }),

/***/ "./src/core/currencyCloud/index.js":
/*!*****************************************!*\
  !*** ./src/core/currencyCloud/index.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _helpers = __webpack_require__(/*! ../../helpers */ \"./src/helpers/index.js\");\n\nvar _currencyCloud = __webpack_require__(/*! ../../client/currencyCloud.client */ \"./src/client/currencyCloud.client.js\");\n\nclass CurrencyCloud {\n  async auth() {\n    const {\n      data\n    } = await _currencyCloud.currencyCloudClient.post('/authenticate/api', {\n      login_id: (0, _helpers.getEnv)('CURRENCY_CLOUD_LOGIN'),\n      api_key: (0, _helpers.getEnv)('CURRENCY_CLOUD_API_KEY')\n    });\n    return data.auth_token;\n  }\n\n  async findBankDetails(identifierType = '', identifierValue = '') {\n    let Type = identifierType.trim();\n    let Value = identifierValue.trim();\n    if (!Type) throw new Error('identifierType is required');\n    if (!Value) throw new Error('identifierValue is required');\n    Type = Type.toLowerCase();\n    Value = Value.toUpperCase();\n    const validTypes = {\n      iban: item => item.match(/^[A-Z]{2}[A-Z|0-9]{6}[0-9]{9,28}$/),\n      bic_swift: item => item.match(/^[A-Z|0-9]{4}[A-Z]{2}[A-Z|0-9]{5}$|^[A-Z|0-9]{4}[A-Z]{2}[A-Z|0-9]{2}$/),\n      sort_code: item => /^[0-9]{6}$/.test(item),\n      aba: item => /^[0-9]{9}$/.test(item),\n      routing_number: item => /^[0-9]{9}$/.test(item),\n      bsb_code: item => item,\n      cnaps: item => item\n    };\n\n    if (!Object.keys(validTypes).includes(Type)) {\n      throw new Error('identifierType is invalid');\n    }\n\n    if (!validTypes[Type](Value)) {\n      throw new Error(`value ${identifierValue} for ${Type} is invalid`);\n    }\n\n    try {\n      var _data$bic_swift;\n\n      const token = await this.auth();\n      const {\n        data\n      } = await _currencyCloud.currencyCloudClient.get(`/reference/bank_details?identifier_type=${Type}&identifier_value=${identifierValue}`, {\n        headers: {\n          'X-Auth-Token': token\n        }\n      });\n      const formatData = {\n        branchDetails: [{\n          bankName: data.bank_name,\n          street: data.bank_address,\n          state: data.bank_state,\n          codeDetails: {\n            codeValue1: data.bic_swift,\n            codeValue2: '',\n            codeValue3: (_data$bic_swift = data.bic_swift) === null || _data$bic_swift === void 0 ? void 0 : _data$bic_swift.substr(0, 4),\n            codeValue4: ''\n          }\n        }]\n      };\n      return formatData;\n    } catch (e) {\n      console.log(e);\n      throw e;\n    }\n  }\n\n}\n\nconst currencyCloud = new CurrencyCloud();\nvar _default = currencyCloud;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/core/currencyCloud/index.js?");

/***/ }),

/***/ "./src/core/shuftiPro/index.js":
/*!*************************************!*\
  !*** ./src/core/shuftiPro/index.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _lodash = _interopRequireDefault(__webpack_require__(/*! lodash */ \"lodash\"));\n\nvar _helpers = __webpack_require__(/*! ../../helpers */ \"./src/helpers/index.js\");\n\nvar _shuftipro = __webpack_require__(/*! ../../client/shuftipro.client */ \"./src/client/shuftipro.client.js\");\n\nvar _models = __webpack_require__(/*! ../../models */ \"./src/models/index.js\");\n\nvar _utils = __webpack_require__(/*! ../../helpers/utils */ \"./src/helpers/utils/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nclass ShuftiPro {\n  constructor() {\n    this.callbackUrlPrefix = `${(0, _helpers.getEnv)('APP_URL_API')}/shuftipro-callback`;\n    this.feAppUrl = `${(0, _helpers.getEnv)('FE_APP_URL')}`;\n    this.appName = `${(0, _helpers.getEnv)('APP_NAME')}`;\n  }\n\n  _createPayload(action, reference, data) {\n    const actions = {\n      verificationCustomer({\n        firstName = '',\n        lastName = '',\n        email = '',\n        documents = {}\n      }) {\n        const payload = {\n          callback_url: `${this.callbackUrlPrefix}/verification-customer`,\n          verification_mode: 'image_only',\n          email,\n          allow_offline: '1',\n          show_privacy_policy: '1',\n          show_results: '1',\n          show_consent: '0',\n          decline_on_single_step: '0',\n          reference\n        };\n\n        if (documents.document) {\n          payload.document = {\n            proof: (0, _utils.convertToBase64)(documents.document.path),\n            supported_types: ['id_card', 'passport'],\n            name: {\n              first_name: firstName,\n              last_name: lastName\n            },\n            allow_offline: '1',\n            fetch_enhanced_data: '1',\n            backside_proof_required: '0'\n          };\n        }\n\n        if (documents.face) {\n          payload.face = {\n            proof: (0, _utils.convertToBase64)(documents.face.path),\n            allow_offline: '1'\n          };\n        }\n\n        if (documents.address) {\n          payload.address = {\n            proof: (0, _utils.convertToBase64)(documents.address.path),\n            supported_types: ['id_card', 'passport', 'driving_license'],\n            address_fuzzy_match: '1',\n            allow_offline: '1'\n          };\n        }\n\n        return payload;\n      }\n\n    }[action];\n\n    if (!actions) {\n      throw new _helpers.APIError('Action not found');\n    }\n\n    return actions.call(this, data);\n  }\n\n  createPayloadEmailError(data, action = 'verificationCustomer') {\n    return {\n      verificationCustomer: {\n        templateName: 'shuftipro-verification-error',\n        to: data.email,\n        params: {\n          name: `${data.firstName} ${data.lastName}`,\n          nonce: data.nonce,\n          appUrl: this.feAppUrl,\n          appName: this.appName // urlAddData: `${this.feAppUrl}?form-data=${encodeURIComponent(JSON.stringify(data))}`,\n\n        }\n      }\n    }[action];\n  }\n\n  async getDataInfoIntoShufiPro(shuftiProPayload) {\n    const {\n      data\n    } = await _shuftipro.shuftiproClient.post('/', shuftiProPayload);\n    return data;\n  }\n\n  async sendShufiPro(shuftiProPayload) {\n    const Messages = [];\n    const data = await this.getDataInfoIntoShufiPro(shuftiProPayload);\n    if (!data.verification_result) throw new Error('No verification result');\n\n    if (data.verification_result.address) {\n      Object.keys(data.verification_result.address).forEach(key => {\n        if (data.verification_result.address[key] === 0) {\n          Messages.push(key);\n        }\n      });\n    }\n\n    if (data.verification_result.document) {\n      Object.keys(data.verification_result.document).forEach(key => {\n        if (data.verification_result.document[key] === 0) {\n          Messages.push(key);\n        }\n      });\n    }\n\n    return Messages;\n  }\n\n  async _saveFiles({\n    ettAppId,\n    documents,\n    stakeholder,\n    nonce\n  }, reference) {\n    const res = {};\n    await Promise.all(Object.keys(documents).map(async key => {\n      if (typeof documents[key] === 'object') {\n        const {\n          file,\n          requirement,\n          type\n        } = documents[key];\n        const filePath = `/documents/${reference}__${process.hrtime.bigint()}___${file.name}`;\n        const serverPath = await (0, _helpers.saveFileServer)(file.path, filePath);\n\n        _lodash.default.set(res, `${key}.path`, serverPath);\n\n        _lodash.default.set(res, `${key}.name`, file.name || '');\n\n        const ettCustomerDocument = await _models.EttCustomerDocument.create({\n          ettAppId,\n          name: file.name,\n          stakeholder: +stakeholder,\n          ettRequirementData: {\n            id: +requirement.id,\n            name: requirement.name,\n            description: requirement.description\n          },\n          ettTypeData: {\n            id: +type.id,\n            name: type.name,\n            description: type.description\n          },\n          reference,\n          path: serverPath,\n          nonce\n        });\n\n        _lodash.default.set(res, `${key}.docId`, ettCustomerDocument._id);\n      }\n\n      return key;\n    }));\n    return res;\n  }\n\n  async sendVerification(data = {}, action = 'verificationCustomer') {\n    const reference = `SP_REQUEST_${Math.random()}`;\n    const saveFileData = { ...data,\n      documents: await this._saveFiles(data, reference)\n    };\n\n    const payload = this._createPayload(action, reference, saveFileData);\n\n    const shuftiMessages = await this.sendShufiPro(payload);\n\n    if (shuftiMessages.length) {\n      const errorEmailPayload = this.createPayloadEmailError(saveFileData, action);\n      const errorMessages = shuftiMessages.map(msg => `${msg.replace(/_/g, ' ')} is invalid\\n`);\n      await (0, _helpers.sendMail)({ ...errorEmailPayload,\n        params: { ...errorEmailPayload.params,\n          errorMessage: errorMessages.join('')\n        }\n      });\n    }\n\n    return shuftiMessages;\n  }\n\n}\n\nexports.default = ShuftiPro;\n\n//# sourceURL=webpack://blocfx-backend/./src/core/shuftiPro/index.js?");

/***/ }),

/***/ "./src/helpers/config/index.js":
/*!*************************************!*\
  !*** ./src/helpers/config/index.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.isProduction = exports.getEnv = void 0;\n\nvar _errors = __webpack_require__(/*! ../errors */ \"./src/helpers/errors/index.js\");\n\nconst config = { ...{\"PORT\":\"9000\",\"APP_NAME\":\"BLOC_FX\",\"FE_APP_URL\":\"https://blockfx-fe.j.layershift.co.uk\",\"APP_URL_API\":\"http://localhost:3000/api/v1\",\"DB_CONNECTION\":\"mongodb+srv://admin:root@cluster0.76jcc.mongodb.net/block-fx?retryWrites=true&w=majority\",\"BASE_API_URL\":\"https://jigzo-sandbox.ett-global.com/api\",\"BASE_API_KEY\":\"fc9dcbf1cdf9dee807a37715de24f844\",\"BASE_API_SECRET\":\"38318561b8910f2353276e697c5f64e5\",\"BACK_OFFICE_URL\":\"https://jigzo-sandbox.ett-global.com\",\"BACK_OFFICE_USERNAME\":\"jigzo\",\"BACK_OFFICE_PASSWORD\":\"jigzo567\",\"FROM_MAIL\":\"noreply@super.cx\",\"MAIL_HOST\":\"smtp.mailtrap.io\",\"MAIL_PORT\":\"2525\",\"MAIL_USERNAME\":\"efe84d2380a130\",\"MAIL_PASSWORD\":\"fd931301288371\",\"CLICKSEND_API_URL\":\"https://rest.clicksend.com/v3\",\"CLICKSEND_API_KEY\":\"CE04E40E-A87F-EFA1-1A0F-CE37DD17337F\",\"CLICKSEND_USERNAIM\":\"elasmar@jigzo.com\",\"APPLY_API_URL\":\"https://apps.applyfinancial.co.uk/validate-api/rest/\",\"APPLY_USERNAME\":\"denys@jigzo.com\",\"APPLY_PASSWORD\":\"5+U*r}Dcj@/x4&'[\",\"SHUFTIPRO_API_URL\":\"https://api.shuftipro.com\",\"SHUFTIPRO_CLIENT_ID\":\"fHDPZ1sgHTVtKfnMf5vnBxOXCmjXYBSH2QUVVYarf6TsispuiL1573826568\",\"SHUFTIPRO_SECRET_KEY\":\"BN7vc6dX3mnj8fwO9frjCaURYmrdx1YG\",\"JWT_AUTHORIZATION_SECRET\":\"xv4czTwnfpCTbOpripI1RDthaWlZVX8aNVHFvKNmvs0kRoDGpk06Hv3G11Ilp8t\",\"CORS_WHITE_LIST\":\"https://blockfx-fe.j.layershift.co.uk,https://sandboxblocfxbackend.j.layershift.co.uk,http://109.109.135.108\",\"CURRENCY_CLOUD_API_URL\":\"https://devapi.currencycloud.com/v2\",\"CURRENCY_CLOUD_LOGIN\":\"denys@jigzo.com\",\"CURRENCY_CLOUD_API_KEY\":\"1bc48f1d0b5af61fe1f879935306dcbc15b3cf05d118a6ad27a12f2249effe70\",\"ZENDESK_API_URL\":\"https://jigzo2128.zendesk.com/api/v2\",\"INTERCASH_API_URL\":\"https://www.prepaidgate.com/ppRequestTest/service1.asmx?WSDL\",\"INTERCASH_LOGIN\":\"em.ict.eur\",\"INTERCASH_PASS\":\"gIC.t10.eur\",\"INTERCASH_XMLNS\":\"https://www.prepaidgate.com/ppRequestTest/\"},\n  NODE_ENV: \"development\" || 0,\n  PORT: {\"PORT\":\"9000\",\"APP_NAME\":\"BLOC_FX\",\"FE_APP_URL\":\"https://blockfx-fe.j.layershift.co.uk\",\"APP_URL_API\":\"http://localhost:3000/api/v1\",\"DB_CONNECTION\":\"mongodb+srv://admin:root@cluster0.76jcc.mongodb.net/block-fx?retryWrites=true&w=majority\",\"BASE_API_URL\":\"https://jigzo-sandbox.ett-global.com/api\",\"BASE_API_KEY\":\"fc9dcbf1cdf9dee807a37715de24f844\",\"BASE_API_SECRET\":\"38318561b8910f2353276e697c5f64e5\",\"BACK_OFFICE_URL\":\"https://jigzo-sandbox.ett-global.com\",\"BACK_OFFICE_USERNAME\":\"jigzo\",\"BACK_OFFICE_PASSWORD\":\"jigzo567\",\"FROM_MAIL\":\"noreply@super.cx\",\"MAIL_HOST\":\"smtp.mailtrap.io\",\"MAIL_PORT\":\"2525\",\"MAIL_USERNAME\":\"efe84d2380a130\",\"MAIL_PASSWORD\":\"fd931301288371\",\"CLICKSEND_API_URL\":\"https://rest.clicksend.com/v3\",\"CLICKSEND_API_KEY\":\"CE04E40E-A87F-EFA1-1A0F-CE37DD17337F\",\"CLICKSEND_USERNAIM\":\"elasmar@jigzo.com\",\"APPLY_API_URL\":\"https://apps.applyfinancial.co.uk/validate-api/rest/\",\"APPLY_USERNAME\":\"denys@jigzo.com\",\"APPLY_PASSWORD\":\"5+U*r}Dcj@/x4&'[\",\"SHUFTIPRO_API_URL\":\"https://api.shuftipro.com\",\"SHUFTIPRO_CLIENT_ID\":\"fHDPZ1sgHTVtKfnMf5vnBxOXCmjXYBSH2QUVVYarf6TsispuiL1573826568\",\"SHUFTIPRO_SECRET_KEY\":\"BN7vc6dX3mnj8fwO9frjCaURYmrdx1YG\",\"JWT_AUTHORIZATION_SECRET\":\"xv4czTwnfpCTbOpripI1RDthaWlZVX8aNVHFvKNmvs0kRoDGpk06Hv3G11Ilp8t\",\"CORS_WHITE_LIST\":\"https://blockfx-fe.j.layershift.co.uk,https://sandboxblocfxbackend.j.layershift.co.uk,http://109.109.135.108\",\"CURRENCY_CLOUD_API_URL\":\"https://devapi.currencycloud.com/v2\",\"CURRENCY_CLOUD_LOGIN\":\"denys@jigzo.com\",\"CURRENCY_CLOUD_API_KEY\":\"1bc48f1d0b5af61fe1f879935306dcbc15b3cf05d118a6ad27a12f2249effe70\",\"ZENDESK_API_URL\":\"https://jigzo2128.zendesk.com/api/v2\",\"INTERCASH_API_URL\":\"https://www.prepaidgate.com/ppRequestTest/service1.asmx?WSDL\",\"INTERCASH_LOGIN\":\"em.ict.eur\",\"INTERCASH_PASS\":\"gIC.t10.eur\",\"INTERCASH_XMLNS\":\"https://www.prepaidgate.com/ppRequestTest/\"}.PORT || 3000\n};\n\nconst getEnv = key => {\n  if (!key) return '';\n\n  if (!config[key]) {\n    throw new _errors.APIError(`Environment variable ${key} should be specified`);\n  }\n\n  return config[key];\n};\n\nexports.getEnv = getEnv;\nconst isProduction = getEnv('NODE_ENV') === 'production';\nexports.isProduction = isProduction;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/config/index.js?");

/***/ }),

/***/ "./src/helpers/db/index.js":
/*!*********************************!*\
  !*** ./src/helpers/db/index.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.connectDb = void 0;\n\nvar _mongoose = _interopRequireDefault(__webpack_require__(/*! mongoose */ \"mongoose\"));\n\nvar _config = __webpack_require__(/*! ../config */ \"./src/helpers/config/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst connectDb = async () => {\n  await _mongoose.default.connect((0, _config.getEnv)('DB_CONNECTION'), {\n    useNewUrlParser: true,\n    useUnifiedTopology: true,\n    useCreateIndex: true,\n    useFindAndModify: false\n  });\n};\n\nexports.connectDb = connectDb;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/db/index.js?");

/***/ }),

/***/ "./src/helpers/errors/APIError.js":
/*!****************************************!*\
  !*** ./src/helpers/errors/APIError.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\n// @ts-check\nclass APIError extends Error {\n  /**\n   * @param {string} message\n   * @param {number} status\n   */\n  constructor(message, status = 500) {\n    super(message);\n    this.name = this.constructor.name;\n    this.status = status;\n\n    if (typeof Error.captureStackTrace === 'function') {\n      Error.captureStackTrace(this, this.constructor);\n    } else {\n      this.stack = new Error(message).stack;\n    }\n  }\n\n}\n\nvar _default = APIError;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/errors/APIError.js?");

/***/ }),

/***/ "./src/helpers/errors/errorMessages.js":
/*!*********************************************!*\
  !*** ./src/helpers/errors/errorMessages.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n// @ts-check\nvar _default = {\n  emptyToken: 'Access denied. No token provided.',\n  invalidToken: 'Access denied. Token is invalid',\n  shuftiProSignature: 'ShuftiPro signature error',\n  applyError: 'Apply response error'\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/errors/errorMessages.js?");

/***/ }),

/***/ "./src/helpers/errors/index.js":
/*!*************************************!*\
  !*** ./src/helpers/errors/index.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"APIError\", ({\n  enumerable: true,\n  get: function () {\n    return _APIError.default;\n  }\n}));\nObject.defineProperty(exports, \"errorMessages\", ({\n  enumerable: true,\n  get: function () {\n    return _errorMessages.default;\n  }\n}));\n\nvar _APIError = _interopRequireDefault(__webpack_require__(/*! ./APIError */ \"./src/helpers/errors/APIError.js\"));\n\nvar _errorMessages = _interopRequireDefault(__webpack_require__(/*! ./errorMessages */ \"./src/helpers/errors/errorMessages.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/errors/index.js?");

/***/ }),

/***/ "./src/helpers/generator/index.js":
/*!****************************************!*\
  !*** ./src/helpers/generator/index.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.getRandomStringNumber = void 0;\n\nconst getMinMax = (start = -Infinity, end = Infinity) => start > end ? [end, start] : [start, end];\n\nconst getRandom = (start, end = 0, floor = 0) => {\n  const [minimum, maximum] = getMinMax(start, end);\n  const range = maximum + 1 - minimum;\n  const coefficient = 10 ** floor;\n  return Math.floor(Math.random() * range * coefficient / coefficient) + minimum;\n};\n\nconst getRandomStringNumber = (start, end) => {\n  const randomNumber = getRandom(start, end);\n  const randomString = randomNumber.toString();\n  const max = Math.max(start, end);\n  const stringZero = '0'.repeat(max.toString().length);\n  return `${stringZero.substr(randomString.length, stringZero.length - randomString.length)}${randomString}`;\n};\n\nexports.getRandomStringNumber = getRandomStringNumber;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/generator/index.js?");

/***/ }),

/***/ "./src/helpers/index.js":
/*!******************************!*\
  !*** ./src/helpers/index.js ***!
  \******************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"getEnv\", ({\n  enumerable: true,\n  get: function () {\n    return _config.getEnv;\n  }\n}));\nObject.defineProperty(exports, \"isProduction\", ({\n  enumerable: true,\n  get: function () {\n    return _config.isProduction;\n  }\n}));\nObject.defineProperty(exports, \"APIError\", ({\n  enumerable: true,\n  get: function () {\n    return _errors.APIError;\n  }\n}));\nObject.defineProperty(exports, \"errorMessages\", ({\n  enumerable: true,\n  get: function () {\n    return _errors.errorMessages;\n  }\n}));\nObject.defineProperty(exports, \"createHashSha1\", ({\n  enumerable: true,\n  get: function () {\n    return _utils.createHashSha1;\n  }\n}));\nObject.defineProperty(exports, \"convertToBase64\", ({\n  enumerable: true,\n  get: function () {\n    return _utils.convertToBase64;\n  }\n}));\nObject.defineProperty(exports, \"saveFileServer\", ({\n  enumerable: true,\n  get: function () {\n    return _utils.saveFileServer;\n  }\n}));\nObject.defineProperty(exports, \"deleteFileServer\", ({\n  enumerable: true,\n  get: function () {\n    return _utils.deleteFileServer;\n  }\n}));\nObject.defineProperty(exports, \"successResponse\", ({\n  enumerable: true,\n  get: function () {\n    return _response.successResponse;\n  }\n}));\nObject.defineProperty(exports, \"errorResponse\", ({\n  enumerable: true,\n  get: function () {\n    return _response.errorResponse;\n  }\n}));\nObject.defineProperty(exports, \"connectDb\", ({\n  enumerable: true,\n  get: function () {\n    return _db.connectDb;\n  }\n}));\nObject.defineProperty(exports, \"sendMail\", ({\n  enumerable: true,\n  get: function () {\n    return _sendMail.default;\n  }\n}));\n\nvar _config = __webpack_require__(/*! ./config */ \"./src/helpers/config/index.js\");\n\nvar _errors = __webpack_require__(/*! ./errors */ \"./src/helpers/errors/index.js\");\n\nvar _utils = __webpack_require__(/*! ./utils */ \"./src/helpers/utils/index.js\");\n\nvar _response = __webpack_require__(/*! ./response */ \"./src/helpers/response/index.js\");\n\nvar _db = __webpack_require__(/*! ./db */ \"./src/helpers/db/index.js\");\n\nvar _sendMail = _interopRequireDefault(__webpack_require__(/*! ./sendMail */ \"./src/helpers/sendMail/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/index.js?");

/***/ }),

/***/ "./src/helpers/parser/index.js":
/*!*************************************!*\
  !*** ./src/helpers/parser/index.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"purposeParser\", ({\n  enumerable: true,\n  get: function () {\n    return _purposes.default;\n  }\n}));\n\nvar _purposes = _interopRequireDefault(__webpack_require__(/*! ./purposes */ \"./src/helpers/parser/purposes.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/parser/index.js?");

/***/ }),

/***/ "./src/helpers/parser/purposes.js":
/*!****************************************!*\
  !*** ./src/helpers/parser/purposes.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _default = html => {\n  const groupPurposes = html.match(/<ul id=\"paymentPurposeSortable\"[^`]+<\\/ul>/)[0];\n  const elementsPurposes = groupPurposes.replace(/<ul[^>]+>/, '').replace('</ul>', '');\n  const listPurposes = elementsPurposes.split('</li>').reduce((accumulator, element) => {\n    const correctElement = element.trim().replace(/<li[^>]+>/, '');\n    if (correctElement) accumulator.push(correctElement);\n    return accumulator;\n  }, []);\n  return listPurposes;\n};\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/parser/purposes.js?");

/***/ }),

/***/ "./src/helpers/parser/zendesk.js":
/*!***************************************!*\
  !*** ./src/helpers/parser/zendesk.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.getLastMessageText = exports.getMessageTextIntoMessage = exports.getLastMessage = exports.getMetaDataIntoZendesk = exports.getMetaDataString = void 0;\n\nconst getMetaDataString = (text = '', metaDataName = '') => {\n  var _text$match;\n\n  const regexp = new RegExp(`- ${metaDataName} +: +[^-]+-`);\n  const foundString = ((_text$match = text.match(regexp)) === null || _text$match === void 0 ? void 0 : _text$match[0]) || '';\n  return foundString;\n};\n\nexports.getMetaDataString = getMetaDataString;\n\nconst getMetaDataIntoZendesk = (text = '', metaDataName = '') => {\n  const metadata = getMetaDataString(text, metaDataName);\n  const rawData = metadata.replace(new RegExp(`- ${metaDataName} +: +`), '');\n  const data = rawData.substr(0, rawData.length - 1).trim();\n  return data;\n};\n\nexports.getMetaDataIntoZendesk = getMetaDataIntoZendesk;\n\nconst getLastMessage = (text = '') => {\n  var _text$match2;\n\n  const rawData = ((_text$match2 = text.match(/<comments_formatted>-{3,100}[^-]+/)) === null || _text$match2 === void 0 ? void 0 : _text$match2[0]) || '';\n  const data = rawData.replace(/<comments_formatted>-{3,100}/, '');\n  return data;\n};\n\nexports.getLastMessage = getLastMessage;\n\nconst getMessageTextIntoMessage = (message = '') => message.replace(/[a-zA-Z ]+, [a-zA-Z]{3} [0-9]{1,2}, [0-9]{4}, +[0-9]{1,2}:+[0-9]{1,2}/, '').trim();\n\nexports.getMessageTextIntoMessage = getMessageTextIntoMessage;\n\nconst getLastMessageText = (text = '') => {\n  const message = getLastMessage(text);\n  const messageText = getMessageTextIntoMessage(message);\n  return messageText;\n};\n\nexports.getLastMessageText = getLastMessageText;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/parser/zendesk.js?");

/***/ }),

/***/ "./src/helpers/regexp/index.js":
/*!*************************************!*\
  !*** ./src/helpers/regexp/index.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nconst regexp = {\n  accountNumber: /^[A-Z]{2}[A-Z|0-9]{6}[0-9]{9,28}$|^[0-9]{7,24}$|^[A-Z]{4}[0-9]{7,24}$/,\n  iban: /^[A-Z]{2}[A-Z|0-9]{6}[0-9]{9,28}$/,\n  bic: /^[A-Z|0-9]{4}[A-Z]{2}[A-Z|0-9]{5}$|^[A-Z|0-9]{4}[A-Z]{2}[A-Z|0-9]{2}$/,\n  sortCode: /^[0-9]{6}$/,\n  aba: /^[0-9]{9}$/,\n  countryCode: /^[A-Z]{2}$/,\n  currencyCode: /^[A-Z]{3}$/,\n  email: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$/,\n  phone: /\\+[0-9]{10,17}$/,\n  digitalCode: /^[0-9]{6}$/\n};\nvar _default = regexp;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/regexp/index.js?");

/***/ }),

/***/ "./src/helpers/response/bankValidator.js":
/*!***********************************************!*\
  !*** ./src/helpers/response/bankValidator.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.bankValidator = void 0;\n\nvar _country = __webpack_require__(/*! ../static/country */ \"./src/helpers/static/country.js\");\n\nconst bankValidator = (req, res) => {\n  const {\n    iban = '',\n    accountNumber = '',\n    bic = '',\n    aba = '',\n    sortCode = '',\n    country = '',\n    isLocal = false\n  } = req.body;\n\n  const sendErr = message => res.status(403).json({\n    message\n  });\n\n  if (!country.match(/^[A-Z]{2}$/)) {\n    return sendErr('country code is invalid');\n  }\n\n  const checkCountry = (countryCode = '') => !!_country.countries.find(({\n    code\n  }) => code === countryCode);\n\n  if (!checkCountry(country)) {\n    return sendErr('country is not found');\n  } // check default params\n\n\n  if (!(iban || accountNumber) || !(bic || iban || aba && country === 'US' || sortCode && country === 'GB')) {\n    return sendErr('This response to be empty required parameters');\n  }\n\n  const countryBic = bic.substr(4, 2);\n  const countryIban = iban.substr(0, 2); // check IBAN\n\n  if (iban) {\n    if (!iban.match(/^[A-Z]{2}[A-Z|0-9]{6}[0-9]{9,28}$/)) {\n      return sendErr('iban is invalid');\n    }\n\n    if (countryIban !== country) {\n      if (!checkCountry(countryIban)) {\n        return sendErr('iban country is invalid');\n      }\n\n      if (bic) {\n        if (countryBic === country) {\n          return sendErr('iban country is invalid');\n        }\n\n        if (countryBic === countryIban) {\n          return sendErr('country is invalid');\n        }\n      }\n\n      return sendErr('country or iban is invalid');\n    }\n  } // check BIC\n\n\n  if (bic) {\n    if (!bic.match(/^[A-Z|0-9]{4}[A-Z]{2}[A-Z|0-9]{5}$|^[A-Z|0-9]{4}[A-Z]{2}[A-Z|0-9]{2}$/)) {\n      return sendErr('bic code is invalid');\n    }\n\n    if (countryBic !== country) {\n      if (!checkCountry(countryBic)) {\n        return sendErr('bic country is invalid');\n      }\n\n      if (iban) {\n        if (countryIban === country) {\n          return sendErr('bic country is invalid');\n        }\n\n        if (countryIban === countryBic) {\n          return sendErr('country is invalid');\n        }\n      }\n\n      return sendErr('country or bic is invalid');\n    }\n  } // check account-number\n\n\n  if (accountNumber) {\n    if (!accountNumber.match(/^[A-Z]{2}[A-Z|0-9]{6}[0-9]{9,28}$|^[0-9]{7,24}$|^[A-Z]{4}[0-9]{7,24}$/)) {\n      return sendErr('accountNumber is invalid');\n    }\n\n    if (accountNumber.match(/^[A-Z]{2}[A-Z|0-9]{6}[0-9]{9,28}$/)) {\n      const countryIntoAcc = accountNumber.substr(0, 2);\n\n      if (countryIntoAcc !== country) {\n        if (!checkCountry(countryIntoAcc)) {\n          return sendErr('country code into accountNumber is invalid');\n        }\n\n        return sendErr('country code into accountNumber or country is invalid');\n      }\n    }\n  } // check other-code (only local)\n\n\n  if (isLocal) {\n    switch (country) {\n      case 'GB':\n        if (!sortCode.match(/^[0-9]{6}$/)) {\n          return sendErr('sort-code is invalid');\n        }\n\n        break;\n\n      case 'US':\n        if (!aba.match(/^[0-9]{9}$/)) {\n          return sendErr('aba code is invalid');\n        }\n\n        break;\n\n      default:\n        return false;\n    }\n  }\n\n  return false;\n};\n\nexports.bankValidator = bankValidator;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/response/bankValidator.js?");

/***/ }),

/***/ "./src/helpers/response/error.js":
/*!***************************************!*\
  !*** ./src/helpers/response/error.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.errorResponse = void 0;\n\n// @ts-check\nconst errorResponse = (res, {\n  message = {},\n  status = 500,\n  response = {\n    status\n  },\n  ...err\n}, json = true) => {\n  let body = message;\n  const {\n    config,\n    ...otherError\n  } = err;\n\n  if (!message || json) {\n    body = {\n      message,\n      ...otherError,\n      stack: err.stack && err.stack.split('\\n')\n    };\n  }\n\n  return res.status((response === null || response === void 0 ? void 0 : response.status) || status || 500)[json ? 'json' : 'send'](body);\n};\n\nexports.errorResponse = errorResponse;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/response/error.js?");

/***/ }),

/***/ "./src/helpers/response/index.js":
/*!***************************************!*\
  !*** ./src/helpers/response/index.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"successResponse\", ({\n  enumerable: true,\n  get: function () {\n    return _success.successResponse;\n  }\n}));\nObject.defineProperty(exports, \"errorResponse\", ({\n  enumerable: true,\n  get: function () {\n    return _error.errorResponse;\n  }\n}));\nObject.defineProperty(exports, \"resourcesNotFound\", ({\n  enumerable: true,\n  get: function () {\n    return _resourcesNotFound.resourcesNotFound;\n  }\n}));\nObject.defineProperty(exports, \"bankValidator\", ({\n  enumerable: true,\n  get: function () {\n    return _bankValidator.bankValidator;\n  }\n}));\n\nvar _success = __webpack_require__(/*! ./success */ \"./src/helpers/response/success.js\");\n\nvar _error = __webpack_require__(/*! ./error */ \"./src/helpers/response/error.js\");\n\nvar _resourcesNotFound = __webpack_require__(/*! ./resources-not-found */ \"./src/helpers/response/resources-not-found.js\");\n\nvar _bankValidator = __webpack_require__(/*! ./bankValidator */ \"./src/helpers/response/bankValidator.js\");\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/response/index.js?");

/***/ }),

/***/ "./src/helpers/response/resources-not-found.js":
/*!*****************************************************!*\
  !*** ./src/helpers/response/resources-not-found.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.resourcesNotFound = void 0;\n\nconst resourcesNotFound = (res, message = 'Resources Not Found') => res.status(404).json({\n  message\n});\n\nexports.resourcesNotFound = resourcesNotFound;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/response/resources-not-found.js?");

/***/ }),

/***/ "./src/helpers/response/success.js":
/*!*****************************************!*\
  !*** ./src/helpers/response/success.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.successResponse = void 0;\n\nconst successResponse = (res, payload, status = 200, json = true) => res.status(status)[json ? 'json' : 'send'](payload);\n\nexports.successResponse = successResponse;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/response/success.js?");

/***/ }),

/***/ "./src/helpers/sendMail/index.js":
/*!***************************************!*\
  !*** ./src/helpers/sendMail/index.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _process = _interopRequireDefault(__webpack_require__(/*! process */ \"process\"));\n\nvar _fs = __webpack_require__(/*! fs */ \"fs\");\n\nvar _path = __webpack_require__(/*! path */ \"path\");\n\nvar _nodemailer = __webpack_require__(/*! nodemailer */ \"nodemailer\");\n\nvar _handlebars = __webpack_require__(/*! handlebars */ \"handlebars\");\n\nvar _config = __webpack_require__(/*! ../config */ \"./src/helpers/config/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst transporter = (0, _nodemailer.createTransport)({\n  host: (0, _config.getEnv)('MAIL_HOST'),\n  port: (0, _config.getEnv)('MAIL_PORT'),\n  auth: {\n    user: (0, _config.getEnv)('MAIL_USERNAME'),\n    pass: (0, _config.getEnv)('MAIL_PASSWORD')\n  }\n});\n\nvar _default = ({\n  from = (0, _config.getEnv)('FROM_MAIL'),\n  to = '',\n  subject = 'blockFx',\n  text = '',\n  templateName = '',\n  params = {}\n}) => new Promise((resolve, reject) => (0, _fs.readFile)((0, _path.join)(`${_process.default.env.PWD}/public/template/${templateName}.handlebars`), 'utf-8', (error, data) => {\n  if (error) return reject(error);\n  const hbs = (0, _handlebars.compile)(data);\n  return transporter.sendMail({\n    from,\n    to,\n    subject,\n    text,\n    html: hbs(params)\n  }).then(dataMail => resolve(dataMail)).catch(errorMail => reject(errorMail));\n}));\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/sendMail/index.js?");

/***/ }),

/***/ "./src/helpers/static/country.js":
/*!***************************************!*\
  !*** ./src/helpers/static/country.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.countries = void 0;\n// @ts-check\nconst countries = [{\n  name: 'Afghanistan',\n  code: 'AF',\n  IBAN: false,\n  id: 1,\n  dialCode: '+93'\n}, {\n  name: 'Albania',\n  code: 'AL',\n  IBAN: false,\n  id: 5,\n  dialCode: '+355'\n}, {\n  name: 'Algeria',\n  code: 'DZ',\n  IBAN: false,\n  id: 65,\n  dialCode: '+213'\n}, {\n  name: 'American Samoa',\n  code: 'AS',\n  IBAN: false,\n  id: 10,\n  dialCode: '+1684'\n}, {\n  name: 'Andorra',\n  code: 'AD',\n  IBAN: true,\n  id: 6,\n  dialCode: '+376'\n}, {\n  name: 'Angola',\n  code: 'AO',\n  IBAN: false,\n  id: 2,\n  dialCode: '+244'\n}, {\n  name: 'Anguilla',\n  code: 'AI',\n  IBAN: false,\n  id: 3,\n  dialCode: '+1264'\n}, {\n  name: 'Antarctica',\n  code: 'AQ',\n  IBAN: false,\n  id: 11,\n  dialCode: '+672'\n}, {\n  name: 'Antigua and Barbuda',\n  code: 'AG',\n  IBAN: false,\n  id: 13,\n  dialCode: '+1268'\n}, {\n  name: 'Argentina',\n  code: 'AR',\n  IBAN: false,\n  id: 8,\n  dialCode: '+54'\n}, {\n  name: 'Armenia',\n  code: 'AM',\n  IBAN: false,\n  id: 9,\n  dialCode: '+374'\n}, {\n  name: 'Aruba',\n  code: 'AW',\n  IBAN: false,\n  id: 0,\n  dialCode: '+297'\n}, {\n  name: 'Australia',\n  code: 'AU',\n  IBAN: false,\n  id: 14,\n  dialCode: '+61'\n}, {\n  name: 'Austria',\n  code: 'AT',\n  IBAN: true,\n  id: 15,\n  dialCode: '+43'\n}, {\n  name: 'Azerbaijan',\n  code: 'AZ',\n  IBAN: true,\n  id: 16,\n  dialCode: '+994'\n}, {\n  name: 'Bahamas',\n  code: 'BS',\n  IBAN: false,\n  id: 24,\n  dialCode: '+1242'\n}, {\n  name: 'Bahrain',\n  code: 'BH',\n  IBAN: true,\n  id: 23,\n  dialCode: '+973'\n}, {\n  name: 'Bangladesh',\n  code: 'BD',\n  IBAN: false,\n  id: 21,\n  dialCode: '+880'\n}, {\n  name: 'Barbados',\n  code: 'BB',\n  IBAN: false,\n  id: 34,\n  dialCode: '+1246'\n}, {\n  name: 'Belarus',\n  code: 'BY',\n  IBAN: true,\n  id: 28,\n  dialCode: '+375'\n}, {\n  name: 'Belgium',\n  code: 'BE',\n  IBAN: true,\n  id: 18,\n  dialCode: '+32'\n}, {\n  name: 'Belize',\n  code: 'BZ',\n  IBAN: false,\n  id: 29,\n  dialCode: '+501'\n}, {\n  name: 'Benin',\n  code: 'BJ',\n  IBAN: false,\n  id: 19,\n  dialCode: '+229'\n}, {\n  name: 'Bermuda',\n  code: 'BM',\n  IBAN: false,\n  id: 30,\n  dialCode: '+1441'\n}, {\n  name: 'Bhutan',\n  code: 'BT',\n  IBAN: false,\n  id: 36,\n  dialCode: '+975'\n}, {\n  name: 'Bolivia',\n  code: 'BO',\n  IBAN: false,\n  id: 31,\n  dialCode: '+591'\n}, {\n  name: 'Bosnia and Herzegovina',\n  code: 'BA',\n  IBAN: true,\n  id: 25,\n  dialCode: '+387'\n}, {\n  name: 'Botswana',\n  code: 'BW',\n  IBAN: false,\n  id: 38,\n  dialCode: '+267'\n}, {\n  name: 'Bouvet Island',\n  code: 'BV',\n  IBAN: false,\n  id: 37\n}, {\n  name: 'Brazil',\n  code: 'BR',\n  IBAN: true,\n  id: 33,\n  dialCode: '+55'\n}, {\n  name: 'British Indian Ocean Territory',\n  code: 'IO',\n  IBAN: false,\n  id: 106,\n  dialCode: '+246'\n}, {\n  name: 'British Virgin Islands',\n  code: 'VG',\n  IBAN: true,\n  id: 240,\n  dialCode: '+1284'\n}, {\n  name: 'Brunei',\n  code: 'BN',\n  IBAN: false,\n  id: 35,\n  dialCode: '+673'\n}, {\n  name: 'Bulgaria',\n  code: 'BG',\n  IBAN: true,\n  id: 22,\n  dialCode: '+359'\n}, {\n  name: 'Burkina Faso',\n  code: 'BF',\n  IBAN: false,\n  id: 20,\n  dialCode: '+226'\n}, {\n  name: 'Burundi',\n  code: 'BI',\n  IBAN: false,\n  id: 17,\n  dialCode: '+257'\n}, {\n  name: 'Cambodia',\n  code: 'KH',\n  IBAN: false,\n  id: 120,\n  dialCode: '+855'\n}, {\n  name: 'Cameroon',\n  code: 'CM',\n  IBAN: false,\n  id: 46,\n  dialCode: '+237'\n}, {\n  name: 'Canada',\n  code: 'CA',\n  IBAN: false,\n  id: 40,\n  dialCode: '+1'\n}, {\n  name: 'Cape Verde',\n  code: 'CV',\n  IBAN: false,\n  id: 52,\n  dialCode: '+238'\n}, {\n  name: 'Caribbean Netherlands',\n  code: 'BQ',\n  IBAN: false,\n  id: 32\n}, {\n  name: 'Cayman Islands',\n  code: 'KY',\n  IBAN: false,\n  id: 57,\n  dialCode: '+ 345'\n}, {\n  name: 'Central African Republic',\n  code: 'CF',\n  IBAN: false,\n  id: 39,\n  dialCode: '+236'\n}, {\n  name: 'Chad',\n  code: 'TD',\n  IBAN: false,\n  id: 217,\n  dialCode: '+235'\n}, {\n  name: 'Chile',\n  code: 'CL',\n  IBAN: false,\n  id: 43,\n  dialCode: '+56'\n}, {\n  name: 'China',\n  code: 'CN',\n  IBAN: false,\n  id: 44,\n  dialCode: '+86'\n}, {\n  name: 'Christmas Island',\n  code: 'CX',\n  IBAN: false,\n  id: 56,\n  dialCode: '+61'\n}, {\n  name: 'Cocos (Keeling) Islands',\n  code: 'CC',\n  IBAN: false,\n  id: 41,\n  dialCode: '+61'\n}, {\n  name: 'Colombia',\n  code: 'CO',\n  IBAN: false,\n  id: 50,\n  dialCode: '+57'\n}, {\n  name: 'Comoros',\n  code: 'KM',\n  IBAN: false,\n  id: 51,\n  dialCode: '+269'\n}, {\n  name: 'Cook Islands',\n  code: 'CK',\n  IBAN: false,\n  id: 49,\n  dialCode: '+682'\n}, {\n  name: 'Costa Rica',\n  code: 'CR',\n  IBAN: true,\n  id: 53,\n  dialCode: '+506'\n}, {\n  name: 'Croatia',\n  code: 'HR',\n  IBAN: true,\n  id: 100,\n  dialCode: '+385'\n}, {\n  name: 'Cuba',\n  code: 'CU',\n  IBAN: false,\n  id: 54,\n  dialCode: '+53'\n}, {\n  name: 'Curaçao',\n  code: 'CW',\n  IBAN: false,\n  id: 55\n}, {\n  name: 'Cyprus',\n  code: 'CY',\n  IBAN: true,\n  id: 58,\n  dialCode: '+357'\n}, {\n  name: 'Czechia',\n  code: 'CZ',\n  IBAN: true,\n  id: 59,\n  dialCode: '+420'\n}, {\n  name: 'DR Congo',\n  code: 'CD',\n  IBAN: false,\n  id: 47,\n  dialCode: '+243'\n}, {\n  name: 'Denmark',\n  code: 'DK',\n  IBAN: true,\n  id: 63,\n  dialCode: '+45'\n}, {\n  name: 'Djibouti',\n  code: 'DJ',\n  IBAN: false,\n  id: 61,\n  dialCode: '+253'\n}, {\n  name: 'Dominica',\n  code: 'DM',\n  IBAN: false,\n  id: 62,\n  dialCode: '+1767'\n}, {\n  name: 'Dominican Republic',\n  code: 'DO',\n  IBAN: true,\n  id: 64,\n  dialCode: '+1849'\n}, {\n  name: 'Ecuador',\n  code: 'EC',\n  IBAN: false,\n  id: 66,\n  dialCode: '+593'\n}, {\n  name: 'Egypt',\n  code: 'EG',\n  IBAN: false,\n  id: 67,\n  dialCode: '+20'\n}, {\n  name: 'El Salvador',\n  code: 'SV',\n  IBAN: true,\n  id: 201,\n  dialCode: '+503'\n}, {\n  name: 'Equatorial Guinea',\n  code: 'GQ',\n  IBAN: false,\n  id: 89,\n  dialCode: '+240'\n}, {\n  name: 'Eritrea',\n  code: 'ER',\n  IBAN: false,\n  id: 68,\n  dialCode: '+291'\n}, {\n  name: 'Estonia',\n  code: 'EE',\n  IBAN: true,\n  id: 71,\n  dialCode: '+372'\n}, {\n  name: 'Eswatini',\n  code: 'SZ',\n  IBAN: false,\n  id: 212,\n  dialCode: '+268'\n}, {\n  name: 'Ethiopia',\n  code: 'ET',\n  IBAN: false,\n  id: 72,\n  dialCode: '+251'\n}, {\n  name: 'Falkland Islands',\n  code: 'FK',\n  IBAN: false,\n  id: 75,\n  dialCode: '+500'\n}, {\n  name: 'Faroe Islands',\n  code: 'FO',\n  IBAN: true,\n  id: 77,\n  dialCode: '+298'\n}, {\n  name: 'Fiji',\n  code: 'FJ',\n  IBAN: false,\n  id: 74,\n  dialCode: '+679'\n}, {\n  name: 'Finland',\n  code: 'FI',\n  IBAN: true,\n  id: 73,\n  dialCode: '+358'\n}, {\n  name: 'France',\n  code: 'FR',\n  IBAN: true,\n  id: 76,\n  dialCode: '+33'\n}, {\n  name: 'French Guiana',\n  code: 'GF',\n  IBAN: false,\n  id: 94,\n  dialCode: '+594'\n}, {\n  name: 'French Polynesia',\n  code: 'PF',\n  IBAN: false,\n  id: 187,\n  dialCode: '+689'\n}, {\n  name: 'French Southern and Antarctic Lands',\n  code: 'TF',\n  IBAN: false,\n  id: 12\n}, {\n  name: 'Gabon',\n  code: 'GA',\n  IBAN: false,\n  id: 79,\n  dialCode: '+241'\n}, {\n  name: 'Gambia',\n  code: 'GM',\n  IBAN: false,\n  id: 87,\n  dialCode: '+220'\n}, {\n  name: 'Georgia',\n  code: 'GE',\n  IBAN: true,\n  id: 81,\n  dialCode: '+995'\n}, {\n  name: 'Germany',\n  code: 'DE',\n  IBAN: true,\n  id: 60,\n  dialCode: '+49'\n}, {\n  name: 'Ghana',\n  code: 'GH',\n  IBAN: false,\n  id: 83,\n  dialCode: '+233'\n}, {\n  name: 'Gibraltar',\n  code: 'GI',\n  IBAN: true,\n  id: 84,\n  dialCode: '+350'\n}, {\n  name: 'Greece',\n  code: 'GR',\n  IBAN: true,\n  id: 90,\n  dialCode: '+30'\n}, {\n  name: 'Greenland',\n  code: 'GL',\n  IBAN: true,\n  id: 92,\n  dialCode: '+299'\n}, {\n  name: 'Grenada',\n  code: 'GD',\n  IBAN: false,\n  id: 91,\n  dialCode: '+1473'\n}, {\n  name: 'Guadeloupe',\n  code: 'GP',\n  IBAN: false,\n  id: 86,\n  dialCode: '+590'\n}, {\n  name: 'Guam',\n  code: 'GU',\n  IBAN: false,\n  id: 95,\n  dialCode: '+1671'\n}, {\n  name: 'Guatemala',\n  code: 'GT',\n  IBAN: true,\n  id: 93,\n  dialCode: '+502'\n}, {\n  name: 'Guernsey',\n  code: 'GG',\n  IBAN: false,\n  id: 82,\n  dialCode: '+44'\n}, {\n  name: 'Guinea',\n  code: 'GN',\n  IBAN: false,\n  id: 85,\n  dialCode: '+224'\n}, {\n  name: 'Guinea-Bissau',\n  code: 'GW',\n  IBAN: false,\n  id: 88,\n  dialCode: '+245'\n}, {\n  name: 'Guyana',\n  code: 'GY',\n  IBAN: false,\n  id: 96,\n  dialCode: '+595'\n}, {\n  name: 'Haiti',\n  code: 'HT',\n  IBAN: false,\n  id: 101,\n  dialCode: '+509'\n}, {\n  name: 'Heard Island and McDonald Islands',\n  code: 'HM',\n  IBAN: false,\n  id: 98\n}, {\n  name: 'Honduras',\n  code: 'HN',\n  IBAN: false,\n  id: 99,\n  dialCode: '+504'\n}, {\n  name: 'Hong Kong',\n  code: 'HK',\n  IBAN: false,\n  id: 97,\n  dialCode: '+852'\n}, {\n  name: 'Hungary',\n  code: 'HU',\n  IBAN: true,\n  id: 102,\n  dialCode: '+36'\n}, {\n  name: 'Iceland',\n  code: 'IS',\n  IBAN: true,\n  id: 110,\n  dialCode: '+354'\n}, {\n  name: 'India',\n  code: 'IN',\n  IBAN: false,\n  id: 105,\n  dialCode: '+91'\n}, {\n  name: 'Indonesia',\n  code: 'ID',\n  IBAN: false,\n  id: 103,\n  dialCode: '+62'\n}, {\n  name: 'Iran',\n  code: 'IR',\n  IBAN: false,\n  id: 108,\n  dialCode: '+98'\n}, {\n  name: 'Iraq',\n  code: 'IQ',\n  IBAN: true,\n  id: 109,\n  dialCode: '+964'\n}, {\n  name: 'Ireland',\n  code: 'IE',\n  IBAN: true,\n  id: 107,\n  dialCode: '+353'\n}, {\n  name: 'Isle of Man',\n  code: 'IM',\n  IBAN: false,\n  id: 104,\n  dialCode: '+44'\n}, {\n  name: 'Israel',\n  code: 'IL',\n  IBAN: true,\n  id: 111,\n  dialCode: '+972'\n}, {\n  name: 'Italy',\n  code: 'IT',\n  IBAN: true,\n  id: 112,\n  dialCode: '+39'\n}, {\n  name: 'Ivory Coast',\n  code: 'CI',\n  IBAN: false,\n  id: 45,\n  dialCode: '+225'\n}, {\n  name: 'Jamaica',\n  code: 'JM',\n  IBAN: false,\n  id: 113,\n  dialCode: '+1876'\n}, {\n  name: 'Japan',\n  code: 'JP',\n  IBAN: false,\n  id: 116,\n  dialCode: '+81'\n}, {\n  name: 'Jersey',\n  code: 'JE',\n  IBAN: false,\n  id: 114,\n  dialCode: '+44'\n}, {\n  name: 'Jordan',\n  code: 'JO',\n  IBAN: true,\n  id: 115,\n  dialCode: '+962'\n}, {\n  name: 'Kazakhstan',\n  code: 'KZ',\n  IBAN: true,\n  id: 117,\n  dialCode: '+77'\n}, {\n  name: 'Kenya',\n  code: 'KE',\n  IBAN: false,\n  id: 118,\n  dialCode: '+254'\n}, {\n  name: 'Kiribati',\n  code: 'KI',\n  IBAN: false,\n  id: 121,\n  dialCode: '+686'\n}, {\n  name: 'Kuwait',\n  code: 'KW',\n  IBAN: true,\n  id: 125,\n  dialCode: '+965'\n}, {\n  name: 'Kyrgyzstan',\n  code: 'KG',\n  IBAN: false,\n  id: 119,\n  dialCode: '+996'\n}, {\n  name: 'Laos',\n  code: 'LA',\n  IBAN: false,\n  id: 126,\n  dialCode: '+856'\n}, {\n  name: 'Latvia',\n  code: 'LV',\n  IBAN: true,\n  id: 136,\n  dialCode: '+371'\n}, {\n  name: 'Lebanon',\n  code: 'LB',\n  IBAN: true,\n  id: 127,\n  dialCode: '+961'\n}, {\n  name: 'Lesotho',\n  code: 'LS',\n  IBAN: false,\n  id: 133,\n  dialCode: '+266'\n}, {\n  name: 'Liberia',\n  code: 'LR',\n  IBAN: false,\n  id: 128,\n  dialCode: '+231'\n}, {\n  name: 'Libya',\n  code: 'LY',\n  IBAN: false,\n  id: 129,\n  dialCode: '+218'\n}, {\n  name: 'Liechtenstein',\n  code: 'LI',\n  IBAN: true,\n  id: 131,\n  dialCode: '+423'\n}, {\n  name: 'Lithuania',\n  code: 'LT',\n  IBAN: true,\n  id: 134,\n  dialCode: '+370'\n}, {\n  name: 'Luxembourg',\n  code: 'LU',\n  IBAN: true,\n  id: 135,\n  dialCode: '+352'\n}, {\n  name: 'Macau',\n  code: 'MO',\n  IBAN: false,\n  id: 137,\n  dialCode: '+853'\n}, {\n  name: 'Madagascar',\n  code: 'MG',\n  IBAN: false,\n  id: 142,\n  dialCode: '+261'\n}, {\n  name: 'Malawi',\n  code: 'MW',\n  IBAN: false,\n  id: 158,\n  dialCode: '+265'\n}, {\n  name: 'Malaysia',\n  code: 'MY',\n  IBAN: false,\n  id: 159,\n  dialCode: '+60'\n}, {\n  name: 'Maldives',\n  code: 'MV',\n  IBAN: false,\n  id: 143,\n  dialCode: '+960'\n}, {\n  name: 'Mali',\n  code: 'ML',\n  IBAN: false,\n  id: 147,\n  dialCode: '+223'\n}, {\n  name: 'Malta',\n  code: 'MT',\n  IBAN: true,\n  id: 148,\n  dialCode: '+356'\n}, {\n  name: 'Marshall Islands',\n  code: 'MH',\n  IBAN: false,\n  id: 145,\n  dialCode: '+692'\n}, {\n  name: 'Martinique',\n  code: 'MQ',\n  IBAN: false,\n  id: 156,\n  dialCode: '+596'\n}, {\n  name: 'Mauritania',\n  code: 'MR',\n  IBAN: true,\n  id: 154,\n  dialCode: '+222'\n}, {\n  name: 'Mauritius',\n  code: 'MU',\n  IBAN: true,\n  id: 157,\n  dialCode: '+230'\n}, {\n  name: 'Mayotte',\n  code: 'YT',\n  IBAN: false,\n  id: 160,\n  dialCode: '+262'\n}, {\n  name: 'Mexico',\n  code: 'MX',\n  IBAN: false,\n  id: 144,\n  dialCode: '+52'\n}, {\n  name: 'Micronesia',\n  code: 'FM',\n  IBAN: false,\n  id: 78,\n  dialCode: '+691'\n}, {\n  name: 'Moldova',\n  code: 'MD',\n  IBAN: true,\n  id: 141,\n  dialCode: '+373'\n}, {\n  name: 'Monaco',\n  code: 'MC',\n  IBAN: true,\n  id: 140,\n  dialCode: '+377'\n}, {\n  name: 'Mongolia',\n  code: 'MN',\n  IBAN: false,\n  id: 151,\n  dialCode: '+976'\n}, {\n  name: 'Montenegro',\n  code: 'ME',\n  IBAN: true,\n  id: 150,\n  dialCode: '+382'\n}, {\n  name: 'Montserrat',\n  code: 'MS',\n  IBAN: false,\n  id: 155,\n  dialCode: '+1664'\n}, {\n  name: 'Morocco',\n  code: 'MA',\n  IBAN: false,\n  id: 139,\n  dialCode: '+212'\n}, {\n  name: 'Mozambique',\n  code: 'MZ',\n  IBAN: false,\n  id: 153,\n  dialCode: '+258'\n}, {\n  name: 'Myanmar',\n  code: 'MM',\n  IBAN: false,\n  id: 149,\n  dialCode: '+95'\n}, {\n  name: 'Namibia',\n  code: 'NA',\n  IBAN: false,\n  id: 161,\n  dialCode: '+264'\n}, {\n  name: 'Nauru',\n  code: 'NR',\n  IBAN: false,\n  id: 171,\n  dialCode: '+674'\n}, {\n  name: 'Nepal',\n  code: 'NP',\n  IBAN: false,\n  id: 170,\n  dialCode: '+977'\n}, {\n  name: 'Netherlands',\n  code: 'NL',\n  IBAN: true,\n  id: 168,\n  dialCode: '+31'\n}, {\n  name: 'New Caledonia',\n  code: 'NC',\n  IBAN: false,\n  id: 162,\n  dialCode: '+687'\n}, {\n  name: 'New Zealand',\n  code: 'NZ',\n  IBAN: false,\n  id: 172,\n  dialCode: '+64'\n}, {\n  name: 'Nicaragua',\n  code: 'NI',\n  IBAN: false,\n  id: 166,\n  dialCode: '+505'\n}, {\n  name: 'Niger',\n  code: 'NE',\n  IBAN: false,\n  id: 163,\n  dialCode: '+227'\n}, {\n  name: 'Nigeria',\n  code: 'NG',\n  IBAN: false,\n  id: 165,\n  dialCode: '+234'\n}, {\n  name: 'Niue',\n  code: 'NU',\n  IBAN: false,\n  id: 167,\n  dialCode: '+683'\n}, {\n  name: 'Norfolk Island',\n  code: 'NF',\n  IBAN: false,\n  id: 164,\n  dialCode: '+672'\n}, {\n  name: 'North Korea',\n  code: 'KP',\n  IBAN: false,\n  id: 183,\n  dialCode: '+850'\n}, {\n  name: 'North Macedonia',\n  code: 'MK',\n  IBAN: true,\n  id: 146,\n  dialCode: '+389'\n}, {\n  name: 'Northern Mariana Islands',\n  code: 'MP',\n  IBAN: false,\n  id: 152,\n  dialCode: '+1670'\n}, {\n  name: 'Norway',\n  code: 'NO',\n  IBAN: true,\n  id: 169,\n  dialCode: '+47'\n}, {\n  name: 'Oman',\n  code: 'OM',\n  IBAN: false,\n  id: 173,\n  dialCode: '+968'\n}, {\n  name: 'Pakistan',\n  code: 'PK',\n  IBAN: true,\n  id: 174,\n  dialCode: '+92'\n}, {\n  name: 'Palau',\n  code: 'PW',\n  IBAN: false,\n  id: 179,\n  dialCode: '+680'\n}, {\n  name: 'Palestine',\n  code: 'PS',\n  IBAN: true,\n  id: 186,\n  dialCode: '+970'\n}, {\n  name: 'Panama',\n  code: 'PA',\n  IBAN: false,\n  id: 175,\n  dialCode: '+507'\n}, {\n  name: 'Papua New Guinea',\n  code: 'PG',\n  IBAN: false,\n  id: 180,\n  dialCode: '+675'\n}, {\n  name: 'Paraguay',\n  code: 'PY',\n  IBAN: false,\n  id: 185,\n  dialCode: '+595'\n}, {\n  name: 'Peru',\n  code: 'PE',\n  IBAN: false,\n  id: 177,\n  dialCode: '+51'\n}, {\n  name: 'Philippines',\n  code: 'PH',\n  IBAN: false,\n  id: 178,\n  dialCode: '+63'\n}, {\n  name: 'Pitcairn Islands',\n  code: 'PN',\n  IBAN: false,\n  id: 176,\n  dialCode: '+872'\n}, {\n  name: 'Poland',\n  code: 'PL',\n  IBAN: true,\n  id: 181,\n  dialCode: '+48'\n}, {\n  name: 'Portugal',\n  code: 'PT',\n  IBAN: true,\n  id: 184,\n  dialCode: '+351'\n}, {\n  name: 'Puerto Rico',\n  code: 'PR',\n  IBAN: false,\n  id: 182,\n  dialCode: '+1939'\n}, {\n  name: 'Qatar',\n  code: 'QA',\n  IBAN: true,\n  id: 188,\n  dialCode: '+974'\n}, {\n  name: 'Republic of the Congo',\n  code: 'CG',\n  IBAN: false,\n  id: 48,\n  dialCode: '+242'\n}, {\n  name: 'Romania',\n  code: 'RO',\n  IBAN: true,\n  id: 190,\n  dialCode: '+40'\n}, {\n  name: 'Russia',\n  code: 'RU',\n  IBAN: false,\n  id: 191,\n  dialCode: '+7'\n}, {\n  name: 'Rwanda',\n  code: 'RW',\n  IBAN: false,\n  id: 192,\n  dialCode: '+250'\n}, {\n  name: 'Réunion',\n  code: 'RE',\n  IBAN: false,\n  id: 189,\n  dialCode: '+262'\n}, {\n  name: 'Saint Barthélemy',\n  code: 'BL',\n  IBAN: false,\n  id: 26,\n  dialCode: '+590'\n}, {\n  name: 'Saint Helena, Ascension and Tristan da Cunha',\n  code: 'SH',\n  IBAN: false,\n  id: 27,\n  dialCode: '+290'\n}, {\n  name: 'Saint Kitts and Nevis',\n  code: 'KN',\n  IBAN: false,\n  id: 122,\n  dialCode: '+1869'\n}, {\n  name: 'Saint Lucia',\n  code: 'LC',\n  IBAN: true,\n  id: 130,\n  dialCode: '+1758'\n}, {\n  name: 'Saint Martin',\n  code: 'MF',\n  IBAN: false,\n  id: 138,\n  dialCode: '+590'\n}, {\n  name: 'Saint Pierre and Miquelon',\n  code: 'PM',\n  IBAN: false,\n  id: 204,\n  dialCode: '+508'\n}, {\n  name: 'Saint Vincent and the Grenadines',\n  code: 'VC',\n  IBAN: false,\n  id: 238,\n  dialCode: '+1784'\n}, {\n  name: 'Samoa',\n  code: 'WS',\n  IBAN: false,\n  id: 245,\n  dialCode: '+685'\n}, {\n  name: 'San Marino',\n  code: 'SM',\n  IBAN: true,\n  id: 202,\n  dialCode: '+378'\n}, {\n  name: 'Saudi Arabia',\n  code: 'SA',\n  IBAN: true,\n  id: 193,\n  dialCode: '+966'\n}, {\n  name: 'Senegal',\n  code: 'SN',\n  IBAN: false,\n  id: 195,\n  dialCode: '+221'\n}, {\n  name: 'Serbia',\n  code: 'RS',\n  IBAN: true,\n  id: 205,\n  dialCode: '+381'\n}, {\n  name: 'Seychelles',\n  code: 'SC',\n  IBAN: true,\n  id: 214,\n  dialCode: '+248'\n}, {\n  name: 'Sierra Leone',\n  code: 'SL',\n  IBAN: false,\n  id: 200,\n  dialCode: '+232'\n}, {\n  name: 'Singapore',\n  code: 'SG',\n  IBAN: false,\n  id: 196,\n  dialCode: '+65'\n}, {\n  name: 'Sint Maarten',\n  code: 'SX',\n  IBAN: false,\n  id: 213\n}, {\n  name: 'Slovakia',\n  code: 'SK',\n  IBAN: true,\n  id: 209,\n  dialCode: '+421'\n}, {\n  name: 'Slovenia',\n  code: 'SI',\n  IBAN: true,\n  id: 210,\n  dialCode: '+386'\n}, {\n  name: 'Solomon Islands',\n  code: 'SB',\n  IBAN: false,\n  id: 199,\n  dialCode: '+677'\n}, {\n  name: 'Somalia',\n  code: 'SO',\n  IBAN: false,\n  id: 203,\n  dialCode: '+252'\n}, {\n  name: 'South Africa',\n  code: 'ZA',\n  IBAN: false,\n  id: 247,\n  dialCode: '+27'\n}, {\n  name: 'South Georgia',\n  code: 'GS',\n  IBAN: false,\n  id: 197,\n  dialCode: '+500'\n}, {\n  name: 'South Korea',\n  code: 'KR',\n  IBAN: false,\n  id: 123,\n  dialCode: '+82'\n}, {\n  name: 'South Sudan',\n  code: 'SS',\n  IBAN: false,\n  id: 206,\n  dialCode: '+211'\n}, {\n  name: 'Spain',\n  code: 'ES',\n  IBAN: true,\n  id: 70,\n  dialCode: '+34'\n}, {\n  name: 'Sri Lanka',\n  code: 'LK',\n  IBAN: false,\n  id: 132,\n  dialCode: '+94'\n}, {\n  name: 'Sudan',\n  code: 'SD',\n  IBAN: false,\n  id: 194,\n  dialCode: '+249'\n}, {\n  name: 'Suriname',\n  code: 'SR',\n  IBAN: false,\n  id: 208,\n  dialCode: '+597'\n}, {\n  name: 'Svalbard and Jan Mayen',\n  code: 'SJ',\n  IBAN: false,\n  id: 198,\n  dialCode: '+47'\n}, {\n  name: 'Sweden',\n  code: 'SE',\n  IBAN: true,\n  id: 211,\n  dialCode: '+46'\n}, {\n  name: 'Switzerland',\n  code: 'CH',\n  IBAN: true,\n  id: 42,\n  dialCode: '+41'\n}, {\n  name: 'Syria',\n  code: 'SY',\n  IBAN: false,\n  id: 215,\n  dialCode: '+963'\n}, {\n  name: 'São Tomé and Príncipe',\n  code: 'ST',\n  IBAN: true,\n  id: 207,\n  dialCode: '+239'\n}, {\n  name: 'Taiwan',\n  code: 'TW',\n  IBAN: false,\n  id: 229,\n  dialCode: '+886'\n}, {\n  name: 'Tajikistan',\n  code: 'TJ',\n  IBAN: false,\n  id: 220,\n  dialCode: '+992'\n}, {\n  name: 'Tanzania',\n  code: 'TZ',\n  IBAN: false,\n  id: 230,\n  dialCode: '+255'\n}, {\n  name: 'Thailand',\n  code: 'TH',\n  IBAN: false,\n  id: 219,\n  dialCode: '+66'\n}, {\n  name: 'Timor-Leste',\n  code: 'TL',\n  IBAN: true,\n  id: 223,\n  dialCode: '+670'\n}, {\n  name: 'Togo',\n  code: 'TG',\n  IBAN: false,\n  id: 218,\n  dialCode: '+228'\n}, {\n  name: 'Tokelau',\n  code: 'TK',\n  IBAN: false,\n  id: 221,\n  dialCode: '+690'\n}, {\n  name: 'Tonga',\n  code: 'TO',\n  IBAN: false,\n  id: 224,\n  dialCode: '+676'\n}, {\n  name: 'Trinidad and Tobago',\n  code: 'TT',\n  IBAN: false,\n  id: 225,\n  dialCode: '+1868'\n}, {\n  name: 'Tunisia',\n  code: 'TN',\n  IBAN: true,\n  id: 226,\n  dialCode: '+216'\n}, {\n  name: 'Turkey',\n  code: 'TR',\n  IBAN: true,\n  id: 227,\n  dialCode: '+90'\n}, {\n  name: 'Turkmenistan',\n  code: 'TM',\n  IBAN: false,\n  id: 222,\n  dialCode: '+993'\n}, {\n  name: 'Turks and Caicos Islands',\n  code: 'TC',\n  IBAN: false,\n  id: 216,\n  dialCode: '+1649'\n}, {\n  name: 'Tuvalu',\n  code: 'TV',\n  IBAN: false,\n  id: 228,\n  dialCode: '+688'\n}, {\n  name: 'Uganda',\n  code: 'UG',\n  IBAN: false,\n  id: 231,\n  dialCode: '+256'\n}, {\n  name: 'Ukraine',\n  code: 'UA',\n  IBAN: true,\n  id: 232,\n  dialCode: '+380'\n}, {\n  name: 'United Arab Emirates',\n  code: 'AE',\n  IBAN: true,\n  id: 7,\n  dialCode: '+971'\n}, {\n  name: 'United Kingdom',\n  code: 'GB',\n  IBAN: true,\n  id: 80,\n  dialCode: '+44'\n}, {\n  name: 'United States',\n  code: 'US',\n  IBAN: false,\n  id: 235,\n  dialCode: '+1'\n}, {\n  name: 'United States Minor Outlying Islands',\n  code: 'UM',\n  IBAN: false,\n  id: 233\n}, {\n  name: 'United States Virgin Islands',\n  code: 'VI',\n  IBAN: false,\n  id: 241,\n  dialCode: '+1340'\n}, {\n  name: 'Uruguay',\n  code: 'UY',\n  IBAN: false,\n  id: 234,\n  dialCode: '+598'\n}, {\n  name: 'Uzbekistan',\n  code: 'UZ',\n  IBAN: false,\n  id: 236,\n  dialCode: '+998'\n}, {\n  name: 'Vanuatu',\n  code: 'VU',\n  IBAN: false,\n  id: 243,\n  dialCode: '+678'\n}, {\n  name: 'Vatican City',\n  code: 'VA',\n  IBAN: true,\n  id: 237,\n  dialCode: '+379'\n}, {\n  name: 'Venezuela',\n  code: 'VE',\n  IBAN: false,\n  id: 239,\n  dialCode: '+58'\n}, {\n  name: 'Vietnam',\n  code: 'VN',\n  IBAN: false,\n  id: 242,\n  dialCode: '+84'\n}, {\n  name: 'Wallis and Futuna',\n  code: 'WF',\n  IBAN: false,\n  id: 244,\n  dialCode: '+681'\n}, {\n  name: 'Western Sahara',\n  code: 'EH',\n  IBAN: false,\n  id: 69\n}, {\n  name: 'Yemen',\n  code: 'YE',\n  IBAN: false,\n  id: 246,\n  dialCode: '+967'\n}, {\n  name: 'Zambia',\n  code: 'ZM',\n  IBAN: false,\n  id: 248,\n  dialCode: '+260'\n}, {\n  name: 'Zimbabwe',\n  code: 'ZW',\n  IBAN: false,\n  id: 249,\n  dialCode: '+263'\n}, {\n  name: 'Åland Islands',\n  code: 'AX',\n  IBAN: false,\n  id: 4,\n  dialCode: '+358'\n}];\nexports.countries = countries;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/static/country.js?");

/***/ }),

/***/ "./src/helpers/upload/index.js":
/*!*************************************!*\
  !*** ./src/helpers/upload/index.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _promises = _interopRequireDefault(__webpack_require__(/*! fs/promises */ \"fs/promises\"));\n\nvar _path = _interopRequireDefault(__webpack_require__(/*! path */ \"path\"));\n\nvar _process = _interopRequireDefault(__webpack_require__(/*! process */ \"process\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* eslint-disable no-shadow */\n\n/* eslint-disable no-param-reassign */\nconst uploadDir = `${_process.default.env.PWD}/uploads`;\n\nconst checkUploadDir = async (dir = '') => {\n  const checkLocalDir = async () => {\n    try {\n      return await _promises.default.stat(uploadDir);\n    } catch {\n      return await _promises.default.mkdir(uploadDir);\n    }\n  };\n\n  await checkLocalDir();\n\n  if (dir) {\n    const checkDir = async (dir = '') => {\n      const dirs = dir.split('/');\n\n      const checkDirs = async (uploadPath = uploadDir, first = '', ...next) => {\n        if (!first && (next === null || next === void 0 ? void 0 : next.length) > 0) {\n          [first = ''] = next.splice(0, 1);\n        }\n\n        if (first) {\n          const checkPath = _path.default.resolve(uploadPath, first);\n\n          try {\n            await _promises.default.stat(checkPath);\n          } catch {\n            await _promises.default.mkdir(checkPath);\n          } finally {\n            if ((next === null || next === void 0 ? void 0 : next.length) > 0) {\n              await checkDirs(checkPath, ...next);\n            }\n          }\n        }\n      };\n\n      return await checkDirs(uploadDir, ...dirs);\n    };\n\n    return await checkDir(dir);\n  }\n\n  return await true;\n};\n\nconst uploadFile = async ({\n  uploadPath = '',\n  name = '',\n  file = {},\n  buffer = null\n}) => {\n  if (!buffer && file) buffer = file.buffer;\n  if (!name && file) name = file.originalname;\n\n  if (buffer) {\n    await checkUploadDir(uploadPath);\n\n    const uploadDirPath = _path.default.resolve(uploadDir, uploadPath);\n\n    const uploadFilePath = _path.default.resolve(uploadDirPath, name);\n\n    await _promises.default.writeFile(uploadFilePath, buffer);\n    return uploadFilePath;\n  }\n\n  return null;\n};\n\nvar _default = uploadFile;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/upload/index.js?");

/***/ }),

/***/ "./src/helpers/utils/check-error-status-code.js":
/*!******************************************************!*\
  !*** ./src/helpers/utils/check-error-status-code.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.checkErrorStatusCode = void 0;\n\nconst checkErrorStatusCode = (statusCode, errorName) => {\n  if (typeof statusCode !== 'number') {\n    throw new Error(`Cannot construct ${errorName} due to arguments error`);\n  }\n\n  const isStatusCodeValid = /^[1-5][0-9]{2}$/.test(String(statusCode));\n\n  if (!isStatusCodeValid) {\n    throw new Error(`statusCode in ${errorName} should be a number in range from 100 to 599`);\n  }\n};\n\nexports.checkErrorStatusCode = checkErrorStatusCode;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/utils/check-error-status-code.js?");

/***/ }),

/***/ "./src/helpers/utils/convert-to-base64.js":
/*!************************************************!*\
  !*** ./src/helpers/utils/convert-to-base64.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.convertToBase64 = void 0;\n\nvar _fs = _interopRequireDefault(__webpack_require__(/*! fs */ \"fs\"));\n\nvar _errors = __webpack_require__(/*! ../errors */ \"./src/helpers/errors/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// Core\nconst convertToBase64 = path => {\n  try {\n    const bitMap = _fs.default.readFileSync(path);\n\n    return Buffer.from(bitMap).toString('base64');\n  } catch ({\n    message\n  }) {\n    throw new _errors.APIError(message);\n  }\n};\n\nexports.convertToBase64 = convertToBase64;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/utils/convert-to-base64.js?");

/***/ }),

/***/ "./src/helpers/utils/deleteFileServer.js":
/*!***********************************************!*\
  !*** ./src/helpers/utils/deleteFileServer.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _fs = _interopRequireDefault(__webpack_require__(/*! fs */ \"fs\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar _default = path => _fs.default.unlinkSync(path);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/utils/deleteFileServer.js?");

/***/ }),

/***/ "./src/helpers/utils/hash-sha1.js":
/*!****************************************!*\
  !*** ./src/helpers/utils/hash-sha1.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.createHashSha1 = void 0;\n\nvar _crypto = _interopRequireDefault(__webpack_require__(/*! crypto */ \"crypto\"));\n\nvar _config = __webpack_require__(/*! ../config */ \"./src/helpers/config/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// Core\n// Instruments\nconst createHashSha1 = data => {\n  const apiSecret = (0, _config.getEnv)('BASE_API_SECRET');\n  return _crypto.default.createHash('sha1').update(data).update(apiSecret).digest('hex');\n};\n\nexports.createHashSha1 = createHashSha1;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/utils/hash-sha1.js?");

/***/ }),

/***/ "./src/helpers/utils/index.js":
/*!************************************!*\
  !*** ./src/helpers/utils/index.js ***!
  \************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"createHashSha1\", ({\n  enumerable: true,\n  get: function () {\n    return _hashSha.createHashSha1;\n  }\n}));\nObject.defineProperty(exports, \"convertToBase64\", ({\n  enumerable: true,\n  get: function () {\n    return _convertToBase.convertToBase64;\n  }\n}));\nObject.defineProperty(exports, \"checkErrorStatusCode\", ({\n  enumerable: true,\n  get: function () {\n    return _checkErrorStatusCode.checkErrorStatusCode;\n  }\n}));\nObject.defineProperty(exports, \"saveFileServer\", ({\n  enumerable: true,\n  get: function () {\n    return _saveFileServer.default;\n  }\n}));\nObject.defineProperty(exports, \"deleteFileServer\", ({\n  enumerable: true,\n  get: function () {\n    return _deleteFileServer.default;\n  }\n}));\n\nvar _hashSha = __webpack_require__(/*! ./hash-sha1 */ \"./src/helpers/utils/hash-sha1.js\");\n\nvar _convertToBase = __webpack_require__(/*! ./convert-to-base64 */ \"./src/helpers/utils/convert-to-base64.js\");\n\nvar _checkErrorStatusCode = __webpack_require__(/*! ./check-error-status-code */ \"./src/helpers/utils/check-error-status-code.js\");\n\nvar _saveFileServer = _interopRequireDefault(__webpack_require__(/*! ./saveFileServer */ \"./src/helpers/utils/saveFileServer.js\"));\n\nvar _deleteFileServer = _interopRequireDefault(__webpack_require__(/*! ./deleteFileServer */ \"./src/helpers/utils/deleteFileServer.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/utils/index.js?");

/***/ }),

/***/ "./src/helpers/utils/saveFileServer.js":
/*!*********************************************!*\
  !*** ./src/helpers/utils/saveFileServer.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _fsExtra = __webpack_require__(/*! fs-extra */ \"fs-extra\");\n\nvar _path = _interopRequireDefault(__webpack_require__(/*! path */ \"path\"));\n\nvar _errors = __webpack_require__(/*! ../errors */ \"./src/helpers/errors/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst saveFile = async (fileOldPath, fileName) => {\n  const newPath = _path.default.join(_path.default.dirname(__dirname), `/storage/${fileName}`);\n\n  try {\n    await (0, _fsExtra.move)(fileOldPath, newPath, {\n      mkdirp: true\n    });\n    return newPath;\n  } catch (e) {\n    throw new _errors.APIError(e.message);\n  }\n};\n\nvar _default = saveFile;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/utils/saveFileServer.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/account/accountWithdrawalTransferSchema.js":
/*!**********************************************************************************!*\
  !*** ./src/helpers/validation-scheme/account/accountWithdrawalTransferSchema.js ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nvar _default = {\n  type: 'object',\n  properties: {\n    currency: {\n      type: 'string',\n      trim: true,\n      minLength: 3\n    },\n    amount: {\n      type: 'number'\n    },\n    beneficiaryId: {\n      type: 'number'\n    },\n    narrative: {\n      type: 'string'\n    },\n    forex: {\n      type: 'number'\n    }\n  },\n  required: ['currency', 'amount', 'beneficiaryId'],\n  additionalProperties: false\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/account/accountWithdrawalTransferSchema.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/account/createAccountSchema.js":
/*!**********************************************************************!*\
  !*** ./src/helpers/validation-scheme/account/createAccountSchema.js ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nvar _default = {\n  type: 'object',\n  properties: {\n    name: {\n      type: 'string',\n      trim: true\n    },\n    currency: {\n      type: 'string',\n      trim: true,\n      minLength: 3,\n      maxlength: 3\n    },\n    country: {\n      type: 'string',\n      trim: true,\n      minLength: 2,\n      maxlength: 2\n    },\n    bankName: {\n      type: 'string',\n      trim: true,\n      minLength: 1\n    },\n    accountNumber: {\n      type: 'string',\n      trim: true,\n      maxlength: 34\n    },\n    sortCode: {\n      type: 'string',\n      trim: true,\n      maxlength: 6\n    },\n    aba: {\n      type: 'string',\n      trim: true,\n      maxlength: 9\n    },\n    bankAddress: {\n      type: 'string',\n      trim: true,\n      minLength: 1\n    },\n    iban: {\n      type: 'string',\n      trim: true,\n      maxLength: 36\n    },\n    bic: {\n      type: 'string',\n      trim: true,\n      maxlength: 11\n    },\n    accountType: {\n      type: 'string',\n      trim: true,\n      maxlength: 1\n    },\n    isLocal: {\n      type: 'boolean'\n    }\n  },\n  required: ['country', 'currency'],\n  additionalProperties: false\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/account/createAccountSchema.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/account/index.js":
/*!********************************************************!*\
  !*** ./src/helpers/validation-scheme/account/index.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"createAccountSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _createAccountSchema.default;\n  }\n}));\nObject.defineProperty(exports, \"accountWithdrawalTransferSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _accountWithdrawalTransferSchema.default;\n  }\n}));\n\nvar _createAccountSchema = _interopRequireDefault(__webpack_require__(/*! ./createAccountSchema */ \"./src/helpers/validation-scheme/account/createAccountSchema.js\"));\n\nvar _accountWithdrawalTransferSchema = _interopRequireDefault(__webpack_require__(/*! ./accountWithdrawalTransferSchema */ \"./src/helpers/validation-scheme/account/accountWithdrawalTransferSchema.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/account/index.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/auth/checkEmailIsExist.js":
/*!*****************************************************************!*\
  !*** ./src/helpers/validation-scheme/auth/checkEmailIsExist.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nvar _default = {\n  type: 'object',\n  properties: {\n    email: {\n      type: 'string',\n      format: 'email',\n      trim: true\n    }\n  },\n  required: ['email'],\n  additionalProperties: false\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/auth/checkEmailIsExist.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/auth/createCustomer.js":
/*!**************************************************************!*\
  !*** ./src/helpers/validation-scheme/auth/createCustomer.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nvar _default = {\n  type: 'object',\n  properties: {\n    firstName: {\n      type: 'string',\n      trim: true,\n      minLength: 1\n    },\n    lastName: {\n      type: 'string',\n      trim: true,\n      minLength: 1\n    },\n    country: {\n      type: 'string',\n      trim: true,\n      minLength: 1\n    },\n    email: {\n      type: 'string',\n      format: 'email',\n      trim: true\n    },\n    phone: {\n      type: 'string',\n      trim: true,\n      minLength: 1\n    },\n    terms: {\n      type: 'boolean'\n    }\n  },\n  required: ['firstName', 'lastName', 'country', 'email', 'phone', 'terms'],\n  additionalProperties: false\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/auth/createCustomer.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/auth/index.js":
/*!*****************************************************!*\
  !*** ./src/helpers/validation-scheme/auth/index.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"loginScheme\", ({\n  enumerable: true,\n  get: function () {\n    return _login.default;\n  }\n}));\nObject.defineProperty(exports, \"passwordResetSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _passwordReset.default;\n  }\n}));\nObject.defineProperty(exports, \"passwordChangeSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _passwordChange.default;\n  }\n}));\nObject.defineProperty(exports, \"passwordConfirmSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _passwordConfirm.default;\n  }\n}));\nObject.defineProperty(exports, \"checkEmailIsExistScheme\", ({\n  enumerable: true,\n  get: function () {\n    return _checkEmailIsExist.default;\n  }\n}));\nObject.defineProperty(exports, \"createCustomerSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _createCustomer.default;\n  }\n}));\nObject.defineProperty(exports, \"verificationConfirmSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _verificationConfirm.default;\n  }\n}));\nObject.defineProperty(exports, \"verificationDocumentSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _verificationDocument.default;\n  }\n}));\nObject.defineProperty(exports, \"verificationCustomerSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _verificationCustomer.default;\n  }\n}));\n\nvar _login = _interopRequireDefault(__webpack_require__(/*! ./login */ \"./src/helpers/validation-scheme/auth/login.js\"));\n\nvar _passwordReset = _interopRequireDefault(__webpack_require__(/*! ./passwordReset */ \"./src/helpers/validation-scheme/auth/passwordReset.js\"));\n\nvar _passwordChange = _interopRequireDefault(__webpack_require__(/*! ./passwordChange */ \"./src/helpers/validation-scheme/auth/passwordChange.js\"));\n\nvar _passwordConfirm = _interopRequireDefault(__webpack_require__(/*! ./passwordConfirm */ \"./src/helpers/validation-scheme/auth/passwordConfirm.js\"));\n\nvar _checkEmailIsExist = _interopRequireDefault(__webpack_require__(/*! ./checkEmailIsExist */ \"./src/helpers/validation-scheme/auth/checkEmailIsExist.js\"));\n\nvar _createCustomer = _interopRequireDefault(__webpack_require__(/*! ./createCustomer */ \"./src/helpers/validation-scheme/auth/createCustomer.js\"));\n\nvar _verificationConfirm = _interopRequireDefault(__webpack_require__(/*! ./verificationConfirm */ \"./src/helpers/validation-scheme/auth/verificationConfirm.js\"));\n\nvar _verificationDocument = _interopRequireDefault(__webpack_require__(/*! ./verificationDocument */ \"./src/helpers/validation-scheme/auth/verificationDocument.js\"));\n\nvar _verificationCustomer = _interopRequireDefault(__webpack_require__(/*! ./verificationCustomer */ \"./src/helpers/validation-scheme/auth/verificationCustomer.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/auth/index.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/auth/login.js":
/*!*****************************************************!*\
  !*** ./src/helpers/validation-scheme/auth/login.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nvar _default = {\n  type: 'object',\n  properties: {\n    login: {\n      type: 'string'\n    },\n    password: {\n      type: 'string'\n    }\n  },\n  required: ['login', 'password']\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/auth/login.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/auth/passwordChange.js":
/*!**************************************************************!*\
  !*** ./src/helpers/validation-scheme/auth/passwordChange.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nvar _default = {\n  type: 'object',\n  properties: {\n    nonce: {\n      type: 'string'\n    },\n    id: {\n      type: 'integer'\n    },\n    time: {\n      type: 'integer'\n    },\n    password: {\n      type: 'string'\n    }\n  },\n  required: ['nonce', 'id', 'time', 'password'],\n  additionalProperties: false\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/auth/passwordChange.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/auth/passwordConfirm.js":
/*!***************************************************************!*\
  !*** ./src/helpers/validation-scheme/auth/passwordConfirm.js ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nvar _default = {\n  type: 'object',\n  properties: {\n    nonce: {\n      type: 'string'\n    },\n    id: {\n      type: 'integer'\n    },\n    time: {\n      type: 'integer'\n    }\n  },\n  required: ['nonce', 'id', 'time'],\n  additionalProperties: false\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/auth/passwordConfirm.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/auth/passwordReset.js":
/*!*************************************************************!*\
  !*** ./src/helpers/validation-scheme/auth/passwordReset.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nvar _default = {\n  type: 'object',\n  properties: {\n    email: {\n      type: 'string',\n      format: 'email'\n    }\n  },\n  required: ['email']\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/auth/passwordReset.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/auth/verificationConfirm.js":
/*!*******************************************************************!*\
  !*** ./src/helpers/validation-scheme/auth/verificationConfirm.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nvar _default = {\n  type: 'object',\n  properties: {\n    nonce: {\n      type: 'string'\n    }\n  },\n  required: ['nonce']\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/auth/verificationConfirm.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/auth/verificationCustomer.js":
/*!********************************************************************!*\
  !*** ./src/helpers/validation-scheme/auth/verificationCustomer.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nconst docSchema = {\n  type: ['object', 'string'],\n  properties: {\n    type: {\n      type: 'string',\n      pattern: 'image.*|application.pdf'\n    },\n    size: {\n      type: 'number',\n      maximum: 9437184 // 9mb\n\n    }\n  },\n  required: ['type', 'size']\n};\nvar _default = {\n  type: 'object',\n  properties: {\n    copyPassport: docSchema,\n    selfiePassport: docSchema,\n    addressPassport: docSchema\n  },\n  anyOf: [{\n    required: ['copyPassport']\n  }, {\n    required: ['addressPassport']\n  }, {\n    required: ['selfiePassport']\n  }],\n  additionalProperties: false\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/auth/verificationCustomer.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/auth/verificationDocument.js":
/*!********************************************************************!*\
  !*** ./src/helpers/validation-scheme/auth/verificationDocument.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nconst typeSchema = {\n  type: 'object',\n  properties: {\n    id: {\n      type: ['string', 'number']\n    },\n    name: {\n      type: 'string'\n    },\n    description: {\n      type: 'string'\n    }\n  }\n};\nconst docSchema = {\n  type: 'object',\n  properties: {\n    file: {\n      type: 'object',\n      properties: {\n        type: {\n          type: 'string',\n          pattern: 'image.(?:jpe?g|png|gif)|application.pdf'\n        }\n      },\n      required: ['type', 'size', 'name', 'path']\n    },\n    requirement: typeSchema,\n    type: typeSchema\n  }\n};\nvar _default = {\n  type: 'object',\n  properties: {\n    documents: {\n      type: 'object',\n      properties: {\n        face: docSchema,\n        document: docSchema,\n        address: docSchema\n      }\n    },\n    stakeholder: {\n      type: 'string'\n    },\n    nonce: {\n      type: 'string'\n    },\n    ettAppId: {\n      type: 'string'\n    }\n  },\n  required: ['documents', 'nonce', 'ettAppId'],\n  additionalProperties: false\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/auth/verificationDocument.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/beneficiary/createBeneficiarySchema.js":
/*!******************************************************************************!*\
  !*** ./src/helpers/validation-scheme/beneficiary/createBeneficiarySchema.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nvar _default = {\n  type: 'object',\n  properties: {\n    type: {\n      type: 'string',\n      enum: ['person', 'company']\n    },\n    name: {\n      type: 'string',\n      trim: true,\n      minLength: 1\n    },\n    street: {\n      type: 'string',\n      trim: true\n    },\n    email: {\n      type: 'string',\n      trim: true\n    },\n    phone: {\n      type: 'string',\n      trim: true\n    },\n    bankName: {\n      type: 'string',\n      trim: true // minLength: 1,\n\n    },\n    bankAddress: {\n      type: 'string',\n      trim: true\n    },\n    country: {\n      type: 'string',\n      trim: true,\n      minLength: 2,\n      maxLength: 2\n    },\n    currencyCode: {\n      type: 'string',\n      trim: true,\n      minLength: 3,\n      maxLength: 3\n    },\n    accountNumber: {\n      type: 'string',\n      trim: true,\n      maxLength: 34\n    },\n    iban: {\n      type: 'string',\n      trim: true,\n      maxLength: 34\n    },\n    bic: {\n      type: 'string',\n      trim: true,\n      maxLength: 11\n    },\n    sortCode: {\n      type: 'string',\n      trim: true,\n      maxlength: 6\n    },\n    aba: {\n      type: 'string',\n      trim: true,\n      maxlength: 9\n    },\n    reference: {\n      type: 'string',\n      trim: true\n    },\n    accountType: {\n      type: 'string',\n      trim: true,\n      maxlength: 1\n    },\n    isLocal: {\n      type: 'boolean'\n    },\n    isCorrectVerify: {\n      type: 'boolean'\n    }\n  },\n  required: ['currencyCode', 'country', 'type', 'name'],\n  additionalProperties: false\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/beneficiary/createBeneficiarySchema.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/beneficiary/index.js":
/*!************************************************************!*\
  !*** ./src/helpers/validation-scheme/beneficiary/index.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"createBeneficiarySchema\", ({\n  enumerable: true,\n  get: function () {\n    return _createBeneficiarySchema.default;\n  }\n}));\n\nvar _createBeneficiarySchema = _interopRequireDefault(__webpack_require__(/*! ./createBeneficiarySchema */ \"./src/helpers/validation-scheme/beneficiary/createBeneficiarySchema.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/beneficiary/index.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/email/index.js":
/*!******************************************************!*\
  !*** ./src/helpers/validation-scheme/email/index.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"sendEmailSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _sendEmailSchema.default;\n  }\n}));\n\nvar _sendEmailSchema = _interopRequireDefault(__webpack_require__(/*! ./sendEmailSchema */ \"./src/helpers/validation-scheme/email/sendEmailSchema.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/email/index.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/email/sendEmailSchema.js":
/*!****************************************************************!*\
  !*** ./src/helpers/validation-scheme/email/sendEmailSchema.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\nvar _default = {\n  type: 'object',\n  properties: {\n    templateName: {\n      type: 'string',\n      enum: ['transaction-details']\n    },\n    subject: {\n      type: 'string'\n    },\n    params: {\n      type: 'object'\n    }\n  },\n  required: ['templateName'],\n  additionalProperties: false\n};\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/email/sendEmailSchema.js?");

/***/ }),

/***/ "./src/helpers/validation-scheme/index.js":
/*!************************************************!*\
  !*** ./src/helpers/validation-scheme/index.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"loginScheme\", ({\n  enumerable: true,\n  get: function () {\n    return _auth.loginScheme;\n  }\n}));\nObject.defineProperty(exports, \"passwordResetSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _auth.passwordResetSchema;\n  }\n}));\nObject.defineProperty(exports, \"passwordChangeSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _auth.passwordChangeSchema;\n  }\n}));\nObject.defineProperty(exports, \"passwordConfirmSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _auth.passwordConfirmSchema;\n  }\n}));\nObject.defineProperty(exports, \"verificationCustomerSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _auth.verificationCustomerSchema;\n  }\n}));\nObject.defineProperty(exports, \"verificationConfirmSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _auth.verificationConfirmSchema;\n  }\n}));\nObject.defineProperty(exports, \"verificationDocumentSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _auth.verificationDocumentSchema;\n  }\n}));\nObject.defineProperty(exports, \"createCustomerSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _auth.createCustomerSchema;\n  }\n}));\nObject.defineProperty(exports, \"checkEmailIsExistScheme\", ({\n  enumerable: true,\n  get: function () {\n    return _auth.checkEmailIsExistScheme;\n  }\n}));\nObject.defineProperty(exports, \"createAccountSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _account.createAccountSchema;\n  }\n}));\nObject.defineProperty(exports, \"accountWithdrawalTransferSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _account.accountWithdrawalTransferSchema;\n  }\n}));\nObject.defineProperty(exports, \"createBeneficiarySchema\", ({\n  enumerable: true,\n  get: function () {\n    return _beneficiary.createBeneficiarySchema;\n  }\n}));\nObject.defineProperty(exports, \"sendEmailSchema\", ({\n  enumerable: true,\n  get: function () {\n    return _email.sendEmailSchema;\n  }\n}));\n\nvar _auth = __webpack_require__(/*! ./auth */ \"./src/helpers/validation-scheme/auth/index.js\");\n\nvar _account = __webpack_require__(/*! ./account */ \"./src/helpers/validation-scheme/account/index.js\");\n\nvar _beneficiary = __webpack_require__(/*! ./beneficiary */ \"./src/helpers/validation-scheme/beneficiary/index.js\");\n\nvar _email = __webpack_require__(/*! ./email */ \"./src/helpers/validation-scheme/email/index.js\");\n\n//# sourceURL=webpack://blocfx-backend/./src/helpers/validation-scheme/index.js?");

/***/ }),

/***/ "./src/middlewares/authentication.js":
/*!*******************************************!*\
  !*** ./src/middlewares/authentication.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.authentication = void 0;\n\nvar _helpers = __webpack_require__(/*! ../helpers */ \"./src/helpers/index.js\");\n\nvar _auth = _interopRequireDefault(__webpack_require__(/*! ../core/auth */ \"./src/core/auth/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst authentication = async (req, res, next) => {\n  const token = req.headers['auth-token'];\n\n  if (!token) {\n    return (0, _helpers.errorResponse)(res, {\n      error: _helpers.errorMessages.emptyToken,\n      action: 'auth-token',\n      status: 401\n    });\n  }\n\n  try {\n    const {\n      customer,\n      ettUserTokenId\n    } = _auth.default.decodeToken(token); // req.ettToken = await auth.getEttToken(ettUserTokenId);\n    // req.customer = customer;\n    // req.ettUserTokenId = ettUserTokenId;\n\n\n    req.ettToken = await _auth.default.getEttToken(ettUserTokenId);\n    req.customer = customer;\n    req.ettUserTokenId = ettUserTokenId;\n    return next();\n  } catch (e) {\n    return (0, _helpers.errorResponse)(res, {\n      error: _helpers.errorMessages.invalidToken,\n      action: 'auth-token',\n      status: 403\n    });\n  }\n};\n\nexports.authentication = authentication;\n\n//# sourceURL=webpack://blocfx-backend/./src/middlewares/authentication.js?");

/***/ }),

/***/ "./src/middlewares/cors.js":
/*!*********************************!*\
  !*** ./src/middlewares/cors.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.makeCors = void 0;\n\nvar _cors = _interopRequireDefault(__webpack_require__(/*! cors */ \"cors\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// import { getEnv, isProduction } from '../helpers/config';\n// const whitelist = getEnv('CORS_WHITE_LIST').split(',');\nconst options = {\n  origin(origin, callback) {\n    callback(null, true);\n  },\n\n  optionsSuccessStatus: 204\n};\n\nconst makeCors = () => (0, _cors.default)(options);\n\nexports.makeCors = makeCors;\n\n//# sourceURL=webpack://blocfx-backend/./src/middlewares/cors.js?");

/***/ }),

/***/ "./src/middlewares/form-data-processing.js":
/*!*************************************************!*\
  !*** ./src/middlewares/form-data-processing.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.formDataProcessing = void 0;\n\nvar _os = _interopRequireDefault(__webpack_require__(/*! os */ \"os\"));\n\nvar _formidable = __webpack_require__(/*! formidable */ \"formidable\");\n\nvar _lodash = _interopRequireDefault(__webpack_require__(/*! lodash */ \"lodash\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// Core\nconst formDataProcessing = ({\n  multiple = false,\n  uploadDir = _os.default.tmpdir(),\n  keepExtensions = true,\n  maxFileSize = 5 * 1024 * 1024,\n  // 5mb\n  maxFieldSize = 10 * 1024 // 10kb\n\n}) => (req, res, next) => {\n  const form = new _formidable.IncomingForm({\n    multiple,\n    uploadDir,\n    maxFileSize,\n    maxFieldSize,\n    keepExtensions\n  });\n  form.parse(req, (error, fields, files) => {\n    if (error) return next(error);\n    const val = { ...fields,\n      ...files\n    };\n    Object.keys(val).forEach(key => {\n      _lodash.default.set(req.body, key, val[key]);\n    });\n    return next();\n  });\n};\n\nexports.formDataProcessing = formDataProcessing;\n\n//# sourceURL=webpack://blocfx-backend/./src/middlewares/form-data-processing.js?");

/***/ }),

/***/ "./src/middlewares/index.js":
/*!**********************************!*\
  !*** ./src/middlewares/index.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"limiter\", ({\n  enumerable: true,\n  get: function () {\n    return _rateLimit.limiter;\n  }\n}));\nObject.defineProperty(exports, \"createTerminus\", ({\n  enumerable: true,\n  get: function () {\n    return _terminus.createTerminus;\n  }\n}));\nObject.defineProperty(exports, \"formDataProcessing\", ({\n  enumerable: true,\n  get: function () {\n    return _formDataProcessing.formDataProcessing;\n  }\n}));\nObject.defineProperty(exports, \"validator\", ({\n  enumerable: true,\n  get: function () {\n    return _validator.validator;\n  }\n}));\nObject.defineProperty(exports, \"authentication\", ({\n  enumerable: true,\n  get: function () {\n    return _authentication.authentication;\n  }\n}));\nObject.defineProperty(exports, \"makeCors\", ({\n  enumerable: true,\n  get: function () {\n    return _cors.makeCors;\n  }\n}));\nObject.defineProperty(exports, \"shuftiProSignature\", ({\n  enumerable: true,\n  get: function () {\n    return _shuftiProSignature.default;\n  }\n}));\n\nvar _rateLimit = __webpack_require__(/*! ./rate-limit */ \"./src/middlewares/rate-limit.js\");\n\nvar _terminus = __webpack_require__(/*! ./terminus */ \"./src/middlewares/terminus.js\");\n\nvar _formDataProcessing = __webpack_require__(/*! ./form-data-processing */ \"./src/middlewares/form-data-processing.js\");\n\nvar _validator = __webpack_require__(/*! ./validator */ \"./src/middlewares/validator.js\");\n\nvar _authentication = __webpack_require__(/*! ./authentication */ \"./src/middlewares/authentication.js\");\n\nvar _cors = __webpack_require__(/*! ./cors */ \"./src/middlewares/cors.js\");\n\nvar _shuftiProSignature = _interopRequireDefault(__webpack_require__(/*! ./shuftiProSignature */ \"./src/middlewares/shuftiProSignature.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//# sourceURL=webpack://blocfx-backend/./src/middlewares/index.js?");

/***/ }),

/***/ "./src/middlewares/meta/beneficiary.js":
/*!*********************************************!*\
  !*** ./src/middlewares/meta/beneficiary.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _models = __webpack_require__(/*! ../../models */ \"./src/models/index.js\");\n\nvar _ett = __webpack_require__(/*! ../../client/ett.client */ \"./src/client/ett.client.js\");\n\nconst notFoundMessage = 'Beneficiary not found!';\n\nconst beneficiaryMiddleware = async (request, response, next) => {\n  const {\n    beneficiaryID\n  } = request.params;\n  if (/[^0-9]/.test(beneficiaryID)) throw new Error('invalid beneficiary id');\n  let beneficiary = {};\n\n  try {\n    const {\n      beneficiaries\n    } = await (0, _ett.ettClientAuthToken)(request).get(`beneficiary/${beneficiaryID}`);\n    if (!beneficiaries.length) throw new Error(notFoundMessage);\n    [beneficiary] = beneficiaries;\n  } catch (error) {\n    if (error.status >= 400 && error.status < 500) throw new Error(notFoundMessage);else throw error;\n  }\n\n  const foundBeneficiary = await _models.Beneficiary.findOne({\n    ettId: beneficiary.id\n  }).lean();\n  if (!request.meta) request.meta = {};\n  request.meta.beneficiary = { ...beneficiary,\n    ...foundBeneficiary\n  };\n  return next();\n};\n\nvar _default = beneficiaryMiddleware;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/middlewares/meta/beneficiary.js?");

/***/ }),

/***/ "./src/middlewares/meta/index.js":
/*!***************************************!*\
  !*** ./src/middlewares/meta/index.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"paymentAccountMiddleware\", ({\n  enumerable: true,\n  get: function () {\n    return _paymentAccount.default;\n  }\n}));\nObject.defineProperty(exports, \"transferMiddleware\", ({\n  enumerable: true,\n  get: function () {\n    return _transfer.default;\n  }\n}));\nObject.defineProperty(exports, \"beneficiaryMiddleware\", ({\n  enumerable: true,\n  get: function () {\n    return _beneficiary.default;\n  }\n}));\n\nvar _paymentAccount = _interopRequireDefault(__webpack_require__(/*! ./paymentAccount */ \"./src/middlewares/meta/paymentAccount.js\"));\n\nvar _transfer = _interopRequireDefault(__webpack_require__(/*! ./transfer */ \"./src/middlewares/meta/transfer.js\"));\n\nvar _beneficiary = _interopRequireDefault(__webpack_require__(/*! ./beneficiary */ \"./src/middlewares/meta/beneficiary.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//# sourceURL=webpack://blocfx-backend/./src/middlewares/meta/index.js?");

/***/ }),

/***/ "./src/middlewares/meta/paymentAccount.js":
/*!************************************************!*\
  !*** ./src/middlewares/meta/paymentAccount.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _models = __webpack_require__(/*! ../../models */ \"./src/models/index.js\");\n\nvar _ett = __webpack_require__(/*! ../../client/ett.client */ \"./src/client/ett.client.js\");\n\nconst notFoundMessage = 'Account not found!';\n\nconst paymentAccountMiddleware = async (request, response, next) => {\n  const {\n    accountId\n  } = request.params;\n  if (/[^0-9]/.test(accountId)) throw new Error('invalid account id');\n  let account = {};\n\n  try {\n    const {\n      accounts\n    } = await (0, _ett.ettClientAuthToken)(request).get(`account/${accountId}`);\n    if (!accounts.length) throw new Error(notFoundMessage);\n    [account] = accounts;\n  } catch (error) {\n    if (error.status >= 400 && error.status < 500) throw new Error(notFoundMessage);else throw error;\n  }\n\n  const foundAccount = await _models.PaymentAccount.findOne({\n    accountNumber: accountId\n  }).lean();\n  if (!request.meta) request.meta = {};\n  request.meta.account = { ...account,\n    ...foundAccount\n  };\n  return next();\n};\n\nvar _default = paymentAccountMiddleware;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/middlewares/meta/paymentAccount.js?");

/***/ }),

/***/ "./src/middlewares/meta/transfer.js":
/*!******************************************!*\
  !*** ./src/middlewares/meta/transfer.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _models = __webpack_require__(/*! ../../models */ \"./src/models/index.js\");\n\nconst transferMiddleware = async (request, response, next) => {\n  const {\n    transferId\n  } = request.params;\n  const transfer = await _models.Transfer.findById(transferId).populate('quote');\n  if (!request.meta) request.meta = {};\n  request.meta.transfer = transfer;\n  return next();\n};\n\nvar _default = transferMiddleware;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/middlewares/meta/transfer.js?");

/***/ }),

/***/ "./src/middlewares/rate-limit.js":
/*!***************************************!*\
  !*** ./src/middlewares/rate-limit.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.limiter = void 0;\n\nvar _expressRateLimit = _interopRequireDefault(__webpack_require__(/*! express-rate-limit */ \"express-rate-limit\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst limiter = (0, _expressRateLimit.default)({\n  windowMS: 15 * 60 * 1000,\n  // 15 minutes\n  max: 100\n});\nexports.limiter = limiter;\n\n//# sourceURL=webpack://blocfx-backend/./src/middlewares/rate-limit.js?");

/***/ }),

/***/ "./src/middlewares/shuftiProSignature.js":
/*!***********************************************!*\
  !*** ./src/middlewares/shuftiProSignature.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _helpers = __webpack_require__(/*! ../helpers */ \"./src/helpers/index.js\");\n\nvar _shuftipro = __webpack_require__(/*! ../client/shuftipro.client */ \"./src/client/shuftipro.client.js\");\n\nvar _default = async (req, res, next) => {\n  const data = req.body;\n  const {\n    signature\n  } = req.headers;\n  if ((0, _shuftipro.validateSignature)({\n    data,\n    signature\n  })) return next();\n  return (0, _helpers.errorResponse)(res, {\n    error: _helpers.errorMessages.shuftiProSignature,\n    status: 403\n  });\n};\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/middlewares/shuftiProSignature.js?");

/***/ }),

/***/ "./src/middlewares/terminus.js":
/*!*************************************!*\
  !*** ./src/middlewares/terminus.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.createTerminus = void 0;\n\nvar _terminus = __webpack_require__(/*! @godaddy/terminus */ \"@godaddy/terminus\");\n\n// Core\nconst getOptions = server => ({\n  signal: 'SIGINT',\n\n  onSignal() {\n    return Promise.all([server.close()]);\n  }\n\n});\n\nconst createTerminus = server => {\n  const options = getOptions(server);\n  return (0, _terminus.createTerminus)(server, options);\n};\n\nexports.createTerminus = createTerminus;\n\n//# sourceURL=webpack://blocfx-backend/./src/middlewares/terminus.js?");

/***/ }),

/***/ "./src/middlewares/validator.js":
/*!**************************************!*\
  !*** ./src/middlewares/validator.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.validator = void 0;\n\nvar _ajv = _interopRequireDefault(__webpack_require__(/*! ajv */ \"ajv\"));\n\nvar _ajvKeywords = _interopRequireDefault(__webpack_require__(/*! ajv-keywords */ \"ajv-keywords\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// Core\nconst normalizeError = ({\n  dataPath,\n  params,\n  message\n}) => {\n  const error = {};\n  const [paramValue] = Object.values(params);\n\n  if (dataPath) {\n    error[dataPath.slice(1)] = message;\n  } else {\n    error[paramValue] = message;\n  }\n\n  return error;\n}; // eslint-disable-next-line consistent-return\n\n\nconst validator = schema => (req, res, next) => {\n  const ajv = new _ajv.default({\n    allErrors: true\n  });\n  (0, _ajvKeywords.default)(ajv);\n  const validate = ajv.compile(schema);\n  const valid = validate(req.body);\n\n  if (valid) {\n    return next();\n  }\n\n  const errors = validate.errors.map(({\n    message,\n    params,\n    dataPath\n  }) => normalizeError({\n    dataPath,\n    message,\n    params\n  }));\n  res.status(400).json({\n    message: 'Validation error',\n    errors\n  });\n};\n\nexports.validator = validator;\n\n//# sourceURL=webpack://blocfx-backend/./src/middlewares/validator.js?");

/***/ }),

/***/ "./src/models/beneficiary/index.js":
/*!*****************************************!*\
  !*** ./src/models/beneficiary/index.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nvar _regexp = _interopRequireDefault(__webpack_require__(/*! ../../helpers/regexp */ \"./src/helpers/regexp/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// @ts-check\nconst BeneficiarySchema = new _mongoose.Schema({\n  ettId: {\n    // UNIQUE ID INTO BACK OFFICE\n    type: Number,\n    unique: true\n  },\n  type: {\n    // Type of external beneficiary. ‘person’ or ‘company’\n    type: String,\n    enum: ['person', 'company']\n  },\n  name: {\n    // Last name or company name of external beneficiary\n    type: String,\n    validate: {\n      validator: name => !/[1-9|!@#$%^&*()_\\-+=;:<>,?`~\"'№]/.test(name)\n    }\n  },\n  firstname: {\n    // Name of external beneficiary\n    type: String,\n    validate: {\n      validator: firstName => !/[1-9|!@#$%^&*()_\\-+=;:<>,?`~\"'№]/.test(firstName)\n    }\n  },\n  dob: {\n    // Date of birth of external beneficiary\n    type: Date\n  },\n  nationality: {\n    // Nationality (iso3166 code) of external beneficiary\n    type: String,\n    match: _regexp.default.countryCode\n  },\n  city: {\n    // City of residence of external beneficiary\n    type: String,\n    validate: {\n      validator: city => !/[|!@$%^&*=;?~]/.test(city)\n    }\n  },\n  idNumber: {\n    // Social security (person) or company registration (company) number\n    type: String\n  },\n  iban: {\n    // If the user wants to provide both a local account number and an iban, both fields can be used\n    type: String,\n    match: _regexp.default.iban,\n    max: 34\n  },\n  accountNumber: {\n    // Account number of beneficiary. This can be an IBAN or other account number\n    type: String,\n    match: _regexp.default.accountNumber,\n    max: 34\n  },\n  accountNumberPrefix: {\n    type: String\n  },\n  accountNumberSuffix: {\n    type: String\n  },\n  //  *** BIC code, sort code, aba routing number or bank code of beneficiary bank\n  bic: {\n    type: String,\n    match: _regexp.default.bic,\n    max: 11\n  },\n  sortCode: {\n    // only local [?ONLY GB]\n    type: String,\n    match: _regexp.default.sortCode,\n    max: 6\n  },\n  aba: {\n    // only local [?ONLY US]\n    type: String,\n    match: _regexp.default.aba,\n    max: 9\n  },\n  // *** BIC code ***\n  postcode: {\n    // Postal code of external beneficiary\n    type: String\n  },\n  state: {\n    // 2-character abbreviation of US state of residence\n    type: String\n  },\n  bankName: {\n    // Name of beneficiary’s bank\n    type: String\n  },\n  bankStateCode: {\n    // State code of the bank (only for North America)\n    type: String\n  },\n  bankCountry: {\n    // 2-character iso3166 country code of the bank, lower case\n    type: String,\n    match: _regexp.default.countryCode\n  },\n  bankKey: {\n    // Bank Key (only needed for local routing in certain countries)\n    type: String\n  },\n  holding: {\n    // Holding branch (only needed for local routing in certain countries)\n    type: String\n  },\n  branchCode: {\n    // Branch code (only needed for local routing in certain countries)\n    type: String\n  },\n  accountType: {\n    // 1 = Savings, 2 = Current (only needed for local routing in certain countries)\n    type: String,\n    enum: ['1', '2']\n  },\n  bankAddress: {\n    // Address of beneficiary’s bank\n    type: String,\n    validate: {\n      validator: bankAddress => !/[|!@$%^&*=;?~]/.test(bankAddress)\n    }\n  },\n  country: {\n    // Country of residence (iso3166) of external beneficiary\n    type: String,\n    match: _regexp.default.countryCode\n  },\n  street: {\n    // Street address of external beneficiary\n    type: String,\n    validate: {\n      validator: street => !/[|!@$%^&*=;?~]/.test(street)\n    }\n  },\n  // 3-character iso code for currency. Only useful if the\n  // beneficiary account is known to have a different\n  // currency than the default for the bank country.\n  currencyCode: {\n    type: String,\n    match: _regexp.default.currencyCode\n  },\n  sepa: {\n    // Indicate whether the payment should be sent through SEPA\n    type: Boolean\n  },\n  email: {\n    // Email address of beneficiary\n    type: String,\n    match: _regexp.default.email\n  },\n  phone: {\n    // International phone number\n    type: String,\n    match: _regexp.default.phone,\n    max: 17\n  },\n  reference: {\n    type: String\n  },\n  // DOCUMENT VERIFY\n  isCorrectVerify: {\n    type: Boolean\n  },\n  expiryDate: {\n    type: Date\n  },\n  issueDate: {\n    type: Date\n  },\n  documentNumber: {\n    type: String\n  },\n  verifyID: {\n    type: Number\n  }\n});\n\nvar _default = (0, _mongoose.model)('beneficiary', BeneficiarySchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/beneficiary/index.js?");

/***/ }),

/***/ "./src/models/emailConfirm/index.js":
/*!******************************************!*\
  !*** ./src/models/emailConfirm/index.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nvar _regexp = _interopRequireDefault(__webpack_require__(/*! ../../helpers/regexp */ \"./src/helpers/regexp/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst EmailConfirmSchema = new _mongoose.Schema({\n  id: {\n    type: Number,\n    unique: true,\n    required: true\n  },\n  email: {\n    type: String,\n    required: true,\n    match: _regexp.default.email\n  },\n  code: {\n    type: String,\n    required: true\n  }\n}, {\n  timestamps: true\n});\n\nvar _default = (0, _mongoose.model)('email-confirm', EmailConfirmSchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/emailConfirm/index.js?");

/***/ }),

/***/ "./src/models/ettCustomer/index.js":
/*!*****************************************!*\
  !*** ./src/models/ettCustomer/index.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nvar _regexp = _interopRequireDefault(__webpack_require__(/*! ../../helpers/regexp */ \"./src/helpers/regexp/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// @ts-check\nconst CustomerSchema = new _mongoose.Schema({\n  ettAppId: {\n    type: String,\n    required: true,\n    unique: true\n  },\n  firstName: {\n    type: String,\n    required: true,\n    validate: {\n      validator: firstName => !/[1-9|!@#$%^&*()_\\-+=;:<>,?`~\"'№]/.test(firstName)\n    },\n    trim: true\n  },\n  lastName: {\n    type: String,\n    required: true,\n    validate: {\n      validator: lastName => !/[1-9|!@#$%^&*()_\\-+=;:<>,?`~\"'№]/.test(lastName)\n    },\n    trim: true\n  },\n  email: {\n    type: String,\n    required: true,\n    match: _regexp.default.email,\n    trim: true,\n    min: 5\n  },\n  phone: {\n    type: String,\n    required: true,\n    match: _regexp.default.phone,\n    trim: true,\n    max: 17,\n    min: 10\n  },\n  country: {\n    type: String,\n    required: true,\n    match: _regexp.default.countryCode,\n    trim: true,\n    max: 2\n  },\n  roles: {\n    type: [{\n      type: Object,\n      ref: 'role'\n    }],\n    default: [{\n      role: 'signatory'\n    }]\n  }\n}, {\n  timestamps: true\n});\n\nclass Customer {\n  static findByEttAppId(ettAppId) {\n    // @ts-ignore\n    return this.findOne({\n      ettAppId\n    });\n  }\n\n}\n\nCustomerSchema.loadClass(Customer);\n\nvar _default = (0, _mongoose.model)('customer', CustomerSchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/ettCustomer/index.js?");

/***/ }),

/***/ "./src/models/ettCustomerDocument/index.js":
/*!*************************************************!*\
  !*** ./src/models/ettCustomerDocument/index.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nvar _deleteFileServer = _interopRequireDefault(__webpack_require__(/*! ../../helpers/utils/deleteFileServer */ \"./src/helpers/utils/deleteFileServer.js\"));\n\nvar _ett = __webpack_require__(/*! ../../client/ett.client */ \"./src/client/ett.client.js\");\n\nvar _utils = __webpack_require__(/*! ../../helpers/utils */ \"./src/helpers/utils/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst TypeSchema = {\n  id: {\n    type: Number\n  },\n  name: {\n    type: String\n  },\n  description: {\n    type: String\n  }\n};\nconst CustomerDocumentSchema = new _mongoose.Schema({\n  ettId: {\n    type: Number\n  },\n  nonce: {\n    type: String\n  },\n  ettAppId: {\n    type: String,\n    required: true\n  },\n  name: {\n    type: String,\n    required: true\n  },\n  status: {\n    type: String,\n    // enum: ['approved', 'rejected', 'pending', 'submitted'],\n    default: 'pending'\n  },\n  path: {\n    type: String\n  },\n  ettTypeData: {\n    type: TypeSchema\n  },\n  ettRequirementData: {\n    type: TypeSchema\n  },\n  reference: {\n    type: String,\n    default: null\n  },\n  stakeholder: {\n    type: Number,\n    default: null\n  },\n  comment: {\n    type: String,\n    default: null\n  }\n}, {\n  timestamps: true\n});\n\nclass CustomerDocument {\n  static findByEttAppId(ettAppId) {\n    return this.findOne({\n      ettAppId\n    });\n  }\n\n}\n\nCustomerDocumentSchema.loadClass(CustomerDocument);\nCustomerDocumentSchema.post('remove', {\n  document: true\n}, doc => {\n  (0, _deleteFileServer.default)(doc.path);\n});\nCustomerDocumentSchema.post('save', {\n  document: true\n}, async (doc, next) => {\n  if (doc.status === 'accepted' && !doc.ettId) {\n    await _ett.ettClient.post('/application/document', {\n      nonce: doc.nonce,\n      name: doc.name,\n      stakeholder: doc.stakeholder,\n      reference: doc._id,\n      data: (0, _utils.convertToBase64)(doc.path),\n      requirement: doc.ettRequirementData.id,\n      type: doc.ettTypeData.id\n    });\n  }\n\n  next();\n});\n\nvar _default = (0, _mongoose.model)('customer-document', CustomerDocumentSchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/ettCustomerDocument/index.js?");

/***/ }),

/***/ "./src/models/ettStakeholderDocumentData/index.js":
/*!********************************************************!*\
  !*** ./src/models/ettStakeholderDocumentData/index.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nconst StakeholderDocumentDataSchema = new _mongoose.Schema({\n  stakeholder: {\n    type: Number,\n    required: true\n  },\n  nonce: {\n    type: String,\n    required: true,\n    unique: true\n  },\n  stakeholderDocumentFiles: [{\n    type: _mongoose.ObjectId,\n    ref: 'stakeholder-document-file'\n  }]\n}, {\n  timestamps: true\n});\n\nvar _default = (0, _mongoose.model)('stakeholder-document-data', StakeholderDocumentDataSchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/ettStakeholderDocumentData/index.js?");

/***/ }),

/***/ "./src/models/ettStakeholderDocumentFile/index.js":
/*!********************************************************!*\
  !*** ./src/models/ettStakeholderDocumentFile/index.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nconst StakeholderDocumentFileSchema = new _mongoose.Schema({\n  stakeholderDocumentData: {\n    type: _mongoose.ObjectId,\n    required: true,\n    ref: 'stakeholder-document-data'\n  },\n  requirement: {\n    type: Number,\n    required: true\n  },\n  type: {\n    type: Number,\n    required: true\n  },\n  mode: {\n    type: String\n  },\n  fileUrl: {\n    type: String,\n    required: true,\n    unique: true\n  }\n}, {\n  timestamps: true\n});\n\nvar _default = (0, _mongoose.model)('stakeholder-document-file', StakeholderDocumentFileSchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/ettStakeholderDocumentFile/index.js?");

/***/ }),

/***/ "./src/models/ettUserToken/index.js":
/*!******************************************!*\
  !*** ./src/models/ettUserToken/index.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nconst EttUserTokenSchema = new _mongoose.Schema({\n  ettAppId: {\n    type: String,\n    required: true\n  },\n  ettCustomerId: {\n    type: Number\n  },\n  ettToken: {\n    type: String,\n    default: null\n  },\n  confirm: {\n    type: Boolean,\n    default: false\n  },\n  type: {\n    type: String,\n    enum: ['auth'],\n    default: 'auth'\n  }\n}, {\n  timestamps: true\n});\n\nclass EttUserTokenClass {\n  static findByEttId(ettId) {\n    return this.findOne({\n      ettId\n    });\n  }\n\n}\n\nEttUserTokenSchema.loadClass(EttUserTokenClass);\n\nvar _default = (0, _mongoose.model)('ett-user-tokens', EttUserTokenSchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/ettUserToken/index.js?");

/***/ }),

/***/ "./src/models/index.js":
/*!*****************************!*\
  !*** ./src/models/index.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"EttCustomer\", ({\n  enumerable: true,\n  get: function () {\n    return _ettCustomer.default;\n  }\n}));\nObject.defineProperty(exports, \"EttUserToken\", ({\n  enumerable: true,\n  get: function () {\n    return _ettUserToken.default;\n  }\n}));\nObject.defineProperty(exports, \"ShuftiProVerification\", ({\n  enumerable: true,\n  get: function () {\n    return _shuftiProVerification.default;\n  }\n}));\nObject.defineProperty(exports, \"EttCustomerDocument\", ({\n  enumerable: true,\n  get: function () {\n    return _ettCustomerDocument.default;\n  }\n}));\nObject.defineProperty(exports, \"PaymentAccount\", ({\n  enumerable: true,\n  get: function () {\n    return _paymentAccount.default;\n  }\n}));\nObject.defineProperty(exports, \"Beneficiary\", ({\n  enumerable: true,\n  get: function () {\n    return _beneficiary.default;\n  }\n}));\nObject.defineProperty(exports, \"Notification\", ({\n  enumerable: true,\n  get: function () {\n    return _notification.default;\n  }\n}));\nObject.defineProperty(exports, \"PhoneConfirm\", ({\n  enumerable: true,\n  get: function () {\n    return _phoneConfirm.default;\n  }\n}));\nObject.defineProperty(exports, \"EmailConfirm\", ({\n  enumerable: true,\n  get: function () {\n    return _emailConfirm.default;\n  }\n}));\nObject.defineProperty(exports, \"Transfer\", ({\n  enumerable: true,\n  get: function () {\n    return _transfer.default;\n  }\n}));\nObject.defineProperty(exports, \"Quote\", ({\n  enumerable: true,\n  get: function () {\n    return _quote.default;\n  }\n}));\nObject.defineProperty(exports, \"StakeholderDocumentData\", ({\n  enumerable: true,\n  get: function () {\n    return _ettStakeholderDocumentData.default;\n  }\n}));\nObject.defineProperty(exports, \"StakeholderDocumentFile\", ({\n  enumerable: true,\n  get: function () {\n    return _ettStakeholderDocumentFile.default;\n  }\n}));\n\nvar _ettCustomer = _interopRequireDefault(__webpack_require__(/*! ./ettCustomer */ \"./src/models/ettCustomer/index.js\"));\n\nvar _ettUserToken = _interopRequireDefault(__webpack_require__(/*! ./ettUserToken */ \"./src/models/ettUserToken/index.js\"));\n\nvar _shuftiProVerification = _interopRequireDefault(__webpack_require__(/*! ./shuftiProVerification */ \"./src/models/shuftiProVerification/index.js\"));\n\nvar _ettCustomerDocument = _interopRequireDefault(__webpack_require__(/*! ./ettCustomerDocument */ \"./src/models/ettCustomerDocument/index.js\"));\n\nvar _paymentAccount = _interopRequireDefault(__webpack_require__(/*! ./paymentAccount */ \"./src/models/paymentAccount/index.js\"));\n\nvar _beneficiary = _interopRequireDefault(__webpack_require__(/*! ./beneficiary */ \"./src/models/beneficiary/index.js\"));\n\nvar _notification = _interopRequireDefault(__webpack_require__(/*! ./notification */ \"./src/models/notification/index.js\"));\n\nvar _phoneConfirm = _interopRequireDefault(__webpack_require__(/*! ./phoneConfirm */ \"./src/models/phoneConfirm/index.js\"));\n\nvar _emailConfirm = _interopRequireDefault(__webpack_require__(/*! ./emailConfirm */ \"./src/models/emailConfirm/index.js\"));\n\nvar _transfer = _interopRequireDefault(__webpack_require__(/*! ./transfer */ \"./src/models/transfer/index.js\"));\n\nvar _quote = _interopRequireDefault(__webpack_require__(/*! ./quote */ \"./src/models/quote/index.js\"));\n\nvar _ettStakeholderDocumentData = _interopRequireDefault(__webpack_require__(/*! ./ettStakeholderDocumentData */ \"./src/models/ettStakeholderDocumentData/index.js\"));\n\nvar _ettStakeholderDocumentFile = _interopRequireDefault(__webpack_require__(/*! ./ettStakeholderDocumentFile */ \"./src/models/ettStakeholderDocumentFile/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//# sourceURL=webpack://blocfx-backend/./src/models/index.js?");

/***/ }),

/***/ "./src/models/notification/index.js":
/*!******************************************!*\
  !*** ./src/models/notification/index.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nconst NotificationSchema = new _mongoose.Schema({\n  title: {\n    type: String,\n    required: true\n  },\n  date: {\n    type: Number,\n    required: true\n  },\n  body: {\n    type: String,\n    required: true\n  },\n  uId: {\n    type: Number,\n    required: true\n  }\n});\n\nvar _default = (0, _mongoose.model)('notification', NotificationSchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/notification/index.js?");

/***/ }),

/***/ "./src/models/paymentAccount/index.js":
/*!********************************************!*\
  !*** ./src/models/paymentAccount/index.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nvar _regexp = _interopRequireDefault(__webpack_require__(/*! ../../helpers/regexp */ \"./src/helpers/regexp/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst AccountFundingSchema = new _mongoose.Schema({\n  name: {\n    type: String,\n    validate: {\n      validator: name => !/[1-9|!@#$%^&*()_\\-+=;:<>,.?`~\"'№]/.test(name)\n    },\n    trim: true\n  },\n  address: {\n    type: String,\n    trim: true\n  },\n  iban: {\n    type: String,\n    max: 34,\n    match: _regexp.default.iban,\n    trim: true\n  },\n  accountNumber: {\n    type: String,\n    max: 34,\n    match: _regexp.default.accountNumber\n  },\n  bankName: {\n    type: String\n  },\n  bankAddress: {\n    type: String\n  },\n  bic: {\n    type: String,\n    match: _regexp.default.bic,\n    max: 11,\n    trim: true\n  },\n  sortCode: {\n    type: String,\n    match: _regexp.default.sortCode,\n    max: 6,\n    trim: true\n  },\n  aba: {\n    type: String,\n    match: _regexp.default.aba,\n    max: 9,\n    trim: true\n  },\n  bankCode: {\n    type: String\n  },\n  bankKey: {\n    type: String\n  },\n  bankCountry: {\n    type: String,\n    trim: true,\n    match: _regexp.default.countryCode\n  },\n  instructions: {\n    type: String\n  },\n  reference: {\n    type: String\n  }\n});\nconst PaymentAccountSchema = new _mongoose.Schema({\n  reference: {\n    type: String\n  },\n  accountNumber: {\n    type: String,\n    trim: true,\n    required: true\n  },\n  name: {\n    type: String,\n    validate: {\n      validator: name => !/[^\\w| ]/.test(name)\n    },\n    trim: true\n  },\n  currency: {\n    type: String,\n    required: true,\n    match: _regexp.default.currencyCode,\n    max: 3,\n    min: 3,\n    trim: true\n  },\n  accountHolder: {\n    type: Object\n  },\n  funding: {\n    type: [{\n      type: AccountFundingSchema,\n      ref: 'funding'\n    }],\n    required: true\n  }\n});\n\nvar _default = (0, _mongoose.model)('payment-account', PaymentAccountSchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/paymentAccount/index.js?");

/***/ }),

/***/ "./src/models/phoneConfirm/index.js":
/*!******************************************!*\
  !*** ./src/models/phoneConfirm/index.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nvar _regexp = _interopRequireDefault(__webpack_require__(/*! ../../helpers/regexp */ \"./src/helpers/regexp/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst PhoneConfirmSchema = new _mongoose.Schema({\n  id: {\n    type: Number,\n    unique: true,\n    required: true\n  },\n  phone: {\n    type: String,\n    required: true,\n    match: _regexp.default.phone\n  },\n  code: {\n    type: String,\n    required: true\n  }\n}, {\n  timestamps: true\n});\n\nvar _default = (0, _mongoose.model)('phone-confirm', PhoneConfirmSchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/phoneConfirm/index.js?");

/***/ }),

/***/ "./src/models/quote/index.js":
/*!***********************************!*\
  !*** ./src/models/quote/index.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nvar _regexp = _interopRequireDefault(__webpack_require__(/*! ../../helpers/regexp */ \"./src/helpers/regexp/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst amountType = {\n  type: Number,\n  required: true,\n  min: 0\n};\nconst currencyType = {\n  type: String,\n  required: true,\n  match: _regexp.default.currencyCode\n};\nconst QuoteSchema = new _mongoose.Schema({\n  id: {\n    type: Number,\n    unique: true,\n    required: true\n  },\n  sellcurrency: currencyType,\n  buycurrency: currencyType,\n  sellamount: amountType,\n  buyamount: amountType,\n  time: {\n    type: Number\n  }\n}, {\n  timestamps: true\n});\n\nvar _default = (0, _mongoose.model)('quote', QuoteSchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/quote/index.js?");

/***/ }),

/***/ "./src/models/shuftiProVerification/index.js":
/*!***************************************************!*\
  !*** ./src/models/shuftiProVerification/index.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nconst ShuftiProVerificationSchema = new _mongoose.Schema({\n  reference: {\n    type: String,\n    required: true,\n    unique: true\n  },\n  data: {\n    type: Object,\n    default: {}\n  },\n  status: {\n    type: String,\n    default: 'pending'\n  }\n});\n\nvar _default = (0, _mongoose.model)('shuftipro-verification', ShuftiProVerificationSchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/shuftiProVerification/index.js?");

/***/ }),

/***/ "./src/models/transfer/index.js":
/*!**************************************!*\
  !*** ./src/models/transfer/index.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nvar _regexp = _interopRequireDefault(__webpack_require__(/*! ../../helpers/regexp */ \"./src/helpers/regexp/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst ReasonOptionSchema = new _mongoose.Schema({\n  id: {\n    type: Number\n  },\n  name: {\n    type: String\n  },\n  description: {\n    type: String\n  }\n});\nconst ReasonSchema = new _mongoose.Schema({\n  id: {\n    type: Number\n  },\n  name: {\n    type: String\n  },\n  description: {\n    type: String\n  },\n  options: {\n    type: [{\n      type: ReasonOptionSchema,\n      ref: 'options'\n    }]\n  }\n});\nconst RuleSchema = new _mongoose.Schema({\n  name: {\n    type: String,\n    required: true,\n    trim: true\n  },\n  description: {\n    type: String\n  },\n  reason: {\n    type: ReasonSchema,\n    ref: 'reason'\n  }\n}); // const CounterpartSchema = new Schema({\n//   id: {\n//     type: Number,\n//     required: true,\n//   },\n//   firstName: {\n//     type: String,\n//   },\n//   name: {\n//     type: String,\n//   },\n//   street: {\n//     type: String,\n//   },\n//   city: {\n//     type: String,\n//   },\n//   postCode: {\n//     type: String,\n//   },\n//   country: {\n//     type: String,\n//     match: regexp.countryCode,\n//   },\n//   dob: {\n//     type: String,\n//   },\n//   nationality: {\n//     type: String,\n//     match: regexp.countryCode,\n//   },\n//   accountName: {\n//     type: String,\n//   },\n//   bankName: {\n//     type: String,\n//   },\n//   bankAddress: {\n//     type: String,\n//   },\n//   bankCountry: {\n//     type: String,\n//     match: regexp.countryCode,\n//   },\n//   accountNumber: {\n//     type: String,\n//     match: regexp.accountNumber,\n//   },\n//   bic: { // ! INVALID STRUCTURE DATA ETT\n//     type: String,\n//     validate: {\n//       validator: (bic) => regexp.bic.test(bic)\n//         || regexp.sortCode.test(bic)\n//         || regexp.aba.test(bic),\n//     },\n//   },\n//   iban: {\n//     type: String,\n//     match: regexp.iban,\n//   },\n//   currencyCode: {\n//     type: String,\n//     match: regexp.currencyCode,\n//   },\n//   displayAccountNo: {\n//     type: String,\n//     match: regexp.accountNumber,\n//   },\n//   type: {\n//     type: String,\n//   },\n// });\n\nconst TransferSchema = new _mongoose.Schema({\n  id: {\n    type: Number\n  },\n  accountNumber: {\n    type: String\n  },\n  counterpart: {\n    type: Object\n  },\n  requestamount: {\n    type: Number\n  },\n  requestcurrency: {\n    type: String,\n    match: _regexp.default.currencyCode\n  },\n  narrative: {\n    type: String\n  },\n  status: {\n    type: String,\n    enum: ['cache', 'pending', 'rejected', 'approved']\n  },\n  date: {\n    type: String,\n    validate: {\n      validator: date => +new Date(date) < Date.now()\n    },\n    match: /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/\n  },\n  time: {\n    type: Number,\n    validate: date => date * 1000 < Date.now()\n  },\n  type: {\n    type: String\n  },\n  rule: {\n    type: RuleSchema,\n    ref: 'rule'\n  },\n  queue: {\n    type: String\n  },\n  quote: {\n    type: _mongoose.ObjectId,\n    ref: 'quote'\n  }\n}, {\n  timestamps: true\n});\n\nvar _default = (0, _mongoose.model)('transfer', TransferSchema);\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/models/transfer/index.js?");

/***/ }),

/***/ "./src/routes/accounts.js":
/*!********************************!*\
  !*** ./src/routes/accounts.js ***!
  \********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _expressPromiseRouter = _interopRequireDefault(__webpack_require__(/*! express-promise-router */ \"express-promise-router\"));\n\nvar _multer = _interopRequireDefault(__webpack_require__(/*! multer */ \"multer\"));\n\nvar _controllers = __webpack_require__(/*! ../controllers */ \"./src/controllers/index.js\");\n\nvar _validationScheme = __webpack_require__(/*! ../helpers/validation-scheme */ \"./src/helpers/validation-scheme/index.js\");\n\nvar _middlewares = __webpack_require__(/*! ../middlewares */ \"./src/middlewares/index.js\");\n\nvar _meta = __webpack_require__(/*! ../middlewares/meta */ \"./src/middlewares/meta/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst accountRouter = (0, _expressPromiseRouter.default)();\nconst validateTransferFacade = [_meta.paymentAccountMiddleware, _meta.transferMiddleware];\naccountRouter // account\n.post('/', [_middlewares.authentication, (0, _middlewares.validator)(_validationScheme.createAccountSchema)], _controllers.paymentAccountController.create).get('/', [_middlewares.authentication], _controllers.paymentAccountController.index).get('/history-transfer', [_middlewares.authentication], _controllers.transferController.historyTransfer).get('/:accountId', [_meta.paymentAccountMiddleware], _controllers.paymentAccountController.show).patch('/:accountId', [_meta.paymentAccountMiddleware], _controllers.paymentAccountController.update) // *** transfer *** //\n.post('/:accountId/transfer/:beneficiaryID', [_meta.paymentAccountMiddleware, _meta.beneficiaryMiddleware], _controllers.transferController.createTransfer).get('/:accountId/transfer/:transferId', [...validateTransferFacade], _controllers.transferController.showTransfer) // transfer document\n.post('/:accountId/transfer/:transferId/confirm', [...validateTransferFacade], _controllers.transferController.setReason).post('/:accountId/transfer/:transferId/document', [...validateTransferFacade, (0, _multer.default)().single('document')], _controllers.transferController.attachDocument).post('/:accountId/transfer/:transferId/submit', [...validateTransferFacade], _controllers.transferController.submitDocument); // *** transfer *** //\n\nvar _default = accountRouter;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/routes/accounts.js?");

/***/ }),

/***/ "./src/routes/apply.js":
/*!*****************************!*\
  !*** ./src/routes/apply.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _expressPromiseRouter = _interopRequireDefault(__webpack_require__(/*! express-promise-router */ \"express-promise-router\"));\n\nvar _controllers = __webpack_require__(/*! ../controllers */ \"./src/controllers/index.js\");\n\nvar _middlewares = __webpack_require__(/*! ../middlewares */ \"./src/middlewares/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// @ts-check\n// Controllers\nconst router = (0, _expressPromiseRouter.default)();\nrouter.get('/validate', [_middlewares.authentication], _controllers.apply.findBankDetails).get('/code', [_middlewares.authentication], _controllers.apply.code);\nvar _default = router;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/routes/apply.js?");

/***/ }),

/***/ "./src/routes/auth.js":
/*!****************************!*\
  !*** ./src/routes/auth.js ***!
  \****************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _expressPromiseRouter = _interopRequireDefault(__webpack_require__(/*! express-promise-router */ \"express-promise-router\"));\n\nvar _multer = _interopRequireDefault(__webpack_require__(/*! multer */ \"multer\"));\n\nvar _validationScheme = __webpack_require__(/*! ../helpers/validation-scheme */ \"./src/helpers/validation-scheme/index.js\");\n\nvar _controllers = __webpack_require__(/*! ../controllers */ \"./src/controllers/index.js\");\n\nvar _middlewares = __webpack_require__(/*! ../middlewares */ \"./src/middlewares/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// @ts-check\nconst authRouter = (0, _expressPromiseRouter.default)();\nauthRouter.post('/login', [(0, _middlewares.validator)(_validationScheme.loginScheme)], _controllers.auth.login).get('/logout', [_middlewares.authentication], _controllers.auth.logout).post('/create-customer', [(0, _middlewares.validator)(_validationScheme.createCustomerSchema)], _controllers.auth.createCustomer).post('/verification-confirm', [(0, _middlewares.validator)(_validationScheme.verificationConfirmSchema)], _controllers.auth.verificationConfirm).post('/verification-customer', [(0, _middlewares.formDataProcessing)({\n  maxFileSize: 16777216\n}), (0, _middlewares.validator)(_validationScheme.verificationCustomerSchema)], _controllers.auth.verificationCustomer) // get info data\n.get('/me', [_middlewares.authentication], _controllers.auth.me).get('/customer', [_middlewares.authentication], _controllers.auth.customer) // password\n.post('/password-reset', [(0, _middlewares.validator)(_validationScheme.passwordResetSchema)], _controllers.auth.passwordReset).post('/password-change', [(0, _middlewares.validator)(_validationScheme.passwordChangeSchema)], _controllers.auth.passwordChange).post('/password-confirm', [(0, _middlewares.validator)(_validationScheme.passwordConfirmSchema)], _controllers.auth.passwordConfirm).post('/check-email-is-exist', [(0, _middlewares.validator)(_validationScheme.checkEmailIsExistScheme)], _controllers.auth.checkEmailIsExist) // document\n.delete('/document/:nonce/:id', _controllers.auth.deleteDocument).post('/upload-document', (0, _multer.default)().single('document'), _controllers.auth.uploadDocument).post('/verification-document', _controllers.auth.verificationDocument) // changeData\n.put('/phone-change', [_middlewares.authentication], _controllers.auth.phoneChange).get('/phone-confirm', [_middlewares.authentication], _controllers.auth.phoneConfirm).put('/email-change', [_middlewares.authentication], _controllers.auth.emailChange).get('/email-confirm', [_middlewares.authentication], _controllers.auth.emailConfirm);\nvar _default = authRouter;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/routes/auth.js?");

/***/ }),

/***/ "./src/routes/beneficiaries.js":
/*!*************************************!*\
  !*** ./src/routes/beneficiaries.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _expressPromiseRouter = _interopRequireDefault(__webpack_require__(/*! express-promise-router */ \"express-promise-router\"));\n\nvar _multer = _interopRequireDefault(__webpack_require__(/*! multer */ \"multer\"));\n\nvar _validationScheme = __webpack_require__(/*! ../helpers/validation-scheme */ \"./src/helpers/validation-scheme/index.js\");\n\nvar _controllers = __webpack_require__(/*! ../controllers */ \"./src/controllers/index.js\");\n\nvar _middlewares = __webpack_require__(/*! ../middlewares */ \"./src/middlewares/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// @ts-check\n// Validation Schemas\n// Controllers\nconst beneficiaryRouter = (0, _expressPromiseRouter.default)();\nbeneficiaryRouter.get('/', [_middlewares.authentication], _controllers.beneficiary.index).get('/:id', [_middlewares.authentication], _controllers.beneficiary.show).post('/', [_middlewares.authentication, (0, _middlewares.validator)(_validationScheme.createBeneficiarySchema)], _controllers.beneficiary.create).patch('/:id', [_middlewares.authentication], _controllers.beneficiary.update).delete('/:id', [_middlewares.authentication], _controllers.beneficiary.delete) // verify\n.use('/:id/document', (0, _multer.default)().any()).post('/:id/document', _controllers.beneficiary.verificationDocument).put('/:id/verify', _controllers.beneficiary.verificationBeneficiary).post('/:id/sendVerificationLink', [_middlewares.authentication], _controllers.beneficiary.sendVerificationLink);\nvar _default = beneficiaryRouter;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/routes/beneficiaries.js?");

/***/ }),

/***/ "./src/routes/currencyCloud.js":
/*!*************************************!*\
  !*** ./src/routes/currencyCloud.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _expressPromiseRouter = _interopRequireDefault(__webpack_require__(/*! express-promise-router */ \"express-promise-router\"));\n\nvar _controllers = __webpack_require__(/*! ../controllers */ \"./src/controllers/index.js\");\n\nvar _middlewares = __webpack_require__(/*! ../middlewares */ \"./src/middlewares/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// @ts-check\n// Controllers\nconst router = (0, _expressPromiseRouter.default)();\nrouter.get('/bank-details', [_middlewares.authentication], _controllers.currencyCloud.findBankDetails);\nvar _default = router;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/routes/currencyCloud.js?");

/***/ }),

/***/ "./src/routes/hooks.js":
/*!*****************************!*\
  !*** ./src/routes/hooks.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _expressPromiseRouter = _interopRequireDefault(__webpack_require__(/*! express-promise-router */ \"express-promise-router\"));\n\nvar _controllers = __webpack_require__(/*! ../controllers */ \"./src/controllers/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// @ts-check\n// Controllers\nconst router = (0, _expressPromiseRouter.default)();\nrouter.use('/external', _controllers.hooks.external).use('/internal', _controllers.hooks.internal).use('/internal/:id', _controllers.hooks.internal).use('/deposit', _controllers.hooks.deposit).use('/deposit/:id', _controllers.hooks.internal).use('/message', _controllers.hooks.zendeskMessage);\nvar _default = router;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/routes/hooks.js?");

/***/ }),

/***/ "./src/routes/index.js":
/*!*****************************!*\
  !*** ./src/routes/index.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _expressPromiseRouter = _interopRequireDefault(__webpack_require__(/*! express-promise-router */ \"express-promise-router\"));\n\nvar _auth = _interopRequireDefault(__webpack_require__(/*! ./auth */ \"./src/routes/auth.js\"));\n\nvar _accounts = _interopRequireDefault(__webpack_require__(/*! ./accounts */ \"./src/routes/accounts.js\"));\n\nvar _beneficiaries = _interopRequireDefault(__webpack_require__(/*! ./beneficiaries */ \"./src/routes/beneficiaries.js\"));\n\nvar _hooks = _interopRequireDefault(__webpack_require__(/*! ./hooks */ \"./src/routes/hooks.js\"));\n\nvar _apply = _interopRequireDefault(__webpack_require__(/*! ./apply */ \"./src/routes/apply.js\"));\n\nvar _currencyCloud = _interopRequireDefault(__webpack_require__(/*! ./currencyCloud */ \"./src/routes/currencyCloud.js\"));\n\nvar _notification = _interopRequireDefault(__webpack_require__(/*! ./notification */ \"./src/routes/notification.js\"));\n\nvar _quote = _interopRequireDefault(__webpack_require__(/*! ./quote */ \"./src/routes/quote.js\"));\n\nvar _helpers = __webpack_require__(/*! ../helpers */ \"./src/helpers/index.js\");\n\nvar _validationScheme = __webpack_require__(/*! ../helpers/validation-scheme */ \"./src/helpers/validation-scheme/index.js\");\n\nvar _controllers = __webpack_require__(/*! ../controllers */ \"./src/controllers/index.js\");\n\nvar _middlewares = __webpack_require__(/*! ../middlewares */ \"./src/middlewares/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// Core\n// Routes\n// Instruments\n// MODELS\n// Controllers\nconst Router = (0, _expressPromiseRouter.default)(); // Registration routs\n\nRouter.use('/auth', _auth.default).use('/accounts', [_middlewares.authentication], _accounts.default).use('/beneficiaries', _beneficiaries.default).use('/notification', [_middlewares.authentication], _notification.default).use('/quote', [_middlewares.authentication], _quote.default).use('/apply', [_middlewares.authentication], _apply.default).use('/currency-cloud', [_middlewares.authentication], _currencyCloud.default).use('/hooks', _hooks.default); // Email\n\nRouter.post('/send-mail', [_middlewares.authentication, (0, _middlewares.validator)(_validationScheme.sendEmailSchema)], _controllers.email.send); // Back office\n\nRouter.get('/fees', [_middlewares.authentication], _controllers.backOffice.fees).get('/fields', [_middlewares.authentication], _controllers.backOffice.fields).get('/purposes', [_middlewares.authentication], _controllers.backOffice.purposes); // ShuftiProCallback\n\nRouter.post('/shuftipro-callback/verification-customer', [_middlewares.shuftiProSignature], _controllers.shuftiProCallback.verificationCustomer);\n/* Service */\n\nRouter.use('/ping', (req, res) => res.send(`PONG (${req.method}) : ${Date.now()}`)).use((req, res, next) => next(new _helpers.APIError(`${req.url} - Not Found`, 404)));\n/* Error handler */\n// eslint-disable-next-line no-unused-vars\n\nRouter.use((err, req, res, next) => {\n  switch (err.name) {\n    default:\n      {\n        var _err$response;\n\n        if (req.ettToken && (err === null || err === void 0 ? void 0 : (_err$response = err.response) === null || _err$response === void 0 ? void 0 : _err$response.status) === 403) {\n          return (0, _helpers.errorResponse)(res, {\n            error: _helpers.errorMessages.invalidToken,\n            action: 'auth-token',\n            status: 403\n          });\n        }\n\n        return (0, _helpers.errorResponse)(res, err);\n      }\n  }\n});\nvar _default = Router;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/routes/index.js?");

/***/ }),

/***/ "./src/routes/notification.js":
/*!************************************!*\
  !*** ./src/routes/notification.js ***!
  \************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _expressPromiseRouter = _interopRequireDefault(__webpack_require__(/*! express-promise-router */ \"express-promise-router\"));\n\nvar _controllers = __webpack_require__(/*! ../controllers */ \"./src/controllers/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// @ts-check\n// Controllers\nconst notificationRouter = (0, _expressPromiseRouter.default)();\nnotificationRouter.get('/', _controllers.notification.getAll).post('/', _controllers.notification.save).delete('/', _controllers.notification.delete).delete('/:id', _controllers.notification.delete);\nvar _default = notificationRouter;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/routes/notification.js?");

/***/ }),

/***/ "./src/routes/quote.js":
/*!*****************************!*\
  !*** ./src/routes/quote.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _expressPromiseRouter = _interopRequireDefault(__webpack_require__(/*! express-promise-router */ \"express-promise-router\"));\n\nvar _quote = _interopRequireDefault(__webpack_require__(/*! ../controllers/quote */ \"./src/controllers/quote/index.js\"));\n\nvar _models = __webpack_require__(/*! ../models */ \"./src/models/index.js\");\n\nvar _quote2 = _interopRequireDefault(__webpack_require__(/*! ../services/quote */ \"./src/services/quote.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst quoteService = new _quote2.default(_models.Quote);\nconst quoteController = new _quote.default(quoteService);\nconst quoteRouter = (0, _expressPromiseRouter.default)();\nquoteRouter.post('/', quoteController.create).get('/:quoteId', quoteController.findById);\nvar _default = quoteRouter;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/routes/quote.js?");

/***/ }),

/***/ "./src/server.js":
/*!***********************!*\
  !*** ./src/server.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

eval("\n\nvar _fs = _interopRequireDefault(__webpack_require__(/*! fs */ \"fs\"));\n\nvar _path = _interopRequireDefault(__webpack_require__(/*! path */ \"path\"));\n\nvar _http = _interopRequireDefault(__webpack_require__(/*! http */ \"http\"));\n\nvar _express = _interopRequireDefault(__webpack_require__(/*! express */ \"express\"));\n\nvar _helmet = _interopRequireDefault(__webpack_require__(/*! helmet */ \"helmet\"));\n\nvar _compression = _interopRequireDefault(__webpack_require__(/*! compression */ \"compression\"));\n\nvar _morgan = _interopRequireDefault(__webpack_require__(/*! morgan */ \"morgan\"));\n\nvar _middlewares = __webpack_require__(/*! ./middlewares */ \"./src/middlewares/index.js\");\n\nvar _response = __webpack_require__(/*! ./helpers/response */ \"./src/helpers/response/index.js\");\n\nvar _helpers = __webpack_require__(/*! ./helpers */ \"./src/helpers/index.js\");\n\nvar _routes = _interopRequireDefault(__webpack_require__(/*! ./routes */ \"./src/routes/index.js\"));\n\nvar _ws = _interopRequireDefault(__webpack_require__(/*! ./ws */ \"./src/ws.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* eslint-disable no-console */\n// Vanilla Node modules\n// Dependencies module\n// Instruments\n// Project modules\nconst app = (0, _express.default)();\nconst PORT = (0, _helpers.getEnv)('PORT');\napp.use((0, _middlewares.makeCors)()).get('/', (request, response) => response.redirect((0, _helpers.getEnv)('FE_APP_URL'))).use((0, _helmet.default)()).use((0, _compression.default)({})).use((0, _morgan.default)('tiny', {\n  stream: _fs.default.createWriteStream(_path.default.join(__dirname, '../logger.log'), {\n    flags: 'a'\n  })\n})).use(_middlewares.limiter).use(_express.default.json()).use(_express.default.urlencoded({\n  extended: false\n})).use('/uploads/', _express.default.static('uploads')).use('/api/v1', _routes.default).use((req, res) => (0, _response.errorResponse)(res, new _helpers.APIError(`${req.url} - Not Found`, 404), false));\n\n(async () => {\n  console.log('server try started');\n\n  try {\n    await (0, _helpers.connectDb)();\n\n    const server = _http.default.createServer(app);\n\n    (0, _ws.default)(server);\n    return server.listen(+PORT, err => {\n      if (err) throw new Error(err.message || err || 'express server: Something went wrong!');\n      console.log(`Server start on port ${PORT}`);\n      process.on('SIGINT', () => process.exit());\n      return false;\n    });\n  } catch (e) {\n    console.error(e);\n    return process.exit(1);\n  }\n})();\n\n//# sourceURL=webpack://blocfx-backend/./src/server.js?");

/***/ }),

/***/ "./src/services/backOffice.js":
/*!************************************!*\
  !*** ./src/services/backOffice.js ***!
  \************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _parser = __webpack_require__(/*! ../helpers/parser */ \"./src/helpers/parser/index.js\");\n\nclass BackOfficeService {\n  constructor(client) {\n    this.client = client;\n    this.cacheFields = {};\n    this.cachePurposes = [];\n    this.cacheFees = [];\n    this.loadingFees = null;\n  }\n\n  fees = async () => {\n    const getFees = async () => {\n      try {\n        if (!this.loadingFess) {\n          this.loadingFees = this.client.send({\n            url: '/backofficev2/xml/fees_list.php?tariffid=0'\n          });\n        }\n\n        const rawData = await this.loadingFees;\n        const rawJson = JSON.parse(rawData.body);\n\n        const formatFees = data => data.map(transfer => ({\n          id: transfer[0],\n          description: transfer[2],\n          currency: transfer[1],\n          fee: +transfer[7] ?? 0,\n          exception: transfer[5] === '-' ? null : transfer[5]\n        }));\n\n        const json = formatFees(rawJson.data);\n        this.cacheFees = json;\n        return this.cacheFees;\n      } catch {\n        return this.cacheFees;\n      }\n    };\n\n    if (this.cacheFees.length) {\n      getFees();\n      return this.cacheFees;\n    }\n\n    return await getFees();\n  };\n  fields = async (countryCode = '') => {\n    if (this.cacheFields[countryCode]) {\n      return this.cacheFields[countryCode];\n    }\n\n    const {\n      body\n    } = await this.client.send({\n      url: `/backofficev2/beneficiary_ext_form.php?countryCode=${countryCode}`\n    });\n    const fieldsNoAccept = ['currencyCode', 'bankName', 'bankAddress', 'bic'];\n    let fields = [];\n    fields = body.match(/for=\"[a-zA-Z]+\"/gi).map(label => label.replace(/for=|[^a-zA-Z]/g, '').replace('swiftBic', 'bic')).filter(field => !fieldsNoAccept.includes(field));\n    this.cacheFields[countryCode] = fields;\n    return this.cacheFields[countryCode];\n  };\n  purposes = async () => {\n    const getDataPurpose = async () => {\n      try {\n        const {\n          body\n        } = await this.client.send({\n          url: '/backofficev2/compliance.php'\n        });\n        const data = (0, _parser.purposeParser)(body);\n        this.cachePurposes = data;\n        return this.cachePurposes;\n      } catch {\n        return this.cachePurposes;\n      }\n    };\n\n    if (this.cachePurposes.length) {\n      getDataPurpose();\n      return this.cachePurposes;\n    }\n\n    return await getDataPurpose();\n  };\n  verificationBeneficiary = async (beneficiary, customerID) => {\n    var _name$split, _name$split2, _name$split3;\n\n    const {\n      currencyCode = '',\n      country = '',\n      firstName = '',\n      lastName = '',\n      name = '',\n      dob = '',\n      iban = '',\n      sortCode = '',\n      accountNumber = '',\n      bic = '',\n      bankName = '',\n      bankAddress = ''\n    } = beneficiary;\n    const formData = {\n      identityType: beneficiary.type,\n      firstName: firstName || ((_name$split = name.split(' ')) === null || _name$split === void 0 ? void 0 : _name$split[0]) || '',\n      name: lastName || ((_name$split2 = name.split(' ')) === null || _name$split2 === void 0 ? void 0 : _name$split2[1]) || ((_name$split3 = name.split(' ')) === null || _name$split3 === void 0 ? void 0 : _name$split3[0]) || '',\n      street: beneficiary.street || '',\n      // city: 'London',\n      // postCode: 'test',\n      country,\n      nationality: country,\n      dob: new Date(dob).toISOString().split('T')[0],\n      accountName: name,\n      bankCountry: country,\n      currencyCode,\n      iban,\n      bankName,\n      bankAddress,\n      accountNumber,\n      swiftBic: bic,\n      sortCode,\n      originalCurrency: currencyCode,\n      chkVerified: 'on',\n      customerID,\n      from: 'customer',\n      id: beneficiary.ettId,\n      transferType: 'E',\n      accountID: customerID,\n      customerStatus: 'Active'\n    };\n    return await this.client.send({\n      method: 'POST',\n      url: '/backofficev2/beneficiary_save.php',\n      formData\n    });\n  };\n}\n\nvar _default = BackOfficeService;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/services/backOffice.js?");

/***/ }),

/***/ "./src/services/hook.js":
/*!******************************!*\
  !*** ./src/services/hook.js ***!
  \******************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _zendesk = __webpack_require__(/*! ../helpers/parser/zendesk */ \"./src/helpers/parser/zendesk.js\");\n\nvar _models = __webpack_require__(/*! ../models */ \"./src/models/index.js\");\n\nvar _ws = _interopRequireDefault(__webpack_require__(/*! ../ws */ \"./src/ws.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nclass HookService {\n  async zendeskMessage(responseMessage = '') {\n    const message = (0, _zendesk.getLastMessageText)(responseMessage);\n    const subject = (0, _zendesk.getMetaDataIntoZendesk)(responseMessage, 'Subject');\n    const customerId = (0, _zendesk.getMetaDataIntoZendesk)(responseMessage, 'Customer ID');\n    const date = Date.now();\n    const data = {\n      title: subject,\n      body: message,\n      uId: +customerId,\n      date\n    };\n\n    if (Object.values(data).every(value => !!value)) {\n      (0, _ws.default)().emit(data.uId)('notification')(data);\n      return await _models.Notification.create(data);\n    }\n\n    return null;\n  }\n\n}\n\nvar _default = HookService;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/services/hook.js?");

/***/ }),

/***/ "./src/services/index.js":
/*!*******************************!*\
  !*** ./src/services/index.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nclass Service {\n  constructor(Model) {\n    this.Model = Model;\n  }\n\n}\n\nvar _default = Service;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/services/index.js?");

/***/ }),

/***/ "./src/services/quote.js":
/*!*******************************!*\
  !*** ./src/services/quote.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _ = _interopRequireDefault(__webpack_require__(/*! . */ \"./src/services/index.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* eslint-disable no-useless-constructor */\n// import { ettClientAuthToken } from '../client/ett.client';\nclass QuoteService extends _.default {\n  constructor(Model) {\n    super(Model);\n  }\n\n  async create(request) {\n    const {\n      // direction = ['sell', 'buy'][0],\n      // amount = 1,\n      sellcurrency,\n      buycurrency\n    } = request.body; // const { forexQuote } = await ettClientAuthToken(request).get('/forex/quote', {\n    //   params: {\n    //     direction,\n    //     amount,\n    //     sellcurrency,\n    //     buycurrency,\n    //   },\n    // });\n    // const newQuote = new this.Model(forexQuote);\n    // await newQuote.validate();\n    // await newQuote.save();\n    // return forexQuote;\n\n    return await this.Model.findOne({\n      sellcurrency,\n      buycurrency\n    });\n  }\n\n  async findById(id) {\n    const quote = await this.Model.find({\n      id\n    });\n    return quote;\n  }\n\n}\n\nvar _default = QuoteService;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/services/quote.js?");

/***/ }),

/***/ "./src/services/transfer.js":
/*!**********************************!*\
  !*** ./src/services/transfer.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _dayjs = _interopRequireDefault(__webpack_require__(/*! dayjs */ \"dayjs\"));\n\nvar _ = _interopRequireDefault(__webpack_require__(/*! . */ \"./src/services/index.js\"));\n\nvar _ett = __webpack_require__(/*! ../client/ett.client */ \"./src/client/ett.client.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* eslint-disable no-useless-constructor */\nclass TransferService extends _.default {\n  constructor(Model) {\n    super(Model);\n  }\n\n  async createTransferEtt(request, transaction, accountNumber, forex) {\n    const transactionPayload = {\n      time: (0, _dayjs.default)().unix(),\n      currency: transaction.requestcurrency,\n      beneficiaryid: transaction.counterpart.id,\n      amount: transaction.requestamount,\n      narrative: transaction.narrative\n    };\n\n    const requestETT = async (params = transactionPayload) => await (0, _ett.ettClientAuthToken)(request).post(`/account/${accountNumber ?? transaction.accountNumber}/transfer`, params);\n\n    let ettRes = null;\n\n    try {\n      ettRes = await requestETT({ ...transactionPayload,\n        forex: forex ?? transaction.quote.id\n      });\n    } catch (e) {\n      ettRes = await requestETT(transactionPayload);\n    }\n\n    return ettRes;\n  }\n\n  async createTransfer(transfer) {\n    const newTransfer = new this.Model(transfer);\n    await newTransfer.validate();\n    return await newTransfer.save();\n  }\n\n  async inspectAmountOrCreateTransferEtt(request, transfer = {}, account = {}) {\n    if (transfer.status === 'cache' && account.available >= transfer.requestamount) {\n      const ettRes = await this.createTransferEtt(request, transfer);\n      const transferInspectAmount = { ...transfer,\n        ...ettRes\n      };\n      return await this.Model.updateOne({\n        id: transfer.id\n      }, transferInspectAmount);\n    }\n\n    return transfer;\n  }\n\n}\n\nvar _default = TransferService;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/services/transfer.js?");

/***/ }),

/***/ "./src/services/zenDesk.js":
/*!*********************************!*\
  !*** ./src/services/zenDesk.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nvar _zenDesk = _interopRequireDefault(__webpack_require__(/*! ../client/zenDesk.client */ \"./src/client/zenDesk.client.js\"));\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nconst getLocalDate = () => new Date().toISOString().replace('T', ' ').substr(0, 19);\n\nconst formattedChangeMessage = (name, oldData, newData) => `\n- Please change my ${name}: ${oldData} -\n- To new ${name}: (${newData}) -\n`;\n\nclass ZenDeskService {\n  constructor() {\n    this.subject = 'Service message from BlockFX: ';\n  }\n\n  setSubject(subjectName = 'message') {\n    return this.subject + subjectName;\n  }\n\n  async message(requester, message, subjectName) {\n    const slicingString = '\\n****\\n';\n    const body = `\n      - Open request   : ${getLocalDate()}  -\n      - Subject        : ${subjectName}     -\n      - Customer ID    : ${requester.id}    -\n      - Customer Name  : ${requester.name}  -\n      - Customer Email : ${requester.email} -\n      - Customer Phone : ${requester.phone} -\n      ${slicingString}\n      ${message}\n      ${slicingString}`;\n    const payload = {\n      request: {\n        requester: {\n          name: requester.name\n        },\n        subject: this.setSubject(subjectName),\n        comment: {\n          body\n        }\n      }\n    };\n    return await _zenDesk.default.post('/requests', payload);\n  }\n\n  async change(requester, body, name) {\n    return await this.message(requester, body, `Change ${name}`);\n  }\n\n  async changePhone(requester, phone) {\n    const changeName = 'phone number';\n    const body = formattedChangeMessage(changeName, requester.phone, phone);\n    return await this.change(requester, body, changeName);\n  }\n\n  async changeEmail(requester, email) {\n    const changeName = 'email address';\n    const body = formattedChangeMessage(changeName, requester.email, email);\n    return await this.change(requester, body, changeName);\n  }\n\n}\n\nvar _default = ZenDeskService;\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/services/zenDesk.js?");

/***/ }),

/***/ "./src/ws.js":
/*!*******************!*\
  !*** ./src/ws.js ***!
  \*******************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nexports.default = void 0;\n\nconst WebSocket = __webpack_require__(/*! ws */ \"ws\");\n\nconst userConnects = {}; // initialize client connect\n\nlet socket = null; // initialize socket server\n// PATTERN chain of responsibility\n\nclass Socket {\n  constructor(server) {\n    this.listeners = {}; // PATTERN OBSERVER\n\n    this.wss = new WebSocket.Server({\n      server\n    });\n    this.wss.on('connection', connect => {\n      // LISTEN connect ws server\n      connect.on('message', rawData => {\n        // LISTEN message from client\n        const {\n          event,\n          data\n        } = JSON.parse(rawData); // get data message\n\n        switch (event) {\n          case 'connect':\n            // initialize connect client\n            userConnects[data] = connect; // clear data client and connect if client disconnected\n\n            userConnects[data].on('close', () => delete userConnects[data]); // send client status is connect\n\n            this.emit(connect)('message')({\n              title: 'external',\n              id: 413,\n              body: 'Confirmed'\n            });\n            return connect.send(JSON.stringify({\n              event,\n              data: true\n            }));\n\n          default:\n            // if no listener send error\n            if (!this.listeners[event]) this.emit(connect)('error')({\n              message: `event (${event}): not found!`\n            }); // start method listener\n            else this.listeners[event].forEach(method => method(data, connect, event));\n            return this;\n        }\n      });\n    });\n    /**\n     * open listener\n     * @param {Array<string>} events\n     */\n\n    this.listen = (...events) => (...methods) => {\n      events.forEach(event => {\n        var _this$listeners, _this$listeners$event;\n\n        if (!((_this$listeners = this.listeners) !== null && _this$listeners !== void 0 && (_this$listeners$event = _this$listeners[event]) !== null && _this$listeners$event !== void 0 && _this$listeners$event.length)) this.listeners[event] = methods;else this.listeners[event].concat(methods);\n      });\n      return this;\n    }; // send event and data from clients\n\n    /**\n     * @param {Array<string>| Array<{}>} clients\n     */\n\n\n    this.emit = (...clients) => event => data => {\n      const rawResponse = {\n        event,\n        data\n      };\n      const response = JSON.stringify(rawResponse);\n\n      if (clients.length) {\n        clients.forEach(client => {\n          const foundClient = userConnects === null || userConnects === void 0 ? void 0 : userConnects[client];\n          foundClient === null || foundClient === void 0 ? void 0 : foundClient.send(response);\n        });\n      } else {\n        userConnects.forEach(client => client.send(response));\n      }\n\n      return this;\n    };\n    /**\n     * remove prevent open listener\n     *\n     * @param {string} event\n     * @param {Function} method\n     */\n\n\n    this.removeListen = (event, method) => {\n      if (method) {\n        this.listeners[event].filter(listen => listen !== method); // if remove method into listeners\n      } else {\n        delete this.listeners[event]; // remove listeners\n      }\n\n      return this;\n    };\n  }\n\n} // SINGLETON\n// if is open connection return connection else open connection\n\n\nvar _default = server => {\n  if (!socket) socket = new Socket(server);\n  return socket;\n};\n\nexports.default = _default;\n\n//# sourceURL=webpack://blocfx-backend/./src/ws.js?");

/***/ }),

/***/ "@godaddy/terminus":
/*!************************************!*\
  !*** external "@godaddy/terminus" ***!
  \************************************/
/***/ ((module) => {

module.exports = require("@godaddy/terminus");;

/***/ }),

/***/ "ajv":
/*!**********************!*\
  !*** external "ajv" ***!
  \**********************/
/***/ ((module) => {

module.exports = require("ajv");;

/***/ }),

/***/ "ajv-keywords":
/*!*******************************!*\
  !*** external "ajv-keywords" ***!
  \*******************************/
/***/ ((module) => {

module.exports = require("ajv-keywords");;

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/***/ ((module) => {

module.exports = require("axios");;

/***/ }),

/***/ "compression":
/*!******************************!*\
  !*** external "compression" ***!
  \******************************/
/***/ ((module) => {

module.exports = require("compression");;

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/***/ ((module) => {

module.exports = require("cors");;

/***/ }),

/***/ "crypto":
/*!*************************!*\
  !*** external "crypto" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("crypto");;

/***/ }),

/***/ "crypto-js/aes":
/*!********************************!*\
  !*** external "crypto-js/aes" ***!
  \********************************/
/***/ ((module) => {

module.exports = require("crypto-js/aes");;

/***/ }),

/***/ "crypto-js/enc-utf8":
/*!*************************************!*\
  !*** external "crypto-js/enc-utf8" ***!
  \*************************************/
/***/ ((module) => {

module.exports = require("crypto-js/enc-utf8");;

/***/ }),

/***/ "dayjs":
/*!************************!*\
  !*** external "dayjs" ***!
  \************************/
/***/ ((module) => {

module.exports = require("dayjs");;

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/***/ ((module) => {

module.exports = require("express");;

/***/ }),

/***/ "express-promise-router":
/*!*****************************************!*\
  !*** external "express-promise-router" ***!
  \*****************************************/
/***/ ((module) => {

module.exports = require("express-promise-router");;

/***/ }),

/***/ "express-rate-limit":
/*!*************************************!*\
  !*** external "express-rate-limit" ***!
  \*************************************/
/***/ ((module) => {

module.exports = require("express-rate-limit");;

/***/ }),

/***/ "formidable":
/*!*****************************!*\
  !*** external "formidable" ***!
  \*****************************/
/***/ ((module) => {

module.exports = require("formidable");;

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/***/ ((module) => {

module.exports = require("fs");;

/***/ }),

/***/ "fs-extra":
/*!***************************!*\
  !*** external "fs-extra" ***!
  \***************************/
/***/ ((module) => {

module.exports = require("fs-extra");;

/***/ }),

/***/ "fs/promises":
/*!******************************!*\
  !*** external "fs/promises" ***!
  \******************************/
/***/ ((module) => {

module.exports = require("fs/promises");;

/***/ }),

/***/ "handlebars":
/*!*****************************!*\
  !*** external "handlebars" ***!
  \*****************************/
/***/ ((module) => {

module.exports = require("handlebars");;

/***/ }),

/***/ "helmet":
/*!*************************!*\
  !*** external "helmet" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("helmet");;

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/***/ ((module) => {

module.exports = require("http");;

/***/ }),

/***/ "jsonwebtoken":
/*!*******************************!*\
  !*** external "jsonwebtoken" ***!
  \*******************************/
/***/ ((module) => {

module.exports = require("jsonwebtoken");;

/***/ }),

/***/ "lodash":
/*!*************************!*\
  !*** external "lodash" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("lodash");;

/***/ }),

/***/ "mongoose":
/*!***************************!*\
  !*** external "mongoose" ***!
  \***************************/
/***/ ((module) => {

module.exports = require("mongoose");;

/***/ }),

/***/ "morgan":
/*!*************************!*\
  !*** external "morgan" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("morgan");;

/***/ }),

/***/ "multer":
/*!*************************!*\
  !*** external "multer" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("multer");;

/***/ }),

/***/ "nodemailer":
/*!*****************************!*\
  !*** external "nodemailer" ***!
  \*****************************/
/***/ ((module) => {

module.exports = require("nodemailer");;

/***/ }),

/***/ "os":
/*!*********************!*\
  !*** external "os" ***!
  \*********************/
/***/ ((module) => {

module.exports = require("os");;

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/***/ ((module) => {

module.exports = require("path");;

/***/ }),

/***/ "process":
/*!**************************!*\
  !*** external "process" ***!
  \**************************/
/***/ ((module) => {

module.exports = require("process");;

/***/ }),

/***/ "qs":
/*!*********************!*\
  !*** external "qs" ***!
  \*********************/
/***/ ((module) => {

module.exports = require("qs");;

/***/ }),

/***/ "request":
/*!**************************!*\
  !*** external "request" ***!
  \**************************/
/***/ ((module) => {

module.exports = require("request");;

/***/ }),

/***/ "ws":
/*!*********************!*\
  !*** external "ws" ***!
  \*********************/
/***/ ((module) => {

module.exports = require("ws");;

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/server.js");
/******/ 	
/******/ })()
;