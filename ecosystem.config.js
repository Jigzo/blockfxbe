module.exports = {
  apps: [{
    name: 'BlocFX',
    script: './build/index.js',
    watch: true,
    instances: 'max',
    exec_mode: 'cluster',
  }],
};
