// @ts-check
// Core
import axios from 'axios';

// Instruments
import { APIError, errorMessages, getEnv } from '../helpers';


const applyClient = axios.create({
  baseURL: getEnv('APPLY_API_URL'),
});

applyClient.interceptors.response.use(
  (response) => {
    const { data } = response;

    if (data?.status === 'FAIL') {
      return Promise.reject(new APIError(data?.comment || errorMessages.applyError));
    }

    return response;
  }, (error) => {
    const { data, status } = error.response;

    return Promise.reject(new APIError(data?.error?.message || errorMessages.applyError, status));
  },
);

export { applyClient };
