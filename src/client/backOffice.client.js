import request from 'request';
import { getEnv } from '../helpers';


class BackOfficeClient {
  constructor() {
    this.API_URL = getEnv('BACK_OFFICE_URL');
    this.login = getEnv('BACK_OFFICE_USERNAME');
    this.password = getEnv('BACK_OFFICE_PASSWORD');
    this.Cookie = null;
    this.CookieDate = null;
    this._loadingSession = null;
  }

  getSession = async () => {
    if (!this._loadingSession) {
      if (this.CookieDate && this.CookieDate + 300000 >= Date.now()) {
        this._loadingSession = Promise.resolve(this.Cookie);
      } else {
        this._loadingSession = new Promise((resolve, reject) => {
          const sessionOptions = {
            method: 'POST',
            url: `${this.API_URL}/shared/login_check.php`,
            headers: {
              Cookie: this.Cookie || '',
            },
            formData: {
              loginName: this.login,
              password: this.password,
              code: '',
              type: 'B',
            },
          };

          request(sessionOptions, (errorSession, responseSession) => {
            if (errorSession) reject(errorSession);
            this.Cookie = responseSession.headers['set-cookie'] || responseSession.request.originalCookieHeader;
            if (this.Cookie) {
              this.CookieDate = Date.now();
              resolve(this.Cookie);
            } else reject();
          });
        });
      }
    }

    const data = await this._loadingSession;
    this._loadingSession = null;

    return data;
  };

  send = async (options = {}) => {
    const Cookie = await this.getSession();
    const payload = {
      method: 'GET',
      ...options,
      url: `${this.API_URL}${options.url}`,
      headers: {
        Cookie,
        ...(options.headers || {}),
      },
    };

    return await new Promise((resolve, reject) => {
      request(payload, (error, data) => {
        if (error) reject(error);
        resolve(data);
      });
    });
  }
}

export default BackOfficeClient;
