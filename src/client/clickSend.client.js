// @ts-check
// Core
import axios from 'axios';

// Instruments
import { getEnv } from '../helpers';


const basicToken = Buffer.from(`${getEnv('CLICKSEND_USERNAIM')}:${getEnv('CLICKSEND_API_KEY')}`).toString('base64');
const clickSendClient = axios.create({
  baseURL: getEnv('CLICKSEND_API_URL'),
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: `Basic ${basicToken}`,
  },
});

export { clickSendClient };
