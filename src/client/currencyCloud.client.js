// @ts-check
// Core
import axios from 'axios';

// Instruments
import { getEnv } from '../helpers';


const currencyCloudClient = axios.create({
  baseURL: getEnv('CURRENCY_CLOUD_API_URL'),
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

export { currencyCloudClient };
