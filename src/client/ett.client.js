// Core
import axios from 'axios';

// Instruments
import { getEnv, APIError } from '../helpers';
// eslint-disable-next-line import/no-cycle
import auth from '../core/auth';


const ettClientDefaultOptions = {
  baseURL: getEnv('BASE_API_URL'),
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};

const interceptorsDefaultResponse = (response) => {
  // Status Code isn't a success code - throw error
  if (response.data?.statusCode !== 0) {
    throw new APIError(response.data?.errorMessage, 400);
  }
  return response.data;
};

const ettClient = axios.create(ettClientDefaultOptions);

ettClient.interceptors.response.use(
  interceptorsDefaultResponse,
);

const ettClientAuthToken = (req, handleApiError = true) => {
  const authEttClient = axios.create({
    ...ettClientDefaultOptions,
    headers: {
      ...ettClientDefaultOptions.headers,
      Authorization: `Basic ${req.ettToken}`,
    },
  });
  authEttClient.interceptors.response.use(
    async (response) => {
      const { token, ...data } = response?.data;
      if (token && req.ettUserTokenId) {
        data.authToken = await auth.refreshEttToken('token', req.ettUserTokenId, req.customer);
      }
      if (handleApiError) {
        interceptorsDefaultResponse(response);
      }

      return data;
    },
  );

  return authEttClient;
};

export { ettClient, ettClientAuthToken };
