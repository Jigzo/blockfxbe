// @ts-check
// Core
import axios from 'axios';

// Instruments
import crypto from 'crypto';
import { APIError, errorMessages, getEnv } from '../helpers';


const token = Buffer.from((`${getEnv('SHUFTIPRO_CLIENT_ID')}:${getEnv('SHUFTIPRO_SECRET_KEY')}`)).toString('base64');

const shuftiproClient = axios.create({
  baseURL: getEnv('SHUFTIPRO_API_URL'),
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: `Basic ${token}`,
  },
});

const validateSignature = ({ data, signature }) => {
  const stringData = JSON.stringify(data).replace(/\//g, '\\/');

  const hash = crypto.createHash('sha256')
    .update(`${stringData}${getEnv('SHUFTIPRO_SECRET_KEY')}`)
    .digest('hex');

  return (hash === signature);
};


shuftiproClient.interceptors.response.use((response) => {
  const { data, headers } = response;
  if (!validateSignature({ data, signature: headers?.signature })) {
    throw new APIError(response.data?.error?.message || errorMessages.shuftiProSignature, 400);
  }
  return response;
}, (error) => {
  const { data, status } = error.response;

  return Promise.reject(new APIError(data?.error?.message || 'ShuftiPro response error', status));
});

export { shuftiproClient, validateSignature };
