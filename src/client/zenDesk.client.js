import axios from 'axios';
import { getEnv } from '../helpers';


const zenDeskClient = axios.create({
  baseURL: getEnv('ZENDESK_API_URL'),
});

export default zenDeskClient;
