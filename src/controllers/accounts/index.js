import { ettClientAuthToken } from '../../client/ett.client';
import { successResponse, bankValidator } from '../../helpers/response';
import { PaymentAccount } from '../../models';
import regexp from '../../helpers/regexp';

// import apply from '../../core/apply';

const ETT_URL = 'account';
const paymentAccountController = {
  index: async (req, res) => {
    // const { accounts, ...responseData } = await ettClientAuthToken(req).get(ETT_URL);
    // const accountsNumber = accounts.map(({ accountNumber }) => accountNumber);
    // const paymentAccounts = await PaymentAccount.find({
    //   accountNumber: {
    //     $in: accountsNumber,
    //   },
    // }).lean();

    // const mergeAccounts = accounts.map((item) => {
    //   const paymentAccount = paymentAccounts.find(({ accountNumber }) => accountNumber === item.accountNumber);
    //   const name = paymentAccount?.name || item?.name || '';

    //   return { ...paymentAccount, ...item, name };
    // });

    // return successResponse(res, {
    //   data: mergeAccounts,
    //   ...responseData,
    // });

    const paymentAccounts = await PaymentAccount.find();

    return successResponse(res, {
      data: paymentAccounts,
      total: paymentAccounts.length,
    });
  },

  show: async (req, res) => {
    const { accounts } = await ettClientAuthToken(req).get(`${ETT_URL}/${req.params.accNumber}`);
    const ettAcc = accounts?.[0] || {};
    let paymentAccount = {};
    if (ettAcc.accountNumber) {
      paymentAccount = await PaymentAccount.findOne({
        accountNumber: ettAcc.accountNumber,
      }).lean() || {};
    }

    return successResponse(res, {
      data: {
        ...paymentAccount,
        ...ettAcc,
      },
    });
  },

  create: async (req, res) => {
    const {
      country = '',
      currency = '',
      // name = '',
      bic = '',
      sortCode = '',
      aba = '',
      bankAddress = '',
      accountType = '',
      bankName = '',
    } = req.body;

    let {
      iban = '',
      accountNumber = '',
    } = req.body;

    const isInvalidBank = bankValidator(req, res);
    if (isInvalidBank) return false;

    if (!iban && accountNumber && regexp.iban.test(accountNumber)) {
      iban = accountNumber;
      accountNumber = accountNumber.substr(14);
    } else if (accountNumber.length < 7 && iban) {
      const accountNumberIntoIban = iban.substr(14);
      if (/^[0-9]{7,24}$/.test(accountNumberIntoIban)) {
        accountNumber = accountNumberIntoIban;
      }
    }

    // *** APPLY VALIDATOR IBAN AND ACCOUNT_NUMBER *** //
    // const dataApply = await apply.verification(country, {
    //   accountNumber: iban || accountNumber,
    //   sortCode,
    //   bic,
    // });

    // if (dataApply.status !== 'PASS') {
    //   throw new Error('bank data is invalid');
    // }
    // *** APPLY VALIDATOR IBAN AND ACCOUNT_NUMBER *** //

    // const ettAccountRes = await ettClientAuthToken(req).post(ETT_URL, {
    //   currency,
    //   name: name || bankName,
    //   accountNumber: iban || accountNumber,
    // });

    // *** payload Data format *** //
    const funding = {
      country,
    };

    // ! ONLY INTERNATIONAL
    if (bic) funding.bic = bic;
    if (iban) funding.iban = iban;

    // ! ONLY LOCAL
    // *** local validation *** //
    const isLikelyLocal = country === currency.substr(0, 2);
    const isLikelySortCode = isLikelyLocal && ['GB'].includes(country);
    const isLikelyABA = isLikelyLocal && ['US'].includes(country);

    const currentSortCode = regexp.sortCode.test(sortCode);
    const currentAba = regexp.aba.test(aba);
    // *** local validation *** //
    if (accountNumber) funding.accountNumber = accountNumber;
    if (isLikelySortCode && currentSortCode && sortCode) funding.sortCode = sortCode;
    if (isLikelyABA && currentAba && aba) funding.aba = aba;
    if (accountType && isLikelyLocal) funding.accountType = accountType;

    // OTHER BANK DATA
    if (bankName) funding.bankName = bankName;
    if (bankAddress) funding.bankAddress = bankAddress;
    // *** payload Data format *** //

    const paymentAccount = await PaymentAccount.create({
      reference: req.customer.reference,
      accountNumber: 'FAKE',
      // accountNumber: ettAccountRes.accountNumber,
      funding,
      currency,
    });

    return successResponse(res, { data: paymentAccount });
  },

  update: async (request, response) => {
    const { accountNumber } = request.params;
    const { name } = request.body;
    const { accounts } = await ettClientAuthToken(request).get(`${ETT_URL}/${accountNumber}`);

    const newAccountData = {
      ...accounts,
      name,
    };

    const account = await PaymentAccount.findOneAndUpdate({ accountNumber }, newAccountData, { upsert: true });

    return response.json({ account });
  },
};

export default paymentAccountController;
