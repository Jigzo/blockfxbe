import { successResponse } from '../../helpers/response';
import apply from '../../core/apply';


export default {
  findBankDetails: async (req, res) => {
    const {
      countryCode, bankName, pageSize, address, bic8,
    } = req.query;

    const data = await apply.findBankDetails(countryCode, bankName, address, bic8, pageSize);

    return successResponse(res, { data });
  },

  code: async (req, res) => {
    const { countryCode, nationalId, accountNumber } = req.query;
    const data = await apply.code(countryCode, nationalId, accountNumber);

    return successResponse(res, { data });
  },
};
