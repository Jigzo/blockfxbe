/* eslint-disable no-plusplus */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
/* eslint-disable radix */
import fs from 'fs/promises';
import path from 'path';
import process from 'process';
import auth from '../../core/auth';
import { successResponse } from '../../helpers/response';
import { ettClientAuthToken, ettClient } from '../../client/ett.client';
import ShuftiPro from '../../core/shuftiPro';
import { getEnv } from '../../helpers/config';
import { createHashSha1 } from '../../helpers/utils';
import {
  EmailConfirm,
  EttCustomer,
  EttCustomerDocument,
  PhoneConfirm,
  StakeholderDocumentData,
  StakeholderDocumentFile,
} from '../../models';
import uploadFile from '../../helpers/upload';
import regexp from '../../helpers/regexp';
import clickSend from '../../core/clickSend';
import ZenDeskService from '../../services/zenDesk';
import { getRandomStringNumber } from '../../helpers/generator';
import { sendMail } from '../../helpers';


const zenDeskService = new ZenDeskService();
const shuftiPro = new ShuftiPro();
export default ({
  login: async (request, response) => {
    const {
      login,
      password,
    } = request.body;

    try {
      const authToken = await auth.login({ login, password });
      return successResponse(response, { authToken });
    } catch (error) {
      if (error.status === 400) {
        throw new Error('ETT is no connect!');
      } else {
        throw error;
      }
    }
  },
  logout: async (req, res) => {
    await auth.logout(req.ettUserTokenId);

    return successResponse(res, true);
  },
  me: async (req, res) => {
    const { user, ...other } = await ettClientAuthToken(req).get('/user/me');

    return successResponse(res, { data: user, ...other });
  },
  customer: async (request, response) => {
    const { customer } = request;
    return successResponse(response, {
      data: customer,
    });
  },
  passwordReset: async (req, res) => {
    const encodeMail = encodeURIComponent(req.body.email);
    const responseData = await ettClient.get(`/password/reset/${encodeMail}`);
    return successResponse(res, responseData);
  },
  passwordConfirm: async (req, res) => {
    const responseData = await ettClient.post('password/confirm', req.body);
    return successResponse(res, responseData);
  },
  passwordChange: async (req, res) => {
    const responseData = await ettClient.post('/password', req.body);
    return successResponse(res, responseData);
  },

  createCustomer: async (req, res) => {
    const {
      country = '',
      email = '',
      phone = '',
    } = req.body;

    if (!regexp.countryCode.test(country)) throw new Error('Country code is invalid');
    if (!regexp.phone.test(phone)) throw new Error('Phone is invalid');
    if (!regexp.email.test(email)) throw new Error('Email is invalid');

    const customerData = {
      ...req.body,
      roles: [
        {
          role: 'signatory',
        },
      ],
    };

    const data = JSON.stringify({
      country,
      stakeholders: [customerData],
    });

    const ettResponseData = await ettClient.get('/application.php', {
      params: {
        request: 'new',
        type: 'personal',
        key: getEnv('BASE_API_KEY'),
        hash: createHashSha1(`personal${data}`.replace(/\//g, '\\/')),
        data,
      },
    });

    await EttCustomer.create({
      ...customerData,
      ettAppId: ettResponseData.reference,
    });

    return successResponse(res, ettResponseData);
  },

  verificationConfirm: async (req, response) => {
    const { nonce } = req.body;
    const { application } = await ettClient.post('/application/confirm', { nonce });

    const stakeholderDocumentData = await StakeholderDocumentData
      .findOne({ nonce }).populate('stakeholderDocumentFiles');

    application.stakeholders[0].requirements = await Promise.all(application.stakeholders[0].requirements.map(async (
      {
        id: requirementId,
        types: requirementTypes,
        ...requirement
      },
    ) => {
      requirementTypes = await Promise.all(requirementTypes.map(async ({ id: typeId, ...requirementType }) => {
        const { fileUrl = null } = stakeholderDocumentData
          ?.stakeholderDocumentFiles
          ?.find(({
            requirement: requirementDBId,
            type: typeDBId,
          }) => requirementDBId === requirementId && typeDBId === typeId) || { fileUrl: null };

        if (fileUrl) {
          requirementType.fileUrl = fileUrl;
        }

        return {
          id: typeId,
          ...requirementType,
        };
      }));

      return {
        ...requirement,
        id: requirementId,
        types: requirementTypes,
      };
    }));

    const data = {
      application,
    };
    return response.json({ data });
  },

  uploadDocument: async (request, response) => {
    const { file } = request;
    if (!file) throw new Error('file is required');

    const {
      nonce = '',
      mode = '',
      stakeholder: rawStakeholder = 0,
      requirement: rawRequirement = 0,
      type: rawType = 0,
    } = request.body;

    if (!nonce.length) throw new Error('Nonce is required');
    if (!mode.length) throw new Error('Mode is required');

    const rawIDSData = {
      rawRequirement,
      rawType,
      rawStakeholder,
    };

    const [
      requirement, type, stakeholder,
    ] = Object.keys(rawIDSData).reduce((accumulator = [], key = '') => {
      const numberFormat = parseInt(rawIDSData[key]);
      if (isNaN(numberFormat)) {
        throw new Error(`Invalid ${key} format!`);
      }

      return accumulator = [...accumulator, numberFormat];
    }, []);

    const { application: { stakeholders } } = await ettClient.post('/application/confirm', { nonce });
    const foundStakeholderIntoEtt = stakeholders.find(({ id }) => id === stakeholder);
    if (!foundStakeholderIntoEtt) throw new Error('Invalid stakeholder');

    const foundRequirementsIntoEtt = foundStakeholderIntoEtt.requirements.find(({ id }) => id === requirement);
    if (!foundRequirementsIntoEtt) throw new Error('Invalid requirement');
    if (foundRequirementsIntoEtt.types.some(({ id }) => id !== type)) throw new Error('Invalid type');

    const rawNameFileArray = file.originalname.split('.');
    const fileTypePath = rawNameFileArray[rawNameFileArray.length - 1];

    const uploadPath = `${nonce}/${requirement}`;
    const fileName = `${type}.${fileTypePath}`;
    const filePathPromise = uploadFile({
      uploadPath,
      name: fileName,
      file,
    });

    const stakeholderDocumentDataPayload = {
      stakeholder,
      nonce,
    };
    let stakeholderDocumentData = await StakeholderDocumentData.findOne(stakeholderDocumentDataPayload);

    if (!stakeholderDocumentData) {
      stakeholderDocumentData = new StakeholderDocumentData({
        ...stakeholderDocumentDataPayload,
        stakeholderDocumentFiles: [],
      });
    }

    const stakeholderDocumentFilePayload = {
      stakeholderDocumentData,
      requirement,
    };
    let stakeholderDocumentFile = await StakeholderDocumentFile.findOne(stakeholderDocumentFilePayload);

    if (!stakeholderDocumentFile) {
      stakeholderDocumentFile = new StakeholderDocumentFile({
        ...stakeholderDocumentFilePayload,
        type,
        mode,
      });
    }

    await filePathPromise;
    stakeholderDocumentFile.fileUrl = `uploads/${uploadPath}/${fileName}`;

    stakeholderDocumentData.stakeholderDocumentFiles = [
      ...stakeholderDocumentData.stakeholderDocumentFiles
        .filter( // filter duplicate
          (filteredDocumentFilesIntoData) => filteredDocumentFilesIntoData.toString() !== (
            stakeholderDocumentFile?._id?.toString() || stakeholderDocumentFile.toString()
          ),
        ),
      stakeholderDocumentFile,
    ];
    const rawDocumentDBData = [stakeholderDocumentData, stakeholderDocumentFile];
    await Promise.all(rawDocumentDBData.map((documentDBData) => documentDBData.validate()));
    await Promise.all(rawDocumentDBData.map((documentDBData) => documentDBData.save()));

    return response.send();
  },

  verificationDocument: async (request, response) => {
    const { nonce = '' } = request.body;
    if (!nonce) throw new Error('Nonce is required');
    const { application: { stakeholders } } = await ettClient.post('/application/confirm', { nonce });
    const stakeholder = stakeholders[0];
    const { requirements } = stakeholder;
    if (requirements.length === 0) return response.send();

    const stakeholderDocumentSchema = await StakeholderDocumentData
      .findOne({ nonce, stakeholder: stakeholder.id })
      .populate('stakeholderDocumentFiles');

    if (!stakeholderDocumentSchema) throw new Error('Documents not found!');

    const { stakeholderDocumentFiles } = stakeholderDocumentSchema;
    const isFullUploadedRequirements = requirements
      .every(({ id: requirementId, types, name }) => stakeholderDocumentFiles
        // is found requirement
        .some(({ type, mode, requirement }) => requirement === requirementId && mode === name && types
          // is found type into requirement
          .some(({ id: typeId }) => typeId === type)));

    if (!isFullUploadedRequirements) throw new Error('Not all required documents have been downloaded!');

    const rawPayloadShiftyProDataDocuments = await Promise.all(
      stakeholderDocumentFiles.map(async ({ mode, fileUrl }) => {
        const fullPath = path.resolve(process.env.PWD, fileUrl);
        const fileBuffer = await fs.readFile(fullPath);
        const fileBase64 = fileBuffer.toString('base64');
        const pathArray = fileUrl.split('/');
        const fileName = pathArray[pathArray.length - 1];

        return [mode, { fileBase64, fileName }];
      }),
    );

    const payloadShiftyProDataDocuments = Object
      .fromEntries(rawPayloadShiftyProDataDocuments.map((array) => ([array[0], { proof: array[1].fileBase64 }])));

    const {
      verification_data: verificationData = {},
      verification_result: verificationResult = {},
      declined_reason: declinedReason = '',
    } = await shuftiPro.getDataInfoIntoShufiPro({
      reference: +new Date() + nonce,
      ...payloadShiftyProDataDocuments,
    });

    let isFailVerification = false;
    const valuesVerificationResult = Object.values(verificationResult);
    for (let i = 0; i < valuesVerificationResult.length; i++) {
      const result = valuesVerificationResult[i];
      if (typeof result === 'string' || typeof result === 'number') {
        isFailVerification = +result === 0;
      } else if (typeof result === 'object') {
        if (Array.isArray(result)) {
          isFailVerification = result.includes(0) || result.includes('0');
        } else {
          const valuesIntoResult = Object.values(result);
          isFailVerification = valuesIntoResult.includes(0) || valuesIntoResult.includes('0');
        }
      }

      if (isFailVerification) break;
    }

    if (!isFailVerification) {
      const payloadETTDataDocuments = Object
        .fromEntries(rawPayloadShiftyProDataDocuments);
      const payloadDataETT = requirements.reduce((accumulator, { id: requirement, types, name }) => {
        const payload = types.map(({ id: type }) => ({
          nonce,
          type,
          requirement,
          stakeholder: stakeholder.id,
          name: payloadETTDataDocuments[name].fileName,
          data: payloadETTDataDocuments[name].fileBase64,
        }));

        accumulator = [...accumulator, ...payload];

        return accumulator;
      }, []);

      await Promise.all(payloadDataETT.map((payload) => ettClient.post('/application/document', payload)));
    }

    return successResponse(response, {
      verificationData,
      verificationResult,
      declinedReason,
    });
  },

  deleteDocument: async (req, res) => {
    const { nonce, id } = req.params;
    await ettClient.post('/application/confirm', { nonce });

    const document = await EttCustomerDocument.findById(id);
    if (document.ettId) {
      await ettClient.delete(`/application/document/${nonce}/${document.ettId}`);
    }

    return successResponse(res, await document.delete());
  },

  verificationCustomer: async (req, res) => {
    const {
      copyPassport, selfiePassport, addressPassport, ...other
    } = req.body;

    return successResponse(res, await shuftiPro.sendVerification({
      ...other,
      files: {
        copyPassport,
        selfiePassport,
        addressPassport,
      },
    }));
  },

  checkEmailIsExist: async (req, res) => {
    try {
      const { email } = req.body;
      if (!email
        || !/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(email)) throw new Error('email is invalid');
      const id = '0';
      const { customers } = await ettClient.get('/customer.php', {
        params: {
          request: 'customer',
          key: getEnv('BASE_API_KEY'),
          email,
          id,
          hash: createHashSha1(id + email),
        },
      });
      return successResponse(res, { data: customers?.length > 0 });
    } catch (e) {
      return successResponse(res, { data: false });
    }
  },

  phoneChange: async (request, response) => {
    const { phone, password } = request.body;
    if (!phone || !regexp.phone.test(phone)) throw new Error('invalid phone number');

    const { customer } = request;
    try {
      await auth.login({ login: customer.email, password });
    } catch {
      throw new Error('invalid password!');
    }
    const code = getRandomStringNumber(0, 999999);

    const savePhonePromise = PhoneConfirm.findOneAndUpdate({ id: customer.id }, {
      id: customer.id,
      phone,
      code,
    }, { upsert: true });


    const sendMessagePromise = clickSend.sendSms(
      code,
      phone,
    );

    await Promise.all([
      savePhonePromise,
      sendMessagePromise,
    ]);

    return response.send();
  },

  phoneConfirm: async (request, response) => {
    const { code } = request.query;
    if (!regexp.digitalCode.test(code)) throw new Error('invalid digital code');
    const { customer } = request;
    const data = await PhoneConfirm.findOneAndRemove({
      id: customer.id,
      code,
    });

    if (!data) throw new Error('invalid digital code');

    try {
      await zenDeskService.changePhone(customer, data.phone);
    } catch (error) {
      throw new Error('Service is disconnected! Please try again later!');
    }

    return response.send();
  },

  emailChange: async (request, response) => {
    const { email, password } = request.body;
    if (!regexp.email.test(email)) throw new Error('Invalid email!');

    const { customer } = request;
    try {
      await auth.login({ login: customer.email, password });
    } catch {
      throw new Error('invalid password!');
    }

    const code = getRandomStringNumber(0, 999999);

    const saveEmailPromise = EmailConfirm.findOneAndUpdate({ id: customer.id }, {
      id: customer.id,
      email,
      code,
    }, { upsert: true });

    const sendEmailPromise = sendMail({
      to: email,
      subject: 'Confirm new email',
      templateName: 'confirm-email',
      params: {
        code,
      },
    });

    await Promise.all([saveEmailPromise, sendEmailPromise]);

    return response.send();
  },

  emailConfirm: async (request, response) => {
    const { code } = request.query;
    if (!regexp.digitalCode.test(code)) throw new Error('invalid digital code');
    const { customer } = request;
    const data = await EmailConfirm.findOneAndRemove({
      id: customer.id,
      code,
    });

    if (!data) throw new Error('invalid digital code');

    try {
      await zenDeskService.changeEmail(customer, data.email);
    } catch (error) {
      throw new Error('Service is disconnected! Please try again later!');
    }

    return response.send();
  },
});
