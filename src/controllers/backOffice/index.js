import BackOfficeService from '../../services/backOffice';
import BackOfficeClient from '../../client/backOffice.client';


const backOfficeService = new BackOfficeService(new BackOfficeClient());

export default {
  fees: async (request, response) => {
    const data = await backOfficeService.fees();
    return response.json(data);
  },

  fields: async (request, response) => {
    const { countryCode } = request.query;
    const data = await backOfficeService.fields(countryCode);
    return response.json(data);
  },

  purposes: async (request, response) => {
    const data = await backOfficeService.purposes();
    return response.json(data);
  },

  verificationBeneficiary: async (beneficiary, customerID) => {
    await backOfficeService.verificationBeneficiary(beneficiary, customerID);
  },
};
