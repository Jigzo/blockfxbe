/* eslint-disable import/no-unresolved */
/* eslint-disable camelcase */
import fs from 'fs/promises';
import process from 'process';
import AES from 'crypto-js/aes';
import ENC from 'crypto-js/enc-utf8';
import { ettClientAuthToken } from '../../client/ett.client';
import {
  // errorResponse,
  successResponse,
  bankValidator,
} from '../../helpers/response';
// import apply from '../../core/apply';
import { Beneficiary } from '../../models';
// import { APIError } from '../../helpers/errors';
import backOffice from '../backOffice/index';
import ShuftiPro from '../../core/shuftiPro';
import { getEnv } from '../../helpers';
import clickSend from '../../core/clickSend';
import sendMail from '../../helpers/sendMail';
import regexp from '../../helpers/regexp';


const shuftiPro = new ShuftiPro();
const prefix = '/beneficiary';
export default {
  index: async (request, response) => {
    const foundBeneficiaries = await Beneficiary.find({});
    return successResponse(response, {
      data: foundBeneficiaries,
      total: foundBeneficiaries.length,
    });
    // const { start, limit = 20 } = request.query;
    // try {
    //   const { beneficiary, ...responseData } = await ettClientAuthToken(request).get(prefix, {
    //     params: {
    //       limit, start,
    //     },
    //   });
    //   const foundBeneficiaries = await Beneficiary.find({ ettId: beneficiary.map(({ id }) => id) });
    //   const mergeBeneficiary = beneficiary.map((acc) => {
    //     acc.name = foundBeneficiaries.find(({ ettId }) => ettId === acc.id)?.name || acc.name;
    //     return acc;
    //   });

    //   return successResponse(response, { data: mergeBeneficiary, ...responseData });
    // } catch (e) {
    //   if (e.message === 'No Records') return successResponse(response, { data: [] });
    //   return errorResponse(response, new APIError(e?.message, 400));
    // }
  },

  show: async (req, res) => {
    const resEtt = await ettClientAuthToken(req).get(`${prefix}/${req.params.id}`);

    return successResponse(res, resEtt);
  },

  create: async (req, res) => {
    const isInvalidBank = bankValidator(req, res);
    const {
      name,
      currencyCode = '',
      country = '',
      type,

      isLocal = false,

      phone = '',
      email = '',

      aba = '',
      bic = '',
      sortCode = '',
      accountNumber = '',

      accountType,

      bankName,
      bankAddress,
    } = req.body;

    let {
      iban = '',
    } = req.body;

    if (isInvalidBank) return false;
    const isLikelyLocal = isLocal && country === currencyCode.substr(0, 2);
    const isLikelySortCode = isLikelyLocal && ['GB'].includes(country);
    const isLikelyABA = isLikelyLocal && ['US'].includes(country);

    const currentSortCode = regexp.sortCode.test(sortCode);
    const currentAba = regexp.aba.test(aba);

    if (accountNumber && regexp.iban.test(accountNumber) && !iban) {
      iban = accountNumber;
    }

    // const dataApply = await apply.verification(country, {
    //   accountNumber: iban || accountNumber,
    //   sortCode,
    //   bic,
    // });

    // if (dataApply.status !== 'PASS') {
    //   throw new Error('bank data is invalid');
    // }

    const reqData = {
      country,
      bankcountry: country,
      currencycode: currencyCode,

      name,
      bic: bic || (isLikelySortCode && currentSortCode && sortCode) || (isLikelyABA && currentAba && aba) || '',
    };

    if (iban) reqData.iban = iban;
    if (accountNumber) reqData.accountnumber = accountNumber;

    if (type) reqData.type = type;

    if (phone) reqData.phone = phone;
    if (email) reqData.email = email;

    if (accountType) reqData.accounttype = accountType;

    if (bankName) reqData.bankname = bankName;
    if (bankAddress) reqData.bankaddress = bankAddress;


    const { id } = await ettClientAuthToken(req).post(prefix, reqData);
    const beneficiary = await Beneficiary.create({
      ettId: id,
      ...req.body,
    });

    return successResponse(res, { data: beneficiary });
  },

  update: async (request, response) => {
    const { id } = request.params;
    const { name } = request.body;
    const { beneficiaries } = await ettClientAuthToken(request).get(`${prefix}/${id}`);
    const beneficiary = await Beneficiary.findOneAndUpdate(
      { ettId: beneficiaries[0].id },
      { ...beneficiaries[0], name },
    );

    beneficiary.id = beneficiaries[0].id;

    return response.json({
      beneficiary,
    });
  },

  delete: async (request, response) => {
    const { id } = request.params;
    const result = await ettClientAuthToken(request).delete(`${prefix}/${id}`);

    return successResponse(response, {
      ...result,
      id: +id,
    });
  },

  sendVerificationLink: async (request, response) => {
    const { id } = request.params;
    const {
      email = '',
      phone = '',
    } = request.body;

    if (!email && !phone) {
      return response.status(400).json({
        message: 'Phone or email is required',
      });
    }

    const { customer } = request;
    await Beneficiary.updateOne({ ettId: id }, { verifyID: customer.id });

    const payloadKey = JSON.stringify({
      customerID: customer.id,
      beneficiaryID: id,
    });

    const crypt = AES.encrypt(payloadKey, getEnv('JWT_AUTHORIZATION_SECRET')).toString();

    const baseFEURL = new URL(getEnv('FE_APP_URL'));
    const verificationUrl = `${baseFEURL.href}recipients/${id}/verification/?key=${crypt}`;

    if (phone) {
      if (!regexp.phone.test(phone)) {
        return response.status(400).json({
          message: 'Phone is invalid',
        });
      }

      let body = 'Please open on the link below to verify your identity on ShuftiPro:\n\n';
      body += 'If you have not submitted a request for "blocFX" please ignore this message.\n\n\n';
      body += verificationUrl;
      body += '\n\n\nThank you for your continued loyalty.';
      // eslint-disable-next-line no-unused-vars
      body += '\n\nBlocFX.';
      clickSend.sendSms(
        body,
        phone,
      );
    }

    if (email) {
      if (!regexp.email.test(email)) {
        return response.status(400).json({
          message: 'Email is invalid',
        });
      }

      sendMail({
        to: email,
        subject: 'Verification account',
        templateName: 'shuftipro-beneficiary',
        params: {
          appUrl: '',
          verificationUrl,
        },
      });
    }

    const beneficiary = await Beneficiary.findOne({ ettId: id });

    return response.json({ beneficiary, customer, verificationUrl });
  },

  verificationDocument: async (request, response) => {
    const { files } = request;
    const { id } = request.params;
    const file = files[0];

    if (!file) {
      return response.status(400).json({
        message: 'File is required',
      });
    }

    if (file.size < 8192) {
      return response.status(403).json({
        message: 'Bad quality file!',
      });
    }

    if (file.size >= 16777216) {
      return response.status(413).json({
        message: 'The maximum file size must not exceed 16 megabytes!',
      });
    }

    if (!['image/png', 'image/jpg', 'image/jpeg'].includes(file.mimetype)) {
      return response.status(406).json({
        message: 'File format must be is jpg or png!',
      });
    }

    const documentTypes = (request.body.supportedTypes || 'id_card,passport').split(',');
    const {
      fullName = '',
      dob = '',
      issueDate = '',
      expiryDate = '',
      documentNumber = '',
      country = '',
      email = '',
    } = request.body;

    const dirname = process.mainModule.path.replace('/src', '').replace('/build', '');
    const uploadsDir = `${dirname}/uploads`;
    const filePath = `${uploadsDir}/${file.originalname}`;
    try {
      await fs.access(uploadsDir);
    } catch {
      await fs.mkdir(uploadsDir);
    }
    await fs.writeFile(filePath, file.buffer, 'binary');

    const baseFEURL = new URL(getEnv('FE_APP_URL'));

    const payload = {
      reference: `beneficiary/${Date.now()}`,
      callback_url: baseFEURL.href,
      email,
      country,
      language: 'EN',
      redirect_url: baseFEURL.href,
      verification_mode: 'any',
      document: {
        supported_types: documentTypes,
        name: {
          full_name: fullName,
        },
        dob,
        issue_date: issueDate,
        expiry_date: expiryDate,
        document_number: documentNumber,
        proof: file.buffer.toString('base64'),
      },
    };

    const data = await shuftiPro.getDataInfoIntoShufiPro(payload);
    const verificationResult = data.verification_result;
    const verificationData = data.verification_data.document;

    if (verificationResult) {
      const statusVerificationSum = Object.values(verificationResult.document)
        // eslint-disable-next-line no-return-assign
        .reduce((accumulator, currentValue) => {
          const check = typeof currentValue === 'number';
          if (check) {
            // eslint-disable-next-line no-param-reassign
            accumulator += currentValue;
          }

          return accumulator;
        }, 0);

      const toCapitalize = (string = '') => {
        const [firstLetter = '', ...nextLetters] = string.split('');

        return firstLetter.toUpperCase() + [...nextLetters].join('').toLowerCase();
      };

      const firstname = toCapitalize(verificationData.name.first_name || verificationData.name.full_name.split(' ')[0]);
      const name = toCapitalize(verificationData.name.last_name || verificationData.name.full_name.split(' ')[1]);

      const payloadData = {
        isCorrectVerify: statusVerificationSum >= 9,
        firstname,
        name,
        dob: new Date(verificationData.dob),
        expiryDate: new Date(verificationData.expiry_date),
        issueDate: new Date(verificationData.issue_date),
        documentNumber: verificationData.document_number,
      };

      await Beneficiary.updateOne({ ettId: id }, payloadData);
    }

    return response.status(200).json({ data });
  },

  verificationBeneficiary: async (request, response) => {
    const { id } = request.params;
    const { key } = request.body;

    if (!key) {
      return response.status(403).json({
        message: 'invalid token',
      });
    }

    // decrypt key
    const binary = AES.decrypt(key, getEnv('JWT_AUTHORIZATION_SECRET'));
    const { customerID, beneficiaryID } = JSON.parse(binary.toString(ENC));

    if (beneficiaryID !== id) {
      return response.status(403).json({
        message: 'invalid beneficiary',
      });
    }

    if (!customerID) {
      return response.status(403).json({
        message: 'invalid customer',
      });
    }

    // check verify
    const beneficiary = await Beneficiary.findOne({ ettId: id, verifyID: customerID });
    if (!beneficiary?.isCorrectVerify) throw new Error('Verification failed');

    // verify
    const result = await backOffice.verificationBeneficiary(beneficiary, customerID);

    if (result.status === 'ok' && id === beneficiaryID) {
      return response.json({ beneficiary });
    }

    return response.status(503).json({
      message: 'Ett is disconnect',
    });
  },
};
