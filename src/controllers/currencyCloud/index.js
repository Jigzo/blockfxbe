import { successResponse } from '../../helpers/response';
import currencyCloud from '../../core/currencyCloud';


export default {
  findBankDetails: async (req, res) => {
    const { identifierType, identifierValue } = req.query;

    const data = await currencyCloud.findBankDetails(identifierType, identifierValue);

    return successResponse(res, { data });
  },
};
