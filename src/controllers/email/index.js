import { successResponse } from '../../helpers/response';
import { sendMail } from '../../helpers';


export default {
  send: async (req, res) => {
    const { templateName, params, subject } = req.body;
    await sendMail({
      to: req.customer.email,
      subject,
      templateName,
      params,
    });
    return successResponse(res, true);
  },
};
