import { successResponse } from '../../helpers/response';
import socket from '../../ws';
import { Notification } from '../../models';
import HookService from '../../services/hook';


const hookService = new HookService();
export default ({
  external: async (req, res) => {
    if (req?.body?.customer) {
      const uId = req.body.customer;
      const { transactionId, status } = req.body;
      const response = {
        title: 'Transaction status',
        date: Date.now(),
        body: `Transaction #${transactionId}: is ${status.toLowerCase()}`,
        uId,
      };
      Notification.create(response);
      socket().emit(uId)('notification')(response);
    }

    return successResponse(res, { success: true });
  },

  internal: async (req, res) => {
    console.log(req.query, req.params, req.body, 'internal');
    return successResponse(res, { success: true });
  },

  deposit: async (req, res) => {
    console.log(req.query, req.params, req.body, 'deposit');
    return successResponse(res, { success: true });
  },

  zendeskMessage: async (request, response) => {
    const { zendesk } = request.body;
    hookService.zendeskMessage(zendesk);

    return successResponse(response, {});
  },
});
