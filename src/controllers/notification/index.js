import { successResponse } from '../../helpers/response';
import { Notification } from '../../models';
import socket from '../../ws';


export default ({
  getAll: async (request, response) => {
    const { customer } = request;
    const uId = customer.id;
    const notifications = await Notification.find({
      uId,
    });

    return successResponse(response, notifications);
  },
  save: async (request, response) => {
    socket().emit('external')(request.body);
    Notification.create({
      title: 'test Title',
      date: Date.now(),
      body: 'test text notification',
      uId: 32,
    });
    return successResponse(response, { success: true });
  },
  delete: async (request, response) => {
    const { customer } = request;
    const uId = customer.id;
    await Notification.remove({
      uId,
    });
    return successResponse(response, []);
  },
});
