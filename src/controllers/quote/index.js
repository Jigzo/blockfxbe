class QuoteController {
  constructor(service) {
    this.service = service;
  }

  create = async (request, response) => {
    const quote = await this.service.create(request);
    return response.status(201).json(quote);
  }

  findById = async (request, response) => {
    const { quoteId } = request.params;
    const quote = await this.service.findById(quoteId);
    return response.status(200).json(quote);
  }
}

export default QuoteController;
