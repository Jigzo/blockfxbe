import { sendMail, successResponse } from '../../helpers';
import { EttCustomerDocument } from '../../models';
import shuftiPro from '../../core/shuftiPro';
import { ettClient } from '../../client/ett.client';


export default {
  verificationCustomer: async (req, res) => {
    const {
      reference, event, email,
    } = req.body;

    const eventStatus = event?.split('.')?.[1] || event || 'invalid';
    const documents = await EttCustomerDocument.find({
      reference,
    });
    const { nonce } = documents?.find?.((doc) => !!(doc.nonce)) || '';

    if (documents.length) {
      await Promise.all(documents.map(async (doc) => {
        // eslint-disable-next-line no-param-reassign
        doc.status = eventStatus;
        await doc.save();
      }));

      const ettAppId = documents?.[0]?.ettAppId;
      const documentsSubmit = await EttCustomerDocument.find({
        ettAppId,
        status: {
          $nin: ['rejected', 'declined'],
        },
      });

      if (eventStatus === 'accepted' && documentsSubmit.length === 3) {
        await ettClient.post('application/submit', { nonce });
      }
    }
    if (eventStatus === 'declined') {
      const { params, ...other } = shuftiPro.createPayloadEmailError({ email });
      await sendMail({
        ...other,
        params: {
          ...params,
          nonce,
          errorMessage: req.body.declined_reason || 'Verification is invalid!',
        },
      });
    }

    return successResponse(res);
  },
};

