/* eslint-disable max-len */
import { ettClientAuthToken } from '../../client/ett.client';
import { successResponse } from '../../helpers/response';

import sendMail from '../../helpers/sendMail';
import { Quote, Transfer } from '../../models';
import TransferService from '../../services/transfer';


const getLocalDate = () => (Date.now() / 1000).toFixed(0);
const transferService = new TransferService(Transfer);
const transferController = {
  createTransfer: async (request, res) => {
    const {
      amount,
      narrative = 'blockFx transfer',
      forex,
    } = request.body;
    const { account = {}, beneficiary = {} } = request.meta;

    if (amount && +amount < 0) throw new Error('amount must be positive!');
    const foundQuote = await Quote.findOne({ id: forex });

    if (amount && foundQuote.sellamount !== amount) throw new Error('Invalid amount transfer!');

    const counterpart = {
      id: beneficiary.id,
      name: beneficiary.name,
      currencyCode: beneficiary.currencyCode,
      country: beneficiary.country,
      displayAccountNo: beneficiary.displayAccountNo || beneficiary.iban || beneficiary.accountNumber,
    };

    if (beneficiary.iban) counterpart.iban = beneficiary.iban;
    if (beneficiary.accountNumber) counterpart.accountNumber = beneficiary.accountNumber;
    if (beneficiary.bic) counterpart.bic = beneficiary.bic;

    const payloadTransferData = {
      accountNumber: account.accountNumber,
      requestcurrency: account.currency,
      requestamount: foundQuote.sellamount,
      counterpart,
      narrative,
      status: 'cache',
      quote: foundQuote,
    };

    if (account.available >= payloadTransferData.requestamount) {
      const ettRes = await transferService
        .createTransferEtt(request, payloadTransferData, account.accountNumber, forex);
      payloadTransferData.id = ettRes.id;
      payloadTransferData.status = ettRes.status;
    }

    await transferService.createTransfer(payloadTransferData);

    if (beneficiary.email) {
      await sendMail({
        to: beneficiary.email,
        subject: 'Transaction open',
        templateName: 'transaction-details-open',
        params: {
          amount: payloadTransferData.requestamount,
          currency: payloadTransferData.requestcurrency,
          from: account.name,
          status: payloadTransferData.status,
        },
      });
    }

    return successResponse(res, payloadTransferData);
  },

  historyTransfer: async (request, response) => {
    const transfers = await Transfer.find();


    return response.json({
      data: transfers,
      total: transfers.length,
    });
    // let {
    //   accountNumber = '' || [],
    // } = request.query;

    // const { accounts } = await ettClientAuthToken(request).get('account');
    // if (typeof accountNumber === 'string') accountNumber = [accountNumber];

    // if (!accountNumber?.length) {
    //   accountNumber = accounts.map((account) => account.accountNumber);
    // } else {
    //   accountNumber = accountNumber.filter((account) => accounts
    //     .some((accountNumberIntoQuery) => account.accountNumber === accountNumberIntoQuery));
    // }

    // if (accountNumber.length === 0) return response.json({ data: [], total: 0 });

    // let foundTransfers = await Transfer.find({ accountNumber }).populate('quote').lean();

    // if (foundTransfers.length) {
    //   foundTransfers = foundTransfers.map(async (foundTransfer) => {
    //     if (foundTransfer.status === 'cache') {
    //       const foundAccount = accounts
    //         .find((filteredAccount) => filteredAccount.accountNumber === foundTransfer.accountNumber);

    //       const foundTransferInspectedAmount = await transferService
    //         .inspectAmountOrCreateTransferEtt(request, foundTransfer, foundAccount);

    //       return foundTransferInspectedAmount;
    //     }

    //     return foundTransfer;
    //   });

    //   foundTransfers = await Promise.all(foundTransfers);

    //   response.json({
    //     data: foundTransfers,
    //     total: foundTransfers.length,
    //   });
    // }

    // const params = {
    //   limit: 10000,
    //   order: 'desc',
    //   fromtime: request.query.fromtime || getLocalDate(),
    //   totime: getLocalDate(),
    // };

    // const SendResponseToETT = (accountId, path = '') => ettClientAuthToken(request, false)
    //   .get(`/account/${accountId}/transfer${path}`, { params });

    // foundTransfers.forEach((transfer) => {
    //   const transferTime = (Date.parse(transfer.updatedAt) / 1000).toFixed();
    //   if (params.fromtime > transferTime) params.fromtime = transferTime;
    // });

    // const payloadAccounts = accountNumber.reduce((accumulator, accountId) => {
    //   if (!accumulator[accountId]) {
    //     accumulator[accountId] = [
    //       SendResponseToETT(accountId),
    //       SendResponseToETT(accountId, '/pending'),
    //     ];
    //   }

    //   return accumulator;
    // }, {});

    // let transfersIntoEtt = [];
    // const payloadPromises = Object.keys(payloadAccounts).map(async (key) => {
    //   const [dataIntoEtt, dataPendingIntoEtt] = await Promise.all(payloadAccounts[key]);
    //   const accountTransfersIntoEtt = [...dataIntoEtt.transactions, ...dataPendingIntoEtt.transactions];
    //   transfersIntoEtt = [...transfersIntoEtt, ...accountTransfersIntoEtt];

    //   const payloadPromisesPart = accountTransfersIntoEtt.map(async (transferIntoEtt) => {
    //     const foundTransfer = foundTransfers.find(({ id }) => id === transferIntoEtt.id);

    //     if (foundTransfer) {
    //       if (
    //         foundTransfer.status !== transferIntoEtt.status
    //         || foundTransfer.queue !== transferIntoEtt.queue
    //         || ((!foundTransfer.rule && transferIntoEtt.rule) || (foundTransfer.rule && !transferIntoEtt.rule))
    //       ) {
    //         return await Transfer.findOneAndUpdate({ id: foundTransfer.id }, { ...foundTransfer, ...transferIntoEtt });
    //       }
    //     } else {
    //       return await transferService.createTransfer({ ...transferIntoEtt, accountNumber: key });
    //     }

    //     return null;
    //   });

    //   return await Promise.all(payloadPromisesPart);
    // });

    // await Promise.all(payloadPromises);

    // if (!foundTransfers.length) {
    //   return response.json({
    //     data: transfersIntoEtt,
    //     total: transfersIntoEtt.length,
    //   });
    // }

    // return null;
  },

  showTransfer: async (request, response) => {
    const { account, transfer } = request.meta;
    const inspectedTransfer = await transferService.inspectAmountOrCreateTransferEtt(request, transfer, account);
    return successResponse(response, {
      data: inspectedTransfer,
    });
  },

  setReason: async (request, response) => {
    const { transferId, accountId } = request.params;
    const { reason } = request.body;
    if (!reason) throw new Error('Reason is required!');

    const data = await ettClientAuthToken(request)
      .post(`/account/${accountId}/transfer/${transferId}/confirm`, {
        reason,
        time: getLocalDate(),
      });

    return successResponse(response, {
      data: data.transactions,
    });
  },

  attachDocument: async (request, response) => {
    const { transferId, accountId } = request.params;
    const { file } = request;
    if (!file) throw new Error('file is required!');

    const { requirement, type } = request.body;

    const base64 = file.buffer.toString('base64');

    const data = await ettClientAuthToken(request)
      .post(`/account/${accountId}/transfer/${transferId}/document`, {
        requirement,
        type,
        time: getLocalDate(),
        name: file.originalname,
        data: base64,
      });

    return successResponse(response, {
      data,
    });
  },

  submitDocument: async (request, response) => {
    const { transferId, accountId } = request.params;
    const data = await ettClientAuthToken(request)
      .post(`/account/${accountId}/transfer/${transferId}/submit`, {
        time: getLocalDate(),
      });

    return successResponse(response, {
      data,
    });
  },
};

export default transferController;
