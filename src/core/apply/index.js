import qs from 'qs';
import { applyClient } from '../../client/apply.client';
import { getEnv } from '../../helpers/config';
import regexp from '../../helpers/regexp';


class Apply {
  constructor() {
    this.token = null;
    this.username = getEnv('APPLY_USERNAME');
    this.password = getEnv('APPLY_PASSWORD');
    this.cacheBank = {};
  }

  _formatBankData(data) {
    const office = data.branchDetails || [data.paymentBicDetails] || [data.headOfficeDetails];

    const formatData = {
      branchDetails: office
        // eslint-disable-next-line no-shadow
        ?.map(({ bankName, street, codeDetails }) => ({ bankName, street, codeDetails })),
    };

    if (data.accountNumber) formatData.accountNumber = data.accountNumber;
    if (data.recommendedNatId) formatData.recommendedNatId = data.recommendedNatId;
    if (data.recommendedBIC) formatData.recommendedBIC = data.recommendedBIC;
    if (data.bic8) formatData.bic8 = data.bic8;

    return formatData;
  }

  async getApplyToken() {
    if (this.token) return Promise.resolve(this.token);
    this.token = await this._getToken();
    setTimeout(() => { this.token = null; }, 60000);
    return Promise.resolve(this.token);
  }

  async _getToken() {
    const { data } = await applyClient.post('authenticate', qs.stringify({
      username: this.username, password: this.password,
    }), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });

    return data.token;
  }

  async verification(countryCode, { accountNumber = '', sortCode = '', aba = '' }) {
    if (!regexp.countryCode.test(countryCode)) throw new Error('invalid country code');
    if (!regexp.accountNumber.test(accountNumber)) throw new Error('invalid account');

    const token = await this.getApplyToken();
    const params = {
      token,
      countryCode,
      accountNumber,
    };

    if (sortCode) params.nationalId = sortCode;
    else if (aba) params.nationalId = aba;

    const { data } = await applyClient.get('convert/1.0.1', { params });

    return data;
  }

  async code(countryCode = '', nationalId = '', accountNumber = '') {
    if (!regexp.countryCode.test(countryCode)) throw new Error('invalid country code');
    if (!nationalId && !accountNumber) throw new Error('invalid params');
    if (!nationalId && !regexp.accountNumber.test(accountNumber)) throw new Error('invalid account number');
    if (!(regexp.sortCode.test(nationalId) || regexp.aba.test(nationalId))
      && !accountNumber) {
      throw new Error('invalid national code');
    }

    const token = await this.getApplyToken();
    const params = {
      token,
      countryCode,
    };

    if ([6, 9].includes(nationalId.length)) params.nationalId = nationalId;
    if (accountNumber.length > 6 && accountNumber.length < 35) params.accountNumber = accountNumber;
    const { data } = await applyClient.get('convert/1.0.1', {
      params,
    });

    return this._formatBankData(data);
  }

  async findBankDetails(countryCode, bankName, address, bic8, pageSize = 10) {
    if (!regexp.countryCode.test(countryCode)) throw new Error('invalid country code');
    let isPreventLoad = false;
    if (bankName === 'B' && !bic8) {
      if (this.cacheBank[countryCode]) {
        return this.cacheBank[countryCode];
      }

      isPreventLoad = true;
    }
    const token = await this.getApplyToken();
    const { data } = await applyClient.get('convert/1.0.2', {
      params: {
        token,
        pageSize,
        countryCode,
        bankName,
        address,
        bic8,
        page: 1,
      },
    });

    const formatData = this._formatBankData(data);
    if (isPreventLoad) this.cacheBank[countryCode] = formatData;

    return formatData;
  }
}

const apply = new Apply();
export default apply;
