import dayjs from 'dayjs';
import jwt from 'jsonwebtoken';
import { EttUserToken } from '../../models';
// eslint-disable-next-line import/no-cycle
// import { ettClient, ettClientAuthToken } from '../../client/ett.client';
import { getEnv } from '../../helpers';


class Auth {
  async login({ login, password }) {
    // const { token: ettToken } = await ettClient.post('/session', { login, password, time: dayjs().unix() });
    // const { customer } = await ettClientAuthToken({ ettToken }).get('/customer');
    if (password !== 'Qwer123!') throw new Error('Invalid password!');

    // ! FAKE
    const customer = {
      id: 0,
      reference: 'Fake',
      email: 'Fake@email.fake',
      phone: '+000000000',
      firstName: 'Fake',
      lastName: 'Fake',
      name: 'Fake',
      signatory: 'Fake',
    };

    const ettToken = password;

    const ettUserToken = await EttUserToken.create({
      ettAppId: customer.reference,
      ettToken,
      confirm: true,
      type: 'auth',
    });

    return this.encodeToken({
      customer: {
        ...customer,
        email: login,
      },
      ettUserTokenId: ettUserToken._id,
    });
  }

  async getEttToken(ettUserTokenId) {
    const { ettToken } = await EttUserToken.findById(ettUserTokenId);

    return ettToken;
  }

  async refreshEttToken(ettToken, ettUserTokenId, customer) {
    await EttUserToken.findOneAndUpdate({
      _id: ettUserTokenId,
    }, { ettToken });

    return this.encodeToken({ customer, ettUserTokenId });
  }

  async logout(ettUserTokenId) {
    await this._deleteTokensCode([ettUserTokenId]);
  }

  async _deleteTokensCode(ids = []) {
    await EttUserToken.deleteMany({
      $or: [
        {
          $and: [
            { createdAt: { $lt: dayjs().subtract(10, 'm').toISOString() } },
            { confirm: false },
            { type: 'auth' },
          ],
        },
        { _id: { $in: ids } },
        { updatedAt: { $lt: dayjs().subtract(65, 'm').toISOString() } },
      ],
    });
  }

  encodeToken(data, secret = getEnv('JWT_AUTHORIZATION_SECRET')) {
    return jwt.sign({
      exp: dayjs().add(65, 'm').unix(),
      ...data,
    }, secret);
  }

  decodeToken(jwtToken, secret = getEnv('JWT_AUTHORIZATION_SECRET')) {
    return jwt.verify(jwtToken, secret);
  }
}

const auth = new Auth();

export default auth;
