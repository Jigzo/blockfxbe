import { clickSendClient } from '../../client/clickSend.client';


class ClickSend {
  async sendSms(body, to, from = 'BlocFX') {
    try {
      const data = await clickSendClient.post('/sms/send',
        {
          messages: [{
            body,
            to,
            from,
          }],
        });
      return data;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
}

const clickSend = new ClickSend();

export default clickSend;
