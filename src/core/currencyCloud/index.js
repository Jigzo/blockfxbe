import { getEnv } from '../../helpers';
import { currencyCloudClient } from '../../client/currencyCloud.client';


class CurrencyCloud {
  async auth() {
    const { data } = await currencyCloudClient.post('/authenticate/api', {
      login_id: getEnv('CURRENCY_CLOUD_LOGIN'),
      api_key: getEnv('CURRENCY_CLOUD_API_KEY'),
    });

    return data.auth_token;
  }

  async findBankDetails(identifierType = '', identifierValue = '') {
    let Type = identifierType.trim();
    let Value = identifierValue.trim();

    if (!Type) throw new Error('identifierType is required');
    if (!Value) throw new Error('identifierValue is required');

    Type = Type.toLowerCase();
    Value = Value.toUpperCase();

    const validTypes = {
      iban: (item) => item.match(/^[A-Z]{2}[A-Z|0-9]{6}[0-9]{9,28}$/),
      bic_swift: (item) => item.match(/^[A-Z|0-9]{4}[A-Z]{2}[A-Z|0-9]{5}$|^[A-Z|0-9]{4}[A-Z]{2}[A-Z|0-9]{2}$/),
      sort_code: (item) => /^[0-9]{6}$/.test(item),
      aba: (item) => /^[0-9]{9}$/.test(item),
      routing_number: (item) => /^[0-9]{9}$/.test(item),
      bsb_code: (item) => item,
      cnaps: (item) => item,
    };

    if (!Object.keys(validTypes).includes(Type)) {
      throw new Error('identifierType is invalid');
    }

    if (!validTypes[Type](Value)) {
      throw new Error(`value ${identifierValue} for ${Type} is invalid`);
    }

    try {
      const token = await this.auth();
      const { data } = await currencyCloudClient
        .get(`/reference/bank_details?identifier_type=${Type}&identifier_value=${identifierValue}`, {
          headers: {
            'X-Auth-Token': token,
          },
        });

      const formatData = {
        branchDetails: [{
          bankName: data.bank_name,
          street: data.bank_address,
          state: data.bank_state,
          codeDetails: {
            codeValue1: data.bic_swift,
            codeValue2: '',
            codeValue3: data.bic_swift?.substr(0, 4),
            codeValue4: '',
          },
        }],
      };

      return formatData;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
}

const currencyCloud = new CurrencyCloud();

export default currencyCloud;
