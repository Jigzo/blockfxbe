import _ from 'lodash';
import {
  getEnv, APIError, sendMail, saveFileServer,
} from '../../helpers';
import { shuftiproClient } from '../../client/shuftipro.client';
import { EttCustomerDocument } from '../../models';
import { convertToBase64 } from '../../helpers/utils';


export default class ShuftiPro {
  constructor() {
    this.callbackUrlPrefix = `${getEnv('APP_URL_API')}/shuftipro-callback`;
    this.feAppUrl = `${getEnv('FE_APP_URL')}`;
    this.appName = `${getEnv('APP_NAME')}`;
  }

  _createPayload(action, reference, data) {
    const actions = ({
      verificationCustomer({
        firstName = '', lastName = '', email = '', documents = {},
      }) {
        const payload = {
          callback_url: `${this.callbackUrlPrefix}/verification-customer`,
          verification_mode: 'image_only',
          email,
          allow_offline: '1',
          show_privacy_policy: '1',
          show_results: '1',
          show_consent: '0',
          decline_on_single_step: '0',
          reference,
        };

        if (documents.document) {
          payload.document = {
            proof: convertToBase64(documents.document.path),
            supported_types: ['id_card', 'passport'],
            name: {
              first_name: firstName,
              last_name: lastName,
            },
            allow_offline: '1',
            fetch_enhanced_data: '1',
            backside_proof_required: '0',
          };
        }
        if (documents.face) {
          payload.face = {
            proof: convertToBase64(documents.face.path),
            allow_offline: '1',
          };
        }
        if (documents.address) {
          payload.address = {
            proof: convertToBase64(documents.address.path),
            supported_types: ['id_card', 'passport', 'driving_license'],
            address_fuzzy_match: '1',
            allow_offline: '1',
          };
        }

        return payload;
      },
    })[action];

    if (!actions) {
      throw new APIError('Action not found');
    }

    return actions.call(this, data);
  }

  createPayloadEmailError(data, action = 'verificationCustomer') {
    return ({
      verificationCustomer: ({
        templateName: 'shuftipro-verification-error',
        to: data.email,
        params: {
          name: `${data.firstName} ${data.lastName}`,
          nonce: data.nonce,
          appUrl: this.feAppUrl,
          appName: this.appName,
          // urlAddData: `${this.feAppUrl}?form-data=${encodeURIComponent(JSON.stringify(data))}`,
        },
      }),
    })[action];
  }

  async getDataInfoIntoShufiPro(shuftiProPayload) {
    const { data } = await shuftiproClient.post('/', shuftiProPayload);
    return data;
  }

  async sendShufiPro(shuftiProPayload) {
    const Messages = [];
    const data = await this.getDataInfoIntoShufiPro(shuftiProPayload);
    if (!data.verification_result) throw new Error('No verification result');
    if (data.verification_result.address) {
      Object.keys(data.verification_result.address).forEach((key) => {
        if (data.verification_result.address[key] === 0) {
          Messages.push(key);
        }
      });
    }

    if (data.verification_result.document) {
      Object.keys(data.verification_result.document).forEach((key) => {
        if (data.verification_result.document[key] === 0) {
          Messages.push(key);
        }
      });
    }

    return Messages;
  }

  async _saveFiles({
    ettAppId, documents, stakeholder, nonce,
  }, reference) {
    const res = {};

    await Promise.all(Object.keys(documents).map(async (key) => {
      if (typeof documents[key] === 'object') {
        const { file, requirement, type } = documents[key];
        const filePath = `/documents/${reference}__${process.hrtime.bigint()}___${file.name}`;
        const serverPath = await saveFileServer(file.path, filePath);
        _.set(res, `${key}.path`, serverPath);
        _.set(res, `${key}.name`, file.name || '');

        const ettCustomerDocument = await EttCustomerDocument.create({
          ettAppId,
          name: file.name,
          stakeholder: +stakeholder,
          ettRequirementData: {
            id: +requirement.id,
            name: requirement.name,
            description: requirement.description,
          },
          ettTypeData: {
            id: +type.id,
            name: type.name,
            description: type.description,
          },
          reference,
          path: serverPath,
          nonce,
        });

        _.set(res, `${key}.docId`, ettCustomerDocument._id);
      }
      return key;
    }));

    return res;
  }

  async sendVerification(data = {}, action = 'verificationCustomer') {
    const reference = `SP_REQUEST_${Math.random()}`;
    const saveFileData = {
      ...data,
      documents: await this._saveFiles(data, reference),
    };

    const payload = this._createPayload(action, reference, saveFileData);
    const shuftiMessages = await this.sendShufiPro(payload);

    if (shuftiMessages.length) {
      const errorEmailPayload = this.createPayloadEmailError(saveFileData, action);
      const errorMessages = shuftiMessages.map((msg) => `${msg.replace(/_/g, ' ')} is invalid\n`);
      await sendMail({
        ...errorEmailPayload,
        params: {
          ...errorEmailPayload.params,
          errorMessage: errorMessages.join(''),
        },
      });
    }

    return shuftiMessages;
  }
}
