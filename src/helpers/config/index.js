import { APIError } from '../errors';


const config = {
  ...process.env,
  NODE_ENV: process.env.NODE_ENV || 'development',
  PORT: process.env.PORT || 3000,
};

export const getEnv = (key) => {
  if (!key) return '';
  if (!config[key]) {
    throw new APIError(`Environment variable ${key} should be specified`);
  }
  return config[key];
};


export const isProduction = getEnv('NODE_ENV') === 'production';
