import mongoose from 'mongoose';
import { getEnv } from '../config';


export const connectDb = async () => {
  await mongoose.connect(getEnv('DB_CONNECTION'), {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  });
};
