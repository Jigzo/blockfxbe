// @ts-check

export default ({
  emptyToken: 'Access denied. No token provided.',
  invalidToken: 'Access denied. Token is invalid',
  shuftiProSignature: 'ShuftiPro signature error',
  applyError: 'Apply response error',
});
