const getMinMax = (start = -Infinity, end = Infinity) => (start > end ? [end, start] : [start, end]);

const getRandom = (start, end = 0, floor = 0) => {
  const [minimum, maximum] = getMinMax(start, end);
  const range = (maximum + 1) - minimum;
  const coefficient = 10 ** floor;
  return Math.floor((Math.random() * range * coefficient) / coefficient) + minimum;
};

export const getRandomStringNumber = (start, end) => {
  const randomNumber = getRandom(start, end);
  const randomString = randomNumber.toString();

  const max = Math.max(start, end);
  const stringZero = '0'.repeat(max.toString().length);

  return `${stringZero.substr(randomString.length, stringZero.length - randomString.length)}${randomString}`;
};
