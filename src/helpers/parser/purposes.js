export default (html) => {
  const groupPurposes = html.match(/<ul id="paymentPurposeSortable"[^`]+<\/ul>/)[0];
  const elementsPurposes = groupPurposes.replace(/<ul[^>]+>/, '').replace('</ul>', '');
  const listPurposes = elementsPurposes.split('</li>').reduce((accumulator, element) => {
    const correctElement = element.trim().replace(/<li[^>]+>/, '');
    if (correctElement) accumulator.push(correctElement);
    return accumulator;
  }, []);

  return listPurposes;
};
