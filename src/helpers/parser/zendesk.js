export const getMetaDataString = (text = '', metaDataName = '') => {
  const regexp = new RegExp(`- ${metaDataName} +: +[^-]+-`);
  const foundString = text.match(regexp)?.[0] || '';

  return foundString;
};

export const getMetaDataIntoZendesk = (text = '', metaDataName = '') => {
  const metadata = getMetaDataString(text, metaDataName);
  const rawData = metadata
    .replace(new RegExp(`- ${metaDataName} +: +`), '');

  const data = rawData.substr(0, rawData.length - 1).trim();

  return data;
};

export const getLastMessage = (text = '') => {
  const rawData = text.match(/<comments_formatted>-{3,100}[^-]+/)?.[0] || '';
  const data = rawData.replace(/<comments_formatted>-{3,100}/, '');

  return data;
};

export const getMessageTextIntoMessage = (message = '') => message
  .replace(/[a-zA-Z ]+, [a-zA-Z]{3} [0-9]{1,2}, [0-9]{4}, +[0-9]{1,2}:+[0-9]{1,2}/, '').trim();

export const getLastMessageText = (text = '') => {
  const message = getLastMessage(text);
  const messageText = getMessageTextIntoMessage(message);

  return messageText;
};
