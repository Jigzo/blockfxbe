const regexp = {
  accountNumber: /^[A-Z]{2}[A-Z|0-9]{6}[0-9]{9,28}$|^[0-9]{7,24}$|^[A-Z]{4}[0-9]{7,24}$/,
  iban: /^[A-Z]{2}[A-Z|0-9]{6}[0-9]{9,28}$/,
  bic: /^[A-Z|0-9]{4}[A-Z]{2}[A-Z|0-9]{5}$|^[A-Z|0-9]{4}[A-Z]{2}[A-Z|0-9]{2}$/,
  sortCode: /^[0-9]{6}$/,
  aba: /^[0-9]{9}$/,
  countryCode: /^[A-Z]{2}$/,
  currencyCode: /^[A-Z]{3}$/,

  email: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
  phone: /\+[0-9]{10,17}$/,

  digitalCode: /^[0-9]{6}$/,
};

export default regexp;
