import { countries } from '../static/country';


export const bankValidator = (req, res) => {
  const {
    iban = '',
    accountNumber = '',
    bic = '',
    aba = '',
    sortCode = '',
    country = '',
    isLocal = false,
  } = req.body;

  const sendErr = (message) => res.status(403).json({ message });

  if (!country.match(/^[A-Z]{2}$/)) {
    return sendErr('country code is invalid');
  }

  const checkCountry = (countryCode = '') => !!countries.find(({ code }) => code === countryCode);

  if (!checkCountry(country)) {
    return sendErr('country is not found');
  }

  // check default params
  if (
    !(iban || accountNumber)
    || !((bic || iban)
      || (aba && country === 'US')
      || (sortCode && country === 'GB')
    )
  ) {
    return sendErr('This response to be empty required parameters');
  }

  const countryBic = bic.substr(4, 2);
  const countryIban = iban.substr(0, 2);

  // check IBAN
  if (iban) {
    if (!iban.match(/^[A-Z]{2}[A-Z|0-9]{6}[0-9]{9,28}$/)) {
      return sendErr('iban is invalid');
    }

    if (countryIban !== country) {
      if (!checkCountry(countryIban)) {
        return sendErr('iban country is invalid');
      }

      if (bic) {
        if (countryBic === country) {
          return sendErr('iban country is invalid');
        }

        if (countryBic === countryIban) {
          return sendErr('country is invalid');
        }
      }

      return sendErr('country or iban is invalid');
    }
  }

  // check BIC
  if (bic) {
    if (!bic.match(/^[A-Z|0-9]{4}[A-Z]{2}[A-Z|0-9]{5}$|^[A-Z|0-9]{4}[A-Z]{2}[A-Z|0-9]{2}$/)) {
      return sendErr('bic code is invalid');
    }

    if (countryBic !== country) {
      if (!checkCountry(countryBic)) {
        return sendErr('bic country is invalid');
      }

      if (iban) {
        if (countryIban === country) {
          return sendErr('bic country is invalid');
        }

        if (countryIban === countryBic) {
          return sendErr('country is invalid');
        }
      }

      return sendErr('country or bic is invalid');
    }
  }

  // check account-number
  if (accountNumber) {
    if (!accountNumber.match(/^[A-Z]{2}[A-Z|0-9]{6}[0-9]{9,28}$|^[0-9]{7,24}$|^[A-Z]{4}[0-9]{7,24}$/)) {
      return sendErr('accountNumber is invalid');
    }

    if (accountNumber.match(/^[A-Z]{2}[A-Z|0-9]{6}[0-9]{9,28}$/)) {
      const countryIntoAcc = accountNumber.substr(0, 2);

      if (countryIntoAcc !== country) {
        if (!checkCountry(countryIntoAcc)) {
          return sendErr('country code into accountNumber is invalid');
        }

        return sendErr('country code into accountNumber or country is invalid');
      }
    }
  }

  // check other-code (only local)
  if (isLocal) {
    switch (country) {
      case 'GB':
        if (!sortCode.match(/^[0-9]{6}$/)) {
          return sendErr('sort-code is invalid');
        }
        break;
      case 'US':
        if (!aba.match(/^[0-9]{9}$/)) {
          return sendErr('aba code is invalid');
        }
        break;
      default: return false;
    }
  }

  return false;
};
