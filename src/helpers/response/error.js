// @ts-check


export const errorResponse = (res, {
  message = {}, status = 500, response = { status }, ...err
}, json = true) => {
  let body = message;
  const { config, ...otherError } = err;
  if (!message || json) {
    body = ({
      message,
      ...otherError,
      stack: err.stack && err.stack.split('\n'),
    });
  }

  return res.status(response?.status || status || 500)[json ? 'json' : 'send'](body);
};
