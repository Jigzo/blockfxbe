export const resourcesNotFound = (res, message = 'Resources Not Found') => res.status(404).json({
  message,
});
