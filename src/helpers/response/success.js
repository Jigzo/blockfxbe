export const successResponse = (res, payload, status = 200, json = true) => res
  .status(status)[json ? 'json' : 'send'](payload);
