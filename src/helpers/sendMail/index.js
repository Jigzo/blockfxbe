import process from 'process';
import { readFile } from 'fs';
import { join } from 'path';
import { createTransport } from 'nodemailer';
import { compile } from 'handlebars';
import { getEnv } from '../config';


const transporter = createTransport({
  host: getEnv('MAIL_HOST'),
  port: getEnv('MAIL_PORT'),
  auth: {
    user: getEnv('MAIL_USERNAME'),
    pass: getEnv('MAIL_PASSWORD'),
  },
});


export default ({
  from = getEnv('FROM_MAIL'), to = '', subject = 'blockFx', text = '', templateName = '', params = {},
}) => new Promise((resolve, reject) => readFile(
  join(`${process.env.PWD}/public/template/${templateName}.handlebars`), 'utf-8', (error, data) => {
    if (error) return reject(error);
    const hbs = compile(data);
    return transporter.sendMail({
      from,
      to,
      subject,
      text,
      html: hbs(params),
    })
      .then((dataMail) => resolve(dataMail))
      .catch((errorMail) => reject(errorMail));
  },
));
