// @ts-check

export const countries = [{
  name: 'Afghanistan', code: 'AF', IBAN: false, id: 1, dialCode: '+93',
}, {
  name: 'Albania', code: 'AL', IBAN: false, id: 5, dialCode: '+355',
}, {
  name: 'Algeria', code: 'DZ', IBAN: false, id: 65, dialCode: '+213',
}, {
  name: 'American Samoa', code: 'AS', IBAN: false, id: 10, dialCode: '+1684',
}, {
  name: 'Andorra', code: 'AD', IBAN: true, id: 6, dialCode: '+376',
}, {
  name: 'Angola', code: 'AO', IBAN: false, id: 2, dialCode: '+244',
}, {
  name: 'Anguilla', code: 'AI', IBAN: false, id: 3, dialCode: '+1264',
}, {
  name: 'Antarctica', code: 'AQ', IBAN: false, id: 11, dialCode: '+672',
}, {
  name: 'Antigua and Barbuda', code: 'AG', IBAN: false, id: 13, dialCode: '+1268',
}, {
  name: 'Argentina', code: 'AR', IBAN: false, id: 8, dialCode: '+54',
}, {
  name: 'Armenia', code: 'AM', IBAN: false, id: 9, dialCode: '+374',
}, {
  name: 'Aruba', code: 'AW', IBAN: false, id: 0, dialCode: '+297',
}, {
  name: 'Australia', code: 'AU', IBAN: false, id: 14, dialCode: '+61',
}, {
  name: 'Austria', code: 'AT', IBAN: true, id: 15, dialCode: '+43',
}, {
  name: 'Azerbaijan', code: 'AZ', IBAN: true, id: 16, dialCode: '+994',
}, {
  name: 'Bahamas', code: 'BS', IBAN: false, id: 24, dialCode: '+1242',
}, {
  name: 'Bahrain', code: 'BH', IBAN: true, id: 23, dialCode: '+973',
}, {
  name: 'Bangladesh', code: 'BD', IBAN: false, id: 21, dialCode: '+880',
}, {
  name: 'Barbados', code: 'BB', IBAN: false, id: 34, dialCode: '+1246',
}, {
  name: 'Belarus', code: 'BY', IBAN: true, id: 28, dialCode: '+375',
}, {
  name: 'Belgium', code: 'BE', IBAN: true, id: 18, dialCode: '+32',
}, {
  name: 'Belize', code: 'BZ', IBAN: false, id: 29, dialCode: '+501',
}, {
  name: 'Benin', code: 'BJ', IBAN: false, id: 19, dialCode: '+229',
}, {
  name: 'Bermuda', code: 'BM', IBAN: false, id: 30, dialCode: '+1441',
}, {
  name: 'Bhutan', code: 'BT', IBAN: false, id: 36, dialCode: '+975',
}, {
  name: 'Bolivia', code: 'BO', IBAN: false, id: 31, dialCode: '+591',
}, {
  name: 'Bosnia and Herzegovina', code: 'BA', IBAN: true, id: 25, dialCode: '+387',
}, {
  name: 'Botswana', code: 'BW', IBAN: false, id: 38, dialCode: '+267',
}, {
  name: 'Bouvet Island', code: 'BV', IBAN: false, id: 37,
}, {
  name: 'Brazil', code: 'BR', IBAN: true, id: 33, dialCode: '+55',
}, {
  name: 'British Indian Ocean Territory', code: 'IO', IBAN: false, id: 106, dialCode: '+246',
}, {
  name: 'British Virgin Islands', code: 'VG', IBAN: true, id: 240, dialCode: '+1284',
}, {
  name: 'Brunei', code: 'BN', IBAN: false, id: 35, dialCode: '+673',
}, {
  name: 'Bulgaria', code: 'BG', IBAN: true, id: 22, dialCode: '+359',
}, {
  name: 'Burkina Faso', code: 'BF', IBAN: false, id: 20, dialCode: '+226',
}, {
  name: 'Burundi', code: 'BI', IBAN: false, id: 17, dialCode: '+257',
}, {
  name: 'Cambodia', code: 'KH', IBAN: false, id: 120, dialCode: '+855',
}, {
  name: 'Cameroon', code: 'CM', IBAN: false, id: 46, dialCode: '+237',
}, {
  name: 'Canada', code: 'CA', IBAN: false, id: 40, dialCode: '+1',
}, {
  name: 'Cape Verde', code: 'CV', IBAN: false, id: 52, dialCode: '+238',
}, {
  name: 'Caribbean Netherlands', code: 'BQ', IBAN: false, id: 32,
}, {
  name: 'Cayman Islands', code: 'KY', IBAN: false, id: 57, dialCode: '+ 345',
}, {
  name: 'Central African Republic', code: 'CF', IBAN: false, id: 39, dialCode: '+236',
}, {
  name: 'Chad', code: 'TD', IBAN: false, id: 217, dialCode: '+235',
}, {
  name: 'Chile', code: 'CL', IBAN: false, id: 43, dialCode: '+56',
}, {
  name: 'China', code: 'CN', IBAN: false, id: 44, dialCode: '+86',
}, {
  name: 'Christmas Island', code: 'CX', IBAN: false, id: 56, dialCode: '+61',
}, {
  name: 'Cocos (Keeling) Islands', code: 'CC', IBAN: false, id: 41, dialCode: '+61',
}, {
  name: 'Colombia', code: 'CO', IBAN: false, id: 50, dialCode: '+57',
}, {
  name: 'Comoros', code: 'KM', IBAN: false, id: 51, dialCode: '+269',
}, {
  name: 'Cook Islands', code: 'CK', IBAN: false, id: 49, dialCode: '+682',
}, {
  name: 'Costa Rica', code: 'CR', IBAN: true, id: 53, dialCode: '+506',
}, {
  name: 'Croatia', code: 'HR', IBAN: true, id: 100, dialCode: '+385',
}, {
  name: 'Cuba', code: 'CU', IBAN: false, id: 54, dialCode: '+53',
}, {
  name: 'Curaçao', code: 'CW', IBAN: false, id: 55,
}, {
  name: 'Cyprus', code: 'CY', IBAN: true, id: 58, dialCode: '+357',
}, {
  name: 'Czechia', code: 'CZ', IBAN: true, id: 59, dialCode: '+420',
}, {
  name: 'DR Congo', code: 'CD', IBAN: false, id: 47, dialCode: '+243',
}, {
  name: 'Denmark', code: 'DK', IBAN: true, id: 63, dialCode: '+45',
}, {
  name: 'Djibouti', code: 'DJ', IBAN: false, id: 61, dialCode: '+253',
}, {
  name: 'Dominica', code: 'DM', IBAN: false, id: 62, dialCode: '+1767',
}, {
  name: 'Dominican Republic', code: 'DO', IBAN: true, id: 64, dialCode: '+1849',
}, {
  name: 'Ecuador', code: 'EC', IBAN: false, id: 66, dialCode: '+593',
}, {
  name: 'Egypt', code: 'EG', IBAN: false, id: 67, dialCode: '+20',
}, {
  name: 'El Salvador', code: 'SV', IBAN: true, id: 201, dialCode: '+503',
}, {
  name: 'Equatorial Guinea', code: 'GQ', IBAN: false, id: 89, dialCode: '+240',
}, {
  name: 'Eritrea', code: 'ER', IBAN: false, id: 68, dialCode: '+291',
}, {
  name: 'Estonia', code: 'EE', IBAN: true, id: 71, dialCode: '+372',
}, {
  name: 'Eswatini', code: 'SZ', IBAN: false, id: 212, dialCode: '+268',
}, {
  name: 'Ethiopia', code: 'ET', IBAN: false, id: 72, dialCode: '+251',
}, {
  name: 'Falkland Islands', code: 'FK', IBAN: false, id: 75, dialCode: '+500',
}, {
  name: 'Faroe Islands', code: 'FO', IBAN: true, id: 77, dialCode: '+298',
}, {
  name: 'Fiji', code: 'FJ', IBAN: false, id: 74, dialCode: '+679',
}, {
  name: 'Finland', code: 'FI', IBAN: true, id: 73, dialCode: '+358',
}, {
  name: 'France', code: 'FR', IBAN: true, id: 76, dialCode: '+33',
}, {
  name: 'French Guiana', code: 'GF', IBAN: false, id: 94, dialCode: '+594',
}, {
  name: 'French Polynesia', code: 'PF', IBAN: false, id: 187, dialCode: '+689',
}, {
  name: 'French Southern and Antarctic Lands', code: 'TF', IBAN: false, id: 12,
}, {
  name: 'Gabon', code: 'GA', IBAN: false, id: 79, dialCode: '+241',
}, {
  name: 'Gambia', code: 'GM', IBAN: false, id: 87, dialCode: '+220',
}, {
  name: 'Georgia', code: 'GE', IBAN: true, id: 81, dialCode: '+995',
}, {
  name: 'Germany', code: 'DE', IBAN: true, id: 60, dialCode: '+49',
}, {
  name: 'Ghana', code: 'GH', IBAN: false, id: 83, dialCode: '+233',
}, {
  name: 'Gibraltar', code: 'GI', IBAN: true, id: 84, dialCode: '+350',
}, {
  name: 'Greece', code: 'GR', IBAN: true, id: 90, dialCode: '+30',
}, {
  name: 'Greenland', code: 'GL', IBAN: true, id: 92, dialCode: '+299',
}, {
  name: 'Grenada', code: 'GD', IBAN: false, id: 91, dialCode: '+1473',
}, {
  name: 'Guadeloupe', code: 'GP', IBAN: false, id: 86, dialCode: '+590',
}, {
  name: 'Guam', code: 'GU', IBAN: false, id: 95, dialCode: '+1671',
}, {
  name: 'Guatemala', code: 'GT', IBAN: true, id: 93, dialCode: '+502',
}, {
  name: 'Guernsey', code: 'GG', IBAN: false, id: 82, dialCode: '+44',
}, {
  name: 'Guinea', code: 'GN', IBAN: false, id: 85, dialCode: '+224',
}, {
  name: 'Guinea-Bissau', code: 'GW', IBAN: false, id: 88, dialCode: '+245',
}, {
  name: 'Guyana', code: 'GY', IBAN: false, id: 96, dialCode: '+595',
}, {
  name: 'Haiti', code: 'HT', IBAN: false, id: 101, dialCode: '+509',
}, {
  name: 'Heard Island and McDonald Islands', code: 'HM', IBAN: false, id: 98,
}, {
  name: 'Honduras', code: 'HN', IBAN: false, id: 99, dialCode: '+504',
}, {
  name: 'Hong Kong', code: 'HK', IBAN: false, id: 97, dialCode: '+852',
}, {
  name: 'Hungary', code: 'HU', IBAN: true, id: 102, dialCode: '+36',
}, {
  name: 'Iceland', code: 'IS', IBAN: true, id: 110, dialCode: '+354',
}, {
  name: 'India', code: 'IN', IBAN: false, id: 105, dialCode: '+91',
}, {
  name: 'Indonesia', code: 'ID', IBAN: false, id: 103, dialCode: '+62',
}, {
  name: 'Iran', code: 'IR', IBAN: false, id: 108, dialCode: '+98',
}, {
  name: 'Iraq', code: 'IQ', IBAN: true, id: 109, dialCode: '+964',
}, {
  name: 'Ireland', code: 'IE', IBAN: true, id: 107, dialCode: '+353',
}, {
  name: 'Isle of Man', code: 'IM', IBAN: false, id: 104, dialCode: '+44',
}, {
  name: 'Israel', code: 'IL', IBAN: true, id: 111, dialCode: '+972',
}, {
  name: 'Italy', code: 'IT', IBAN: true, id: 112, dialCode: '+39',
}, {
  name: 'Ivory Coast', code: 'CI', IBAN: false, id: 45, dialCode: '+225',
}, {
  name: 'Jamaica', code: 'JM', IBAN: false, id: 113, dialCode: '+1876',
}, {
  name: 'Japan', code: 'JP', IBAN: false, id: 116, dialCode: '+81',
}, {
  name: 'Jersey', code: 'JE', IBAN: false, id: 114, dialCode: '+44',
}, {
  name: 'Jordan', code: 'JO', IBAN: true, id: 115, dialCode: '+962',
}, {
  name: 'Kazakhstan', code: 'KZ', IBAN: true, id: 117, dialCode: '+77',
}, {
  name: 'Kenya', code: 'KE', IBAN: false, id: 118, dialCode: '+254',
}, {
  name: 'Kiribati', code: 'KI', IBAN: false, id: 121, dialCode: '+686',
}, {
  name: 'Kuwait', code: 'KW', IBAN: true, id: 125, dialCode: '+965',
}, {
  name: 'Kyrgyzstan', code: 'KG', IBAN: false, id: 119, dialCode: '+996',
}, {
  name: 'Laos', code: 'LA', IBAN: false, id: 126, dialCode: '+856',
}, {
  name: 'Latvia', code: 'LV', IBAN: true, id: 136, dialCode: '+371',
}, {
  name: 'Lebanon', code: 'LB', IBAN: true, id: 127, dialCode: '+961',
}, {
  name: 'Lesotho', code: 'LS', IBAN: false, id: 133, dialCode: '+266',
}, {
  name: 'Liberia', code: 'LR', IBAN: false, id: 128, dialCode: '+231',
}, {
  name: 'Libya', code: 'LY', IBAN: false, id: 129, dialCode: '+218',
}, {
  name: 'Liechtenstein', code: 'LI', IBAN: true, id: 131, dialCode: '+423',
}, {
  name: 'Lithuania', code: 'LT', IBAN: true, id: 134, dialCode: '+370',
}, {
  name: 'Luxembourg', code: 'LU', IBAN: true, id: 135, dialCode: '+352',
}, {
  name: 'Macau', code: 'MO', IBAN: false, id: 137, dialCode: '+853',
}, {
  name: 'Madagascar', code: 'MG', IBAN: false, id: 142, dialCode: '+261',
}, {
  name: 'Malawi', code: 'MW', IBAN: false, id: 158, dialCode: '+265',
}, {
  name: 'Malaysia', code: 'MY', IBAN: false, id: 159, dialCode: '+60',
}, {
  name: 'Maldives', code: 'MV', IBAN: false, id: 143, dialCode: '+960',
}, {
  name: 'Mali', code: 'ML', IBAN: false, id: 147, dialCode: '+223',
}, {
  name: 'Malta', code: 'MT', IBAN: true, id: 148, dialCode: '+356',
}, {
  name: 'Marshall Islands', code: 'MH', IBAN: false, id: 145, dialCode: '+692',
}, {
  name: 'Martinique', code: 'MQ', IBAN: false, id: 156, dialCode: '+596',
}, {
  name: 'Mauritania', code: 'MR', IBAN: true, id: 154, dialCode: '+222',
}, {
  name: 'Mauritius', code: 'MU', IBAN: true, id: 157, dialCode: '+230',
}, {
  name: 'Mayotte', code: 'YT', IBAN: false, id: 160, dialCode: '+262',
}, {
  name: 'Mexico', code: 'MX', IBAN: false, id: 144, dialCode: '+52',
}, {
  name: 'Micronesia', code: 'FM', IBAN: false, id: 78, dialCode: '+691',
}, {
  name: 'Moldova', code: 'MD', IBAN: true, id: 141, dialCode: '+373',
}, {
  name: 'Monaco', code: 'MC', IBAN: true, id: 140, dialCode: '+377',
}, {
  name: 'Mongolia', code: 'MN', IBAN: false, id: 151, dialCode: '+976',
}, {
  name: 'Montenegro', code: 'ME', IBAN: true, id: 150, dialCode: '+382',
}, {
  name: 'Montserrat', code: 'MS', IBAN: false, id: 155, dialCode: '+1664',
}, {
  name: 'Morocco', code: 'MA', IBAN: false, id: 139, dialCode: '+212',
}, {
  name: 'Mozambique', code: 'MZ', IBAN: false, id: 153, dialCode: '+258',
}, {
  name: 'Myanmar', code: 'MM', IBAN: false, id: 149, dialCode: '+95',
}, {
  name: 'Namibia', code: 'NA', IBAN: false, id: 161, dialCode: '+264',
}, {
  name: 'Nauru', code: 'NR', IBAN: false, id: 171, dialCode: '+674',
}, {
  name: 'Nepal', code: 'NP', IBAN: false, id: 170, dialCode: '+977',
}, {
  name: 'Netherlands', code: 'NL', IBAN: true, id: 168, dialCode: '+31',
}, {
  name: 'New Caledonia', code: 'NC', IBAN: false, id: 162, dialCode: '+687',
}, {
  name: 'New Zealand', code: 'NZ', IBAN: false, id: 172, dialCode: '+64',
}, {
  name: 'Nicaragua', code: 'NI', IBAN: false, id: 166, dialCode: '+505',
}, {
  name: 'Niger', code: 'NE', IBAN: false, id: 163, dialCode: '+227',
}, {
  name: 'Nigeria', code: 'NG', IBAN: false, id: 165, dialCode: '+234',
}, {
  name: 'Niue', code: 'NU', IBAN: false, id: 167, dialCode: '+683',
}, {
  name: 'Norfolk Island', code: 'NF', IBAN: false, id: 164, dialCode: '+672',
}, {
  name: 'North Korea', code: 'KP', IBAN: false, id: 183, dialCode: '+850',
}, {
  name: 'North Macedonia', code: 'MK', IBAN: true, id: 146, dialCode: '+389',
}, {
  name: 'Northern Mariana Islands', code: 'MP', IBAN: false, id: 152, dialCode: '+1670',
}, {
  name: 'Norway', code: 'NO', IBAN: true, id: 169, dialCode: '+47',
}, {
  name: 'Oman', code: 'OM', IBAN: false, id: 173, dialCode: '+968',
}, {
  name: 'Pakistan', code: 'PK', IBAN: true, id: 174, dialCode: '+92',
}, {
  name: 'Palau', code: 'PW', IBAN: false, id: 179, dialCode: '+680',
}, {
  name: 'Palestine', code: 'PS', IBAN: true, id: 186, dialCode: '+970',
}, {
  name: 'Panama', code: 'PA', IBAN: false, id: 175, dialCode: '+507',
}, {
  name: 'Papua New Guinea', code: 'PG', IBAN: false, id: 180, dialCode: '+675',
}, {
  name: 'Paraguay', code: 'PY', IBAN: false, id: 185, dialCode: '+595',
}, {
  name: 'Peru', code: 'PE', IBAN: false, id: 177, dialCode: '+51',
}, {
  name: 'Philippines', code: 'PH', IBAN: false, id: 178, dialCode: '+63',
}, {
  name: 'Pitcairn Islands', code: 'PN', IBAN: false, id: 176, dialCode: '+872',
}, {
  name: 'Poland', code: 'PL', IBAN: true, id: 181, dialCode: '+48',
}, {
  name: 'Portugal', code: 'PT', IBAN: true, id: 184, dialCode: '+351',
}, {
  name: 'Puerto Rico', code: 'PR', IBAN: false, id: 182, dialCode: '+1939',
}, {
  name: 'Qatar', code: 'QA', IBAN: true, id: 188, dialCode: '+974',
}, {
  name: 'Republic of the Congo', code: 'CG', IBAN: false, id: 48, dialCode: '+242',
}, {
  name: 'Romania', code: 'RO', IBAN: true, id: 190, dialCode: '+40',
}, {
  name: 'Russia', code: 'RU', IBAN: false, id: 191, dialCode: '+7',
}, {
  name: 'Rwanda', code: 'RW', IBAN: false, id: 192, dialCode: '+250',
}, {
  name: 'Réunion', code: 'RE', IBAN: false, id: 189, dialCode: '+262',
}, {
  name: 'Saint Barthélemy', code: 'BL', IBAN: false, id: 26, dialCode: '+590',
}, {
  name: 'Saint Helena, Ascension and Tristan da Cunha', code: 'SH', IBAN: false, id: 27, dialCode: '+290',
}, {
  name: 'Saint Kitts and Nevis', code: 'KN', IBAN: false, id: 122, dialCode: '+1869',
}, {
  name: 'Saint Lucia', code: 'LC', IBAN: true, id: 130, dialCode: '+1758',
}, {
  name: 'Saint Martin', code: 'MF', IBAN: false, id: 138, dialCode: '+590',
}, {
  name: 'Saint Pierre and Miquelon', code: 'PM', IBAN: false, id: 204, dialCode: '+508',
}, {
  name: 'Saint Vincent and the Grenadines', code: 'VC', IBAN: false, id: 238, dialCode: '+1784',
}, {
  name: 'Samoa', code: 'WS', IBAN: false, id: 245, dialCode: '+685',
}, {
  name: 'San Marino', code: 'SM', IBAN: true, id: 202, dialCode: '+378',
}, {
  name: 'Saudi Arabia', code: 'SA', IBAN: true, id: 193, dialCode: '+966',
}, {
  name: 'Senegal', code: 'SN', IBAN: false, id: 195, dialCode: '+221',
}, {
  name: 'Serbia', code: 'RS', IBAN: true, id: 205, dialCode: '+381',
}, {
  name: 'Seychelles', code: 'SC', IBAN: true, id: 214, dialCode: '+248',
}, {
  name: 'Sierra Leone', code: 'SL', IBAN: false, id: 200, dialCode: '+232',
}, {
  name: 'Singapore', code: 'SG', IBAN: false, id: 196, dialCode: '+65',
}, {
  name: 'Sint Maarten', code: 'SX', IBAN: false, id: 213,
}, {
  name: 'Slovakia', code: 'SK', IBAN: true, id: 209, dialCode: '+421',
}, {
  name: 'Slovenia', code: 'SI', IBAN: true, id: 210, dialCode: '+386',
}, {
  name: 'Solomon Islands', code: 'SB', IBAN: false, id: 199, dialCode: '+677',
}, {
  name: 'Somalia', code: 'SO', IBAN: false, id: 203, dialCode: '+252',
}, {
  name: 'South Africa', code: 'ZA', IBAN: false, id: 247, dialCode: '+27',
}, {
  name: 'South Georgia', code: 'GS', IBAN: false, id: 197, dialCode: '+500',
}, {
  name: 'South Korea', code: 'KR', IBAN: false, id: 123, dialCode: '+82',
}, {
  name: 'South Sudan', code: 'SS', IBAN: false, id: 206, dialCode: '+211',
}, {
  name: 'Spain', code: 'ES', IBAN: true, id: 70, dialCode: '+34',
}, {
  name: 'Sri Lanka', code: 'LK', IBAN: false, id: 132, dialCode: '+94',
}, {
  name: 'Sudan', code: 'SD', IBAN: false, id: 194, dialCode: '+249',
}, {
  name: 'Suriname', code: 'SR', IBAN: false, id: 208, dialCode: '+597',
}, {
  name: 'Svalbard and Jan Mayen', code: 'SJ', IBAN: false, id: 198, dialCode: '+47',
}, {
  name: 'Sweden', code: 'SE', IBAN: true, id: 211, dialCode: '+46',
}, {
  name: 'Switzerland', code: 'CH', IBAN: true, id: 42, dialCode: '+41',
}, {
  name: 'Syria', code: 'SY', IBAN: false, id: 215, dialCode: '+963',
}, {
  name: 'São Tomé and Príncipe', code: 'ST', IBAN: true, id: 207, dialCode: '+239',
}, {
  name: 'Taiwan', code: 'TW', IBAN: false, id: 229, dialCode: '+886',
}, {
  name: 'Tajikistan', code: 'TJ', IBAN: false, id: 220, dialCode: '+992',
}, {
  name: 'Tanzania', code: 'TZ', IBAN: false, id: 230, dialCode: '+255',
}, {
  name: 'Thailand', code: 'TH', IBAN: false, id: 219, dialCode: '+66',
}, {
  name: 'Timor-Leste', code: 'TL', IBAN: true, id: 223, dialCode: '+670',
}, {
  name: 'Togo', code: 'TG', IBAN: false, id: 218, dialCode: '+228',
}, {
  name: 'Tokelau', code: 'TK', IBAN: false, id: 221, dialCode: '+690',
}, {
  name: 'Tonga', code: 'TO', IBAN: false, id: 224, dialCode: '+676',
}, {
  name: 'Trinidad and Tobago', code: 'TT', IBAN: false, id: 225, dialCode: '+1868',
}, {
  name: 'Tunisia', code: 'TN', IBAN: true, id: 226, dialCode: '+216',
}, {
  name: 'Turkey', code: 'TR', IBAN: true, id: 227, dialCode: '+90',
}, {
  name: 'Turkmenistan', code: 'TM', IBAN: false, id: 222, dialCode: '+993',
}, {
  name: 'Turks and Caicos Islands', code: 'TC', IBAN: false, id: 216, dialCode: '+1649',
}, {
  name: 'Tuvalu', code: 'TV', IBAN: false, id: 228, dialCode: '+688',
}, {
  name: 'Uganda', code: 'UG', IBAN: false, id: 231, dialCode: '+256',
}, {
  name: 'Ukraine', code: 'UA', IBAN: true, id: 232, dialCode: '+380',
}, {
  name: 'United Arab Emirates', code: 'AE', IBAN: true, id: 7, dialCode: '+971',
}, {
  name: 'United Kingdom', code: 'GB', IBAN: true, id: 80, dialCode: '+44',
}, {
  name: 'United States', code: 'US', IBAN: false, id: 235, dialCode: '+1',
}, {
  name: 'United States Minor Outlying Islands', code: 'UM', IBAN: false, id: 233,
}, {
  name: 'United States Virgin Islands', code: 'VI', IBAN: false, id: 241, dialCode: '+1340',
}, {
  name: 'Uruguay', code: 'UY', IBAN: false, id: 234, dialCode: '+598',
}, {
  name: 'Uzbekistan', code: 'UZ', IBAN: false, id: 236, dialCode: '+998',
}, {
  name: 'Vanuatu', code: 'VU', IBAN: false, id: 243, dialCode: '+678',
}, {
  name: 'Vatican City', code: 'VA', IBAN: true, id: 237, dialCode: '+379',
}, {
  name: 'Venezuela', code: 'VE', IBAN: false, id: 239, dialCode: '+58',
}, {
  name: 'Vietnam', code: 'VN', IBAN: false, id: 242, dialCode: '+84',
}, {
  name: 'Wallis and Futuna', code: 'WF', IBAN: false, id: 244, dialCode: '+681',
}, {
  name: 'Western Sahara', code: 'EH', IBAN: false, id: 69,
}, {
  name: 'Yemen', code: 'YE', IBAN: false, id: 246, dialCode: '+967',
}, {
  name: 'Zambia', code: 'ZM', IBAN: false, id: 248, dialCode: '+260',
}, {
  name: 'Zimbabwe', code: 'ZW', IBAN: false, id: 249, dialCode: '+263',
}, {
  name: 'Åland Islands', code: 'AX', IBAN: false, id: 4, dialCode: '+358',
}];
