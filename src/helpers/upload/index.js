/* eslint-disable no-shadow */
/* eslint-disable no-param-reassign */
import fs from 'fs/promises';
import path from 'path';
import process from 'process';


const uploadDir = `${process.env.PWD}/uploads`;

const checkUploadDir = async (dir = '') => {
  const checkLocalDir = async () => {
    try {
      return await fs.stat(uploadDir);
    } catch {
      return await fs.mkdir(uploadDir);
    }
  };

  await checkLocalDir();

  if (dir) {
    const checkDir = async (dir = '') => {
      const dirs = dir.split('/');
      const checkDirs = async (uploadPath = uploadDir, first = '', ...next) => {
        if (!first && next?.length > 0) {
          [first = ''] = next.splice(0, 1);
        }

        if (first) {
          const checkPath = path.resolve(uploadPath, first);
          try {
            await fs.stat(checkPath);
          } catch {
            await fs.mkdir(checkPath);
          } finally {
            if (next?.length > 0) {
              await checkDirs(checkPath, ...next);
            }
          }
        }
      };

      return await checkDirs(uploadDir, ...dirs);
    };

    return await checkDir(dir);
  }

  return await true;
};


const uploadFile = async ({
  uploadPath = '',
  name = '',
  file = {},
  buffer = null,
}) => {
  if (!buffer && file) buffer = file.buffer;
  if (!name && file) name = file.originalname;

  if (buffer) {
    await checkUploadDir(uploadPath);
    const uploadDirPath = path.resolve(uploadDir, uploadPath);
    const uploadFilePath = path.resolve(uploadDirPath, name);
    await fs.writeFile(uploadFilePath, buffer);

    return uploadFilePath;
  }

  return null;
};

export default uploadFile;
