// Core
import fs from 'fs';
import { APIError } from '../errors';


export const convertToBase64 = (path) => {
  try {
    const bitMap = fs.readFileSync(path);
    return Buffer.from(bitMap).toString('base64');
  } catch ({ message }) {
    throw new APIError(message);
  }
};
