import fs from 'fs';


export default (path) => fs.unlinkSync(path);
