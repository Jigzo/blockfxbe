// Core
import crypto from 'crypto';

// Instruments
import { getEnv } from '../config';


export const createHashSha1 = (data) => {
  const apiSecret = getEnv('BASE_API_SECRET');

  return crypto
    .createHash('sha1')
    .update(data)
    .update(apiSecret)
    .digest('hex');
};
