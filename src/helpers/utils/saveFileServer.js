import { move } from 'fs-extra';
import path from 'path';
import { APIError } from '../errors';


const saveFile = async (fileOldPath, fileName) => {
  const newPath = path.join(path.dirname(__dirname), `/storage/${fileName}`);
  try {
    await move(fileOldPath, newPath, { mkdirp: true });

    return newPath;
  } catch (e) {
    throw new APIError(e.message);
  }
};


export default saveFile;
