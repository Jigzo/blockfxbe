export default ({
  type: 'object',
  properties: {
    currency: {
      type: 'string',
      trim: true,
      minLength: 3,
    },
    amount: {
      type: 'number',
    },
    beneficiaryId: {
      type: 'number',
    },
    narrative: {
      type: 'string',
    },
    forex: {
      type: 'number',
    },
  },
  required: [
    'currency',
    'amount',
    'beneficiaryId',
  ],
  additionalProperties: false,
});
