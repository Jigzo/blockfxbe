export default ({
  type: 'object',
  properties: {
    name: {
      type: 'string',
      trim: true,
    },
    currency: {
      type: 'string',
      trim: true,
      minLength: 3,
      maxlength: 3,
    },
    country: {
      type: 'string',
      trim: true,
      minLength: 2,
      maxlength: 2,
    },
    bankName: {
      type: 'string',
      trim: true,
      minLength: 1,
    },
    accountNumber: {
      type: 'string',
      trim: true,
      maxlength: 34,
    },
    sortCode: {
      type: 'string',
      trim: true,
      maxlength: 6,
    },
    aba: {
      type: 'string',
      trim: true,
      maxlength: 9,
    },
    bankAddress: {
      type: 'string',
      trim: true,
      minLength: 1,
    },
    iban: {
      type: 'string',
      trim: true,
      maxLength: 36,
    },
    bic: {
      type: 'string',
      trim: true,
      maxlength: 11,
    },
    accountType: {
      type: 'string',
      trim: true,
      maxlength: 1,
    },
    isLocal: {
      type: 'boolean',
    },
  },
  required: [
    'country',
    'currency',
  ],
  additionalProperties: false,
});
