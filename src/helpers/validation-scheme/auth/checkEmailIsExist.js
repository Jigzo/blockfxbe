export default ({
  type: 'object',
  properties: {
    email: {
      type: 'string',
      format: 'email',
      trim: true,
    },
  },
  required: [
    'email',
  ],
  additionalProperties: false,
});
