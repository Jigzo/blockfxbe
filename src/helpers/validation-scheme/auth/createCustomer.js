export default ({
  type: 'object',
  properties: {
    firstName: {
      type: 'string',
      trim: true,
      minLength: 1,
    },
    lastName: {
      type: 'string',
      trim: true,
      minLength: 1,
    },
    country: {
      type: 'string',
      trim: true,
      minLength: 1,
    },
    email: {
      type: 'string',
      format: 'email',
      trim: true,
    },
    phone: {
      type: 'string',
      trim: true,
      minLength: 1,
    },
    terms: {
      type: 'boolean',
    },
  },
  required: [
    'firstName',
    'lastName',
    'country',
    'email',
    'phone',
    'terms',
  ],
  additionalProperties: false,
});
