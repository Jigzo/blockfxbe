export default ({
  type: 'object',
  properties: {
    nonce: {
      type: 'string',
    },
    id: {
      type: 'integer',
    },
    time: {
      type: 'integer',
    },
    password: {
      type: 'string',
    },
  },
  required: [
    'nonce',
    'id',
    'time',
    'password',
  ],
  additionalProperties: false,
});
