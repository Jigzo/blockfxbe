export default ({
  type: 'object',
  properties: {
    nonce: {
      type: 'string',
    },
    id: {
      type: 'integer',
    },
    time: {
      type: 'integer',
    },
  },
  required: [
    'nonce',
    'id',
    'time',
  ],
  additionalProperties: false,
});
