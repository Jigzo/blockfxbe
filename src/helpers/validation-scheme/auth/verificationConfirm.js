export default ({
  type: 'object',
  properties: {
    nonce: {
      type: 'string',
    },
  },
  required: [
    'nonce',
  ],
});
