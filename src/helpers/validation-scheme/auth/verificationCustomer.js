const docSchema = {
  type: ['object', 'string'],
  properties: {
    type: {
      type: 'string',
      pattern: 'image.*|application.pdf',
    },
    size: {
      type: 'number',
      maximum: 9437184, // 9mb
    },
  },
  required: ['type', 'size'],
};

export default ({
  type: 'object',
  properties: {
    copyPassport: docSchema,
    selfiePassport: docSchema,
    addressPassport: docSchema,
  },
  anyOf: [
    {
      required: [
        'copyPassport',
      ],
    },
    {
      required: [
        'addressPassport',
      ],
    },
    {
      required: [
        'selfiePassport',
      ],
    },
  ],
  additionalProperties: false,
});
