const typeSchema = ({
  type: 'object',
  properties: {
    id: {
      type: ['string', 'number'],
    },
    name: {
      type: 'string',
    },
    description: {
      type: 'string',
    },
  },
});

const docSchema = ({
  type: 'object',
  properties: {
    file: {
      type: 'object',
      properties: {
        type: {
          type: 'string',
          pattern: 'image.(?:jpe?g|png|gif)|application.pdf',
        },
      },
      required: ['type', 'size', 'name', 'path'],
    },
    requirement: typeSchema,
    type: typeSchema,
  },
});

export default ({
  type: 'object',
  properties: {
    documents: {
      type: 'object',
      properties: {
        face: docSchema,
        document: docSchema,
        address: docSchema,
      },
    },
    stakeholder: {
      type: 'string',
    },
    nonce: {
      type: 'string',
    },
    ettAppId: {
      type: 'string',
    },
  },
  required: ['documents', 'nonce', 'ettAppId'],
  additionalProperties: false,
});
