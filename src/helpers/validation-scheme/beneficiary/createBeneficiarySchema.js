export default ({
  type: 'object',
  properties: {
    type: {
      type: 'string',
      enum: ['person', 'company'],
    },
    name: {
      type: 'string',
      trim: true,
      minLength: 1,
    },
    street: {
      type: 'string',
      trim: true,
    },
    email: {
      type: 'string',
      trim: true,
    },
    phone: {
      type: 'string',
      trim: true,
    },
    bankName: {
      type: 'string',
      trim: true,
      // minLength: 1,
    },
    bankAddress: {
      type: 'string',
      trim: true,
    },
    country: {
      type: 'string',
      trim: true,
      minLength: 2,
      maxLength: 2,
    },
    currencyCode: {
      type: 'string',
      trim: true,
      minLength: 3,
      maxLength: 3,
    },
    accountNumber: {
      type: 'string',
      trim: true,
      maxLength: 34,
    },
    iban: {
      type: 'string',
      trim: true,
      maxLength: 34,
    },
    bic: {
      type: 'string',
      trim: true,
      maxLength: 11,
    },
    sortCode: {
      type: 'string',
      trim: true,
      maxlength: 6,
    },
    aba: {
      type: 'string',
      trim: true,
      maxlength: 9,
    },
    reference: {
      type: 'string',
      trim: true,
    },
    accountType: {
      type: 'string',
      trim: true,
      maxlength: 1,
    },
    isLocal: {
      type: 'boolean',
    },
    isCorrectVerify: {
      type: 'boolean',
    },
  },
  required: [
    'currencyCode',
    'country',
    'type',
    'name',
  ],
  additionalProperties: false,
});
