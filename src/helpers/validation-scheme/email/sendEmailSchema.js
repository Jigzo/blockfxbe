export default ({
  type: 'object',
  properties: {
    templateName: {
      type: 'string',
      enum: ['transaction-details'],
    },
    subject: {
      type: 'string',
    },
    params: {
      type: 'object',
    },
  },
  required: [
    'templateName',
  ],
  additionalProperties: false,
});
