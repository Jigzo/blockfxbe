/* eslint-disable camelcase */
import clickSend from '../../core/clickSend';
import sendMail from '../sendMail';
import { ettClientAuthToken } from '../../client/ett.client';
import { shuftiproClient } from '../../client/shuftipro.client';


const prefix = '/beneficiary';
export const verificationBeneficiary = async (request) => {
  const { id } = request.params;
  await ettClientAuthToken(request).get(`${prefix}/${id}`);
  const {
    phone,
    email,
    country,
    document = {},
  } = request.body;

  if (!phone && !email) throw new Error('Email or Phone number is required!');

  const {
    name = '',
    dob = '',
    issue_date = '',
    expiry_date = '',
    document_number = '',
    proof = null,
  } = document;

  const { language = 'EN' } = request.query;

  const payloadData = {
    reference: '12345694',
    verification_mode: 'any',
    language,
    country,
    document: {
      supported_types: ['id_card', 'passport'],
      name,
      dob,
      issue_date,
      expiry_date,
      document_number,
      proof,
    },
  };

  const { data } = await shuftiproClient.post('/', payloadData);
  const verificationUrl = new URL(data.verification_url);

  if (email) {
    sendMail({
      to: email,
      subject: 'Verification account',
      templateName: 'shuftipro-beneficiary',
      params: {
        appUrl: '',
        verificationUrl: verificationUrl.href,
      },
    });
  }

  if (phone) {
    let body = 'Please open on the link below to verify your identity on ShuftiPro:\n\n';
    body += 'If you have not submitted a request for "blocFX" please ignore this message.\n\n\n';
    body += verificationUrl.href;
    body += '\n\n\nThank you for your continued loyalty.';
    body += '\n\nBlocFX.';
    clickSend.sendSms(
      body,
      phone,
    );
  }
};
