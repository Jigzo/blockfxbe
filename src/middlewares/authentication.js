import {
  errorResponse, errorMessages,
} from '../helpers';
import auth from '../core/auth';


export const authentication = async (req, res, next) => {
  const token = req.headers['auth-token'];
  if (!token) {
    return errorResponse(res, { error: errorMessages.emptyToken, action: 'auth-token', status: 401 });
  }
  try {
    const { customer, ettUserTokenId } = auth.decodeToken(token);
    // req.ettToken = await auth.getEttToken(ettUserTokenId);
    // req.customer = customer;
    // req.ettUserTokenId = ettUserTokenId;

    req.ettToken = await auth.getEttToken(ettUserTokenId);
    req.customer = customer;
    req.ettUserTokenId = ettUserTokenId;
    return next();
  } catch (e) {
    return errorResponse(res, { error: errorMessages.invalidToken, action: 'auth-token', status: 403 });
  }
};
