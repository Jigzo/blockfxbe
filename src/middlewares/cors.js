import cors from 'cors';
// import { getEnv, isProduction } from '../helpers/config';


// const whitelist = getEnv('CORS_WHITE_LIST').split(',');

const options = {
  origin(origin, callback) {
    callback(null, true);
  },
  optionsSuccessStatus: 204,
};

export const makeCors = () => cors(options);
