// Core
import os from 'os';
import { IncomingForm } from 'formidable';
import _ from 'lodash';


export const formDataProcessing = ({
  multiple = false,
  uploadDir = os.tmpdir(),
  keepExtensions = true,
  maxFileSize = 5 * 1024 * 1024, // 5mb
  maxFieldSize = 10 * 1024, // 10kb
}) => (req, res, next) => {
  const form = new IncomingForm({
    multiple, uploadDir, maxFileSize, maxFieldSize, keepExtensions,
  });

  form.parse(req, (error, fields, files) => {
    if (error) return next(error);
    const val = { ...fields, ...files };

    Object.keys(val).forEach((key) => {
      _.set(req.body, key, val[key]);
    });

    return next();
  });
};
