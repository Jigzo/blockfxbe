import { Beneficiary } from '../../models';
import { ettClientAuthToken } from '../../client/ett.client';


const notFoundMessage = 'Beneficiary not found!';
const beneficiaryMiddleware = async (request, response, next) => {
  const { beneficiaryID } = request.params;
  if (/[^0-9]/.test(beneficiaryID)) throw new Error('invalid beneficiary id');

  let beneficiary = {};
  try {
    const { beneficiaries } = await ettClientAuthToken(request).get(`beneficiary/${beneficiaryID}`);
    if (!beneficiaries.length) throw new Error(notFoundMessage);
    [beneficiary] = beneficiaries;
  } catch (error) {
    if (error.status >= 400 && error.status < 500) throw new Error(notFoundMessage);
    else throw error;
  }

  const foundBeneficiary = await Beneficiary.findOne({ ettId: beneficiary.id }).lean();
  if (!request.meta) request.meta = {};
  request.meta.beneficiary = {
    ...beneficiary,
    ...foundBeneficiary,
  };

  return next();
};

export default beneficiaryMiddleware;
