import { PaymentAccount } from '../../models';
import { ettClientAuthToken } from '../../client/ett.client';


const notFoundMessage = 'Account not found!';
const paymentAccountMiddleware = async (request, response, next) => {
  const { accountId } = request.params;
  if (/[^0-9]/.test(accountId)) throw new Error('invalid account id');

  let account = {};
  try {
    const { accounts } = await ettClientAuthToken(request).get(`account/${accountId}`);
    if (!accounts.length) throw new Error(notFoundMessage);
    [account] = accounts;
  } catch (error) {
    if (error.status >= 400 && error.status < 500) throw new Error(notFoundMessage);
    else throw error;
  }

  const foundAccount = await PaymentAccount.findOne({ accountNumber: accountId }).lean();
  if (!request.meta) request.meta = {};
  request.meta.account = {
    ...account,
    ...foundAccount,
  };

  return next();
};

export default paymentAccountMiddleware;
