import { Transfer } from '../../models';


const transferMiddleware = async (request, response, next) => {
  const { transferId } = request.params;
  const transfer = await Transfer.findById(transferId).populate('quote');
  if (!request.meta) request.meta = {};
  request.meta.transfer = transfer;

  return next();
};

export default transferMiddleware;
