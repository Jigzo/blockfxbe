import {
  errorResponse, errorMessages,
} from '../helpers';
import { validateSignature } from '../client/shuftipro.client';


export default async (req, res, next) => {
  const data = req.body;
  const { signature } = req.headers;
  if (validateSignature({ data, signature })) return next();
  return errorResponse(res, { error: errorMessages.shuftiProSignature, status: 403 });
};
