// Core
import { createTerminus as create } from '@godaddy/terminus';


const getOptions = (server) => ({
  signal: 'SIGINT',
  onSignal() {
    return Promise.all([server.close()]);
  },
});

export const createTerminus = (server) => {
  const options = getOptions(server);
  return create(server, options);
};
