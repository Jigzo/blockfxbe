// @ts-check
import { Schema, model } from 'mongoose';
import regexp from '../../helpers/regexp';


const BeneficiarySchema = new Schema({
  ettId: { // UNIQUE ID INTO BACK OFFICE
    type: Number,
    unique: true,
  },
  type: { // Type of external beneficiary. ‘person’ or ‘company’
    type: String,
    enum: ['person', 'company'],
  },
  name: { // Last name or company name of external beneficiary
    type: String,
    validate: {
      validator: (name) => !/[1-9|!@#$%^&*()_\-+=;:<>,?`~"'№]/.test(name),
    },
  },
  firstname: { // Name of external beneficiary
    type: String,
    validate: {
      validator: (firstName) => !/[1-9|!@#$%^&*()_\-+=;:<>,?`~"'№]/.test(firstName),
    },
  },
  dob: { // Date of birth of external beneficiary
    type: Date,
  },
  nationality: { // Nationality (iso3166 code) of external beneficiary
    type: String,
    match: regexp.countryCode,
  },
  city: { // City of residence of external beneficiary
    type: String,
    validate: {
      validator: (city) => !/[|!@$%^&*=;?~]/.test(city),
    },
  },
  idNumber: { // Social security (person) or company registration (company) number
    type: String,
  },
  iban: { // If the user wants to provide both a local account number and an iban, both fields can be used
    type: String,
    match: regexp.iban,
    max: 34,
  },
  accountNumber: { // Account number of beneficiary. This can be an IBAN or other account number
    type: String,
    match: regexp.accountNumber,
    max: 34,
  },
  accountNumberPrefix: {
    type: String,
  },
  accountNumberSuffix: {
    type: String,
  },
  //  *** BIC code, sort code, aba routing number or bank code of beneficiary bank
  bic: {
    type: String,
    match: regexp.bic,
    max: 11,
  },
  sortCode: { // only local [?ONLY GB]
    type: String,
    match: regexp.sortCode,
    max: 6,
  },
  aba: { // only local [?ONLY US]
    type: String,
    match: regexp.aba,
    max: 9,
  },
  // *** BIC code ***
  postcode: { // Postal code of external beneficiary
    type: String,
  },
  state: { // 2-character abbreviation of US state of residence
    type: String,
  },
  bankName: { // Name of beneficiary’s bank
    type: String,
  },
  bankStateCode: { // State code of the bank (only for North America)
    type: String,
  },
  bankCountry: { // 2-character iso3166 country code of the bank, lower case
    type: String,
    match: regexp.countryCode,
  },
  bankKey: { // Bank Key (only needed for local routing in certain countries)
    type: String,
  },
  holding: { // Holding branch (only needed for local routing in certain countries)
    type: String,
  },
  branchCode: { // Branch code (only needed for local routing in certain countries)
    type: String,
  },
  accountType: { // 1 = Savings, 2 = Current (only needed for local routing in certain countries)
    type: String,
    enum: ['1', '2'],
  },
  bankAddress: { // Address of beneficiary’s bank
    type: String,
    validate: {
      validator: (bankAddress) => !/[|!@$%^&*=;?~]/.test(bankAddress),
    },
  },
  country: { // Country of residence (iso3166) of external beneficiary
    type: String,
    match: regexp.countryCode,
  },
  street: { // Street address of external beneficiary
    type: String,
    validate: {
      validator: (street) => !/[|!@$%^&*=;?~]/.test(street),
    },
  },

  // 3-character iso code for currency. Only useful if the
  // beneficiary account is known to have a different
  // currency than the default for the bank country.
  currencyCode: {
    type: String,
    match: regexp.currencyCode,
  },
  sepa: { // Indicate whether the payment should be sent through SEPA
    type: Boolean,
  },
  email: { // Email address of beneficiary
    type: String,
    match: regexp.email,
  },
  phone: { // International phone number
    type: String,
    match: regexp.phone,
    max: 17,
  },
  reference: {
    type: String,
  },

  // DOCUMENT VERIFY
  isCorrectVerify: {
    type: Boolean,
  },
  expiryDate: {
    type: Date,
  },
  issueDate: {
    type: Date,
  },
  documentNumber: {
    type: String,
  },
  verifyID: {
    type: Number,
  },
});


export default model('beneficiary', BeneficiarySchema);


