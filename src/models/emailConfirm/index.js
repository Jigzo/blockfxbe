import { Schema, model } from 'mongoose';
import regexp from '../../helpers/regexp';


const EmailConfirmSchema = new Schema({
  id: {
    type: Number,
    unique: true,
    required: true,
  },
  email: {
    type: String,
    required: true,
    match: regexp.email,
  },
  code: {
    type: String,
    required: true,
  },
}, { timestamps: true });

export default model('email-confirm', EmailConfirmSchema);
