// @ts-check

import { Schema, model } from 'mongoose';
import regexp from '../../helpers/regexp';


const CustomerSchema = new Schema({
  ettAppId: {
    type: String,
    required: true,
    unique: true,
  },
  firstName: {
    type: String,
    required: true,
    validate: {
      validator: (firstName) => !/[1-9|!@#$%^&*()_\-+=;:<>,?`~"'№]/.test(firstName),
    },
    trim: true,
  },
  lastName: {
    type: String,
    required: true,
    validate: {
      validator: (lastName) => !/[1-9|!@#$%^&*()_\-+=;:<>,?`~"'№]/.test(lastName),
    },
    trim: true,
  },
  email: {
    type: String,
    required: true,
    match: regexp.email,
    trim: true,
    min: 5,
  },
  phone: {
    type: String,
    required: true,
    match: regexp.phone,
    trim: true,
    max: 17,
    min: 10,
  },
  country: {
    type: String,
    required: true,
    match: regexp.countryCode,
    trim: true,
    max: 2,
  },
  roles: {
    type: [{
      type: Object,
      ref: 'role',
    }],
    default: [{ role: 'signatory' }],
  },
}, { timestamps: true });

class Customer {
  static findByEttAppId(ettAppId) {
    // @ts-ignore
    return this.findOne({ ettAppId });
  }
}

CustomerSchema.loadClass(Customer);

export default model('customer', CustomerSchema);
