import { Schema, model } from 'mongoose';
import deleteFileServer from '../../helpers/utils/deleteFileServer';
import { ettClient } from '../../client/ett.client';
import { convertToBase64 } from '../../helpers/utils';


const TypeSchema = {
  id: {
    type: Number,
  },
  name: {
    type: String,
  },
  description: {
    type: String,
  },
};

const CustomerDocumentSchema = new Schema({
  ettId: {
    type: Number,
  },
  nonce: {
    type: String,
  },
  ettAppId: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    // enum: ['approved', 'rejected', 'pending', 'submitted'],
    default: 'pending',
  },
  path: {
    type: String,
  },
  ettTypeData: {
    type: TypeSchema,
  },
  ettRequirementData: {
    type: TypeSchema,
  },
  reference: {
    type: String,
    default: null,
  },
  stakeholder: {
    type: Number,
    default: null,
  },
  comment: {
    type: String,
    default: null,
  },
}, { timestamps: true });

class CustomerDocument {
  static findByEttAppId(ettAppId) {
    return this.findOne({ ettAppId });
  }
}


CustomerDocumentSchema.loadClass(CustomerDocument);

CustomerDocumentSchema.post('remove', { document: true }, (doc) => {
  deleteFileServer(doc.path);
});


CustomerDocumentSchema.post('save', { document: true }, async (doc, next) => {
  if (doc.status === 'accepted' && !doc.ettId) {
    await ettClient.post('/application/document', {
      nonce: doc.nonce,
      name: doc.name,
      stakeholder: doc.stakeholder,
      reference: doc._id,
      data: convertToBase64(doc.path),
      requirement: doc.ettRequirementData.id,
      type: doc.ettTypeData.id,
    });
  }

  next();
});

export default model('customer-document', CustomerDocumentSchema);
