import { Schema, model, ObjectId } from 'mongoose';


const StakeholderDocumentDataSchema = new Schema({
  stakeholder: {
    type: Number,
    required: true,
  },
  nonce: {
    type: String,
    required: true,
    unique: true,
  },
  stakeholderDocumentFiles: [
    { type: ObjectId, ref: 'stakeholder-document-file' },
  ],
}, { timestamps: true });


export default model('stakeholder-document-data', StakeholderDocumentDataSchema);
