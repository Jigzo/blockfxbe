import { Schema, model, ObjectId } from 'mongoose';


const StakeholderDocumentFileSchema = new Schema({
  stakeholderDocumentData: {
    type: ObjectId,
    required: true,
    ref: 'stakeholder-document-data',
  },
  requirement: {
    type: Number,
    required: true,
  },
  type: {
    type: Number,
    required: true,
  },
  mode: {
    type: String,
  },
  fileUrl: {
    type: String,
    required: true,
    unique: true,
  },
}, { timestamps: true });


export default model('stakeholder-document-file', StakeholderDocumentFileSchema);
