import { Schema, model } from 'mongoose';


const EttUserTokenSchema = new Schema({
  ettAppId: {
    type: String,
    required: true,
  },
  ettCustomerId: {
    type: Number,
  },
  ettToken: {
    type: String,
    default: null,
  },
  confirm: {
    type: Boolean,
    default: false,
  },
  type: {
    type: String,
    enum: ['auth'],
    default: 'auth',
  },
}, {
  timestamps: true,
});

class EttUserTokenClass {
  static findByEttId(ettId) {
    return this.findOne({ ettId });
  }
}

EttUserTokenSchema.loadClass(EttUserTokenClass);

export default model('ett-user-tokens', EttUserTokenSchema);
