import { Schema, model } from 'mongoose';


const NotificationSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  date: {
    type: Number,
    required: true,
  },
  body: {
    type: String,
    required: true,
  },
  uId: {
    type: Number,
    required: true,
  },
});

export default model('notification', NotificationSchema);


