import { Schema, model } from 'mongoose';
import regexp from '../../helpers/regexp';


const AccountFundingSchema = new Schema({
  name: {
    type: String,
    validate: {
      validator: (name) => !/[1-9|!@#$%^&*()_\-+=;:<>,.?`~"'№]/.test(name),
    },
    trim: true,
  },
  address: {
    type: String,
    trim: true,
  },
  iban: {
    type: String,
    max: 34,
    match: regexp.iban,
    trim: true,
  },
  accountNumber: {
    type: String,
    max: 34,
    match: regexp.accountNumber,
  },
  bankName: {
    type: String,
  },
  bankAddress: {
    type: String,
  },
  bic: {
    type: String,
    match: regexp.bic,
    max: 11,
    trim: true,
  },
  sortCode: {
    type: String,
    match: regexp.sortCode,
    max: 6,
    trim: true,
  },
  aba: {
    type: String,
    match: regexp.aba,
    max: 9,
    trim: true,
  },
  bankCode: {
    type: String,
  },
  bankKey: {
    type: String,
  },
  bankCountry: {
    type: String,
    trim: true,
    match: regexp.countryCode,
  },
  instructions: {
    type: String,
  },
  reference: {
    type: String,
  },
});

const PaymentAccountSchema = new Schema({
  reference: {
    type: String,
  },
  accountNumber: {
    type: String,
    trim: true,
    required: true,
  },
  name: {
    type: String,
    validate: {
      validator: (name) => !/[^\w| ]/.test(name),
    },
    trim: true,
  },
  currency: {
    type: String,
    required: true,
    match: regexp.currencyCode,
    max: 3,
    min: 3,
    trim: true,
  },
  accountHolder: {
    type: Object,
  },
  funding: {
    type: [{
      type: AccountFundingSchema,
      ref: 'funding',
    }],
    required: true,
  },
});

export default model('payment-account', PaymentAccountSchema);


