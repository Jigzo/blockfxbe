import { Schema, model } from 'mongoose';
import regexp from '../../helpers/regexp';


const PhoneConfirmSchema = new Schema({
  id: {
    type: Number,
    unique: true,
    required: true,
  },
  phone: {
    type: String,
    required: true,
    match: regexp.phone,
  },
  code: {
    type: String,
    required: true,
  },
}, { timestamps: true });

export default model('phone-confirm', PhoneConfirmSchema);
