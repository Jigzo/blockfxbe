import { Schema, model } from 'mongoose';
import regexp from '../../helpers/regexp';


const amountType = {
  type: Number,
  required: true,
  min: 0,
};

const currencyType = {
  type: String,
  required: true,
  match: regexp.currencyCode,
};

const QuoteSchema = new Schema({
  id: {
    type: Number,
    unique: true,
    required: true,
  },
  sellcurrency: currencyType,
  buycurrency: currencyType,
  sellamount: amountType,
  buyamount: amountType,
  time: {
    type: Number,
  },
}, { timestamps: true });

export default model('quote', QuoteSchema);
