import { Schema, model } from 'mongoose';


const ShuftiProVerificationSchema = new Schema({
  reference: {
    type: String,
    required: true,
    unique: true,
  },
  data: {
    type: Object,
    default: {},
  },
  status: {
    type: String,
    default: 'pending',
  },
});

export default model('shuftipro-verification', ShuftiProVerificationSchema);
