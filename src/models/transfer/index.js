import { Schema, model, ObjectId } from 'mongoose';
import regexp from '../../helpers/regexp';


const ReasonOptionSchema = new Schema({
  id: {
    type: Number,
  },
  name: {
    type: String,
  },
  description: {
    type: String,
  },
});

const ReasonSchema = new Schema({
  id: {
    type: Number,
  },
  name: {
    type: String,
  },
  description: {
    type: String,
  },
  options: {
    type: [{
      type: ReasonOptionSchema,
      ref: 'options',
    }],
  },
});

const RuleSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  description: {
    type: String,
  },
  reason: {
    type: ReasonSchema,
    ref: 'reason',
  },
});

// const CounterpartSchema = new Schema({
//   id: {
//     type: Number,
//     required: true,
//   },
//   firstName: {
//     type: String,
//   },
//   name: {
//     type: String,
//   },
//   street: {
//     type: String,
//   },
//   city: {
//     type: String,
//   },
//   postCode: {
//     type: String,
//   },
//   country: {
//     type: String,
//     match: regexp.countryCode,
//   },
//   dob: {
//     type: String,
//   },
//   nationality: {
//     type: String,
//     match: regexp.countryCode,
//   },
//   accountName: {
//     type: String,
//   },
//   bankName: {
//     type: String,
//   },
//   bankAddress: {
//     type: String,
//   },
//   bankCountry: {
//     type: String,
//     match: regexp.countryCode,
//   },
//   accountNumber: {
//     type: String,
//     match: regexp.accountNumber,
//   },
//   bic: { // ! INVALID STRUCTURE DATA ETT
//     type: String,
//     validate: {
//       validator: (bic) => regexp.bic.test(bic)
//         || regexp.sortCode.test(bic)
//         || regexp.aba.test(bic),
//     },
//   },
//   iban: {
//     type: String,
//     match: regexp.iban,
//   },
//   currencyCode: {
//     type: String,
//     match: regexp.currencyCode,
//   },
//   displayAccountNo: {
//     type: String,
//     match: regexp.accountNumber,
//   },
//   type: {
//     type: String,
//   },
// });

const TransferSchema = new Schema({
  id: {
    type: Number,
  },
  accountNumber: {
    type: String,
  },
  counterpart: {
    type: Object,
  },
  requestamount: {
    type: Number,
  },
  requestcurrency: {
    type: String,
    match: regexp.currencyCode,
  },
  narrative: {
    type: String,
  },
  status: {
    type: String,
    enum: ['cache', 'pending', 'rejected', 'approved'],
  },
  date: {
    type: String,
    validate: {
      validator: (date) => +(new Date(date)) < Date.now(),
    },
    match: /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/,
  },
  time: {
    type: Number,
    validate: (date) => (date * 1000) < Date.now(),
  },
  type: {
    type: String,
  },
  rule: {
    type: RuleSchema,
    ref: 'rule',
  },
  queue: {
    type: String,
  },
  quote: {
    type: ObjectId,
    ref: 'quote',
  },
}, { timestamps: true });

export default model('transfer', TransferSchema);
