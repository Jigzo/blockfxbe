import Router from 'express-promise-router';
import multer from 'multer';
import { paymentAccountController, transferController } from '../controllers';
import {
  createAccountSchema,
} from '../helpers/validation-scheme';

import {
  authentication,
  validator,
} from '../middlewares';

import {
  paymentAccountMiddleware,
  transferMiddleware,
  beneficiaryMiddleware,
} from '../middlewares/meta';


const accountRouter = Router();

const validateTransferFacade = [
  paymentAccountMiddleware,
  transferMiddleware,
];

accountRouter
  // account
  .post('/', [
    authentication,
    validator(createAccountSchema),
  ], paymentAccountController.create)

  .get('/', [
    authentication,
  ], paymentAccountController.index)

  .get('/history-transfer', [
    authentication,
  ], transferController.historyTransfer)

  .get('/:accountId', [
    paymentAccountMiddleware,
  ], paymentAccountController.show)

  .patch('/:accountId', [
    paymentAccountMiddleware,
  ], paymentAccountController.update)

  // *** transfer *** //
  .post('/:accountId/transfer/:beneficiaryID', [
    paymentAccountMiddleware,
    beneficiaryMiddleware,
  ], transferController.createTransfer)

  .get('/:accountId/transfer/:transferId', [
    ...validateTransferFacade,
  ], transferController.showTransfer)

  // transfer document
  .post('/:accountId/transfer/:transferId/confirm', [
    ...validateTransferFacade,
  ], transferController.setReason)

  .post('/:accountId/transfer/:transferId/document', [
    ...validateTransferFacade,
    multer().single('document'),
  ], transferController.attachDocument)

  .post('/:accountId/transfer/:transferId/submit', [
    ...validateTransferFacade,
  ], transferController.submitDocument);
// *** transfer *** //

export default accountRouter;
