// @ts-check

import Router from 'express-promise-router';
// Controllers
import { apply } from '../controllers';
import { authentication } from '../middlewares';


const router = Router();
router
  .get('/validate', [authentication], apply.findBankDetails)
  .get('/code', [authentication], apply.code);


export default router;
