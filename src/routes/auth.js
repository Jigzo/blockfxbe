// @ts-check

import Router from 'express-promise-router';
import multer from 'multer';

import {
  loginScheme,
  passwordResetSchema,
  passwordChangeSchema,
  passwordConfirmSchema,
  verificationCustomerSchema,
  createCustomerSchema,
  verificationConfirmSchema,
  checkEmailIsExistScheme,
} from '../helpers/validation-scheme';
import { auth } from '../controllers';
import { authentication, formDataProcessing, validator } from '../middlewares';


const authRouter = Router();

authRouter
  .post('/login', [validator(loginScheme)], auth.login)
  .get('/logout', [authentication], auth.logout)
  .post('/create-customer', [validator(createCustomerSchema)], auth.createCustomer)
  .post('/verification-confirm', [validator(verificationConfirmSchema)],
    auth.verificationConfirm)
  .post('/verification-customer', [formDataProcessing({
    maxFileSize: 16777216,
  }), validator(verificationCustomerSchema)], auth.verificationCustomer)

  // get info data
  .get('/me', [authentication], auth.me)
  .get('/customer', [authentication], auth.customer)

  // password
  .post('/password-reset', [validator(passwordResetSchema)], auth.passwordReset)
  .post('/password-change', [validator(passwordChangeSchema)], auth.passwordChange)
  .post('/password-confirm', [validator(passwordConfirmSchema)], auth.passwordConfirm)

  .post('/check-email-is-exist', [validator(checkEmailIsExistScheme)], auth.checkEmailIsExist)

  // document
  .delete('/document/:nonce/:id', auth.deleteDocument)
  .post('/upload-document', multer().single('document'), auth.uploadDocument)
  .post('/verification-document', auth.verificationDocument)

  // changeData
  .put('/phone-change', [authentication], auth.phoneChange)
  .get('/phone-confirm', [authentication], auth.phoneConfirm)
  .put('/email-change', [authentication], auth.emailChange)
  .get('/email-confirm', [authentication], auth.emailConfirm);

export default authRouter;
