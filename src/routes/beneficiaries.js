// @ts-check
import Router from 'express-promise-router';
import multer from 'multer';

// Validation Schemas
import { createBeneficiarySchema } from '../helpers/validation-scheme';

// Controllers
import { beneficiary } from '../controllers';
import {
  authentication, validator,
} from '../middlewares';


const beneficiaryRouter = Router();
beneficiaryRouter
  .get('/', [authentication], beneficiary.index)
  .get('/:id', [authentication], beneficiary.show)
  .post('/', [authentication, validator(createBeneficiarySchema)], beneficiary.create)
  .patch('/:id', [authentication], beneficiary.update)
  .delete('/:id', [authentication], beneficiary.delete)

  // verify
  .use('/:id/document', multer().any())
  .post('/:id/document', beneficiary.verificationDocument)
  .put('/:id/verify', beneficiary.verificationBeneficiary)
  .post('/:id/sendVerificationLink', [authentication], beneficiary.sendVerificationLink);

export default beneficiaryRouter;
