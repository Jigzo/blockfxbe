// @ts-check

import Router from 'express-promise-router';
// Controllers
import { currencyCloud } from '../controllers';
import { authentication } from '../middlewares';


const router = Router();
router
  .get('/bank-details', [authentication], currencyCloud.findBankDetails);


export default router;
