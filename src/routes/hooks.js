// @ts-check

import Router from 'express-promise-router';

// Controllers
import { hooks } from '../controllers';


const router = Router();
router
  .use('/external', hooks.external)
  .use('/internal', hooks.internal)
  .use('/internal/:id', hooks.internal)
  .use('/deposit', hooks.deposit)
  .use('/deposit/:id', hooks.internal)
  .use('/message', hooks.zendeskMessage);


export default router;
