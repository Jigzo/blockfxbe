// Core
import PromiseRouter from 'express-promise-router';

// Routes
import authRouter from './auth';
import accountRouter from './accounts';
import beneficiaryRouter from './beneficiaries';
import hooks from './hooks';
import apply from './apply';
import currencyCloud from './currencyCloud';
import notificationRouter from './notification';
import quoteRouter from './quote';

// Instruments
import {
  errorResponse, APIError, errorMessages,
} from '../helpers';

// MODELS
import { sendEmailSchema } from '../helpers/validation-scheme';

// Controllers
import {
  email,
  shuftiProCallback,
  backOffice,
} from '../controllers';
import {
  authentication, shuftiProSignature, validator,
} from '../middlewares';


const Router = PromiseRouter();

// Registration routs
Router
  .use('/auth', authRouter)
  .use('/accounts', [authentication], accountRouter)
  .use('/beneficiaries', beneficiaryRouter)
  .use('/notification', [authentication], notificationRouter)
  .use('/quote', [authentication], quoteRouter)
  .use('/apply', [authentication], apply)
  .use('/currency-cloud', [authentication], currencyCloud)
  .use('/hooks', hooks);

// Email
Router.post('/send-mail', [authentication, validator(sendEmailSchema)], email.send);

// Back office
Router.get('/fees', [authentication], backOffice.fees)
  .get('/fields', [authentication], backOffice.fields)
  .get('/purposes', [authentication], backOffice.purposes);

// ShuftiProCallback
Router.post('/shuftipro-callback/verification-customer', [shuftiProSignature],
  shuftiProCallback.verificationCustomer);

/* Service */
Router
  .use('/ping', (req, res) => res.send(`PONG (${req.method}) : ${Date.now()}`))
  .use((req, res, next) => next(new APIError(`${req.url} - Not Found`, 404)));

/* Error handler */
// eslint-disable-next-line no-unused-vars
Router.use((err, req, res, next) => {
  switch (err.name) {
    default: {
      if (req.ettToken && err?.response?.status === 403) {
        return errorResponse(res, { error: errorMessages.invalidToken, action: 'auth-token', status: 403 });
      }
      return errorResponse(res, err);
    }
  }
});

export default Router;
