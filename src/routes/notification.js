// @ts-check
import Router from 'express-promise-router';

// Controllers
import { notification } from '../controllers';


const notificationRouter = Router();
notificationRouter
  .get('/', notification.getAll)
  .post('/', notification.save)
  .delete('/', notification.delete)
  .delete('/:id', notification.delete);

export default notificationRouter;
