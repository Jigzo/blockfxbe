import Router from 'express-promise-router';
import QuoteController from '../controllers/quote';
import { Quote } from '../models';
import QuoteService from '../services/quote';


const quoteService = new QuoteService(Quote);
const quoteController = new QuoteController(quoteService);


const quoteRouter = Router();

quoteRouter
  .post('/', quoteController.create)
  .get('/:quoteId', quoteController.findById);

export default quoteRouter;
