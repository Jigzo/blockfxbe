/* eslint-disable no-console */

// Vanilla Node modules
import fs from 'fs';
import path from 'path';
import http from 'http';

// Dependencies module
import express from 'express';
import helmet from 'helmet';
import compression from 'compression';
import logger from 'morgan';

// Instruments
import { limiter, makeCors } from './middlewares';
import { errorResponse } from './helpers/response';
import { getEnv, connectDb, APIError } from './helpers';

// Project modules
import routes from './routes';
import socket from './ws';


const app = express();
const PORT = getEnv('PORT');


app
  .use(makeCors())
  .get('/', (request, response) => response.redirect(getEnv('FE_APP_URL')))
  .use(helmet())
  .use(compression({}))
  .use(logger('tiny', {
    stream: fs.createWriteStream(path.join(__dirname, '../logger.log'), { flags: 'a' }),
  }))
  .use(limiter)
  .use(express.json())
  .use(express.urlencoded({ extended: false }))
  .use('/uploads/', express.static('uploads'))
  .use('/api/v1', routes)
  .use((req, res) => errorResponse(res, new APIError(`${req.url} - Not Found`, 404), false));


(async () => {
  console.log('server try started');
  try {
    await connectDb();
    const server = http.createServer(app);
    socket(server);

    return server.listen(+PORT, (err) => {
      if (err) throw new Error(err.message || err || 'express server: Something went wrong!');
      console.log(`Server start on port ${PORT}`);
      process.on('SIGINT', () => process.exit());
      return false;
    });
  } catch (e) {
    console.error(e);
    return process.exit(1);
  }
})();
