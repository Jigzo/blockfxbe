import { purposeParser } from '../helpers/parser';


class BackOfficeService {
  constructor(client) {
    this.client = client;
    this.cacheFields = {};
    this.cachePurposes = [];
    this.cacheFees = [];
    this.loadingFees = null;
  }

  fees = async () => {
    const getFees = async () => {
      try {
        if (!this.loadingFess) {
          this.loadingFees = this.client.send({
            url: '/backofficev2/xml/fees_list.php?tariffid=0',
          });
        }

        const rawData = await this.loadingFees;

        const rawJson = JSON.parse(rawData.body);
        const formatFees = (data) => data.map((transfer) => ({
          id: transfer[0],
          description: transfer[2],
          currency: transfer[1],
          fee: +transfer[7] ?? 0,
          exception: transfer[5] === '-' ? null : transfer[5],
        }));
        const json = formatFees(rawJson.data);
        this.cacheFees = json;

        return this.cacheFees;
      } catch {
        return this.cacheFees;
      }
    };

    if (this.cacheFees.length) {
      getFees();
      return this.cacheFees;
    }

    return await getFees();
  }

  fields = async (countryCode = '') => {
    if (this.cacheFields[countryCode]) {
      return this.cacheFields[countryCode];
    }

    const { body } = await this.client.send({
      url: `/backofficev2/beneficiary_ext_form.php?countryCode=${countryCode}`,
    });

    const fieldsNoAccept = [
      'currencyCode',
      'bankName',
      'bankAddress',
      'bic',
    ];

    let fields = [];
    fields = body
      .match(/for="[a-zA-Z]+"/gi)
      .map((label) => label
        .replace(/for=|[^a-zA-Z]/g, '')
        .replace('swiftBic', 'bic'))
      .filter((field) => !fieldsNoAccept.includes(field));
    this.cacheFields[countryCode] = fields;

    return this.cacheFields[countryCode];
  }

  purposes = async () => {
    const getDataPurpose = async () => {
      try {
        const { body } = await this.client.send({
          url: '/backofficev2/compliance.php',
        });
        const data = purposeParser(body);
        this.cachePurposes = data;
        return this.cachePurposes;
      } catch {
        return this.cachePurposes;
      }
    };

    if (this.cachePurposes.length) {
      getDataPurpose();
      return this.cachePurposes;
    }

    return await getDataPurpose();
  }

  verificationBeneficiary = async (beneficiary, customerID) => {
    const {
      currencyCode = '',
      country = '',
      firstName = '',
      lastName = '',
      name = '',
      dob = '',
      iban = '',
      sortCode = '',
      accountNumber = '',
      bic = '',
      bankName = '',
      bankAddress = '',
    } = beneficiary;

    const formData = {
      identityType: beneficiary.type,
      firstName: firstName || name.split(' ')?.[0] || '',
      name: lastName || name.split(' ')?.[1] || name.split(' ')?.[0] || '',
      street: beneficiary.street || '',
      // city: 'London',
      // postCode: 'test',
      country,
      nationality: country,
      dob: new Date(dob).toISOString().split('T')[0],
      accountName: name,
      bankCountry: country,
      currencyCode,
      iban,
      bankName,
      bankAddress,
      accountNumber,
      swiftBic: bic,
      sortCode,
      originalCurrency: currencyCode,
      chkVerified: 'on',
      customerID,
      from: 'customer',
      id: beneficiary.ettId,
      transferType: 'E',
      accountID: customerID,
      customerStatus: 'Active',
    };

    return await this.client.send({
      method: 'POST',
      url: '/backofficev2/beneficiary_save.php',
      formData,
    });
  }
}

export default BackOfficeService;
