import { getLastMessageText, getMetaDataIntoZendesk } from '../helpers/parser/zendesk';
import { Notification } from '../models';
import socket from '../ws';


class HookService {
  async zendeskMessage(responseMessage = '') {
    const message = getLastMessageText(responseMessage);
    const subject = getMetaDataIntoZendesk(responseMessage, 'Subject');
    const customerId = getMetaDataIntoZendesk(responseMessage, 'Customer ID');
    const date = Date.now();

    const data = {
      title: subject,
      body: message,
      uId: +customerId,
      date,
    };

    if (Object.values(data).every((value) => !!value)) {
      socket().emit(data.uId)('notification')(data);
      return await Notification.create(data);
    }

    return null;
  }
}

export default HookService;
