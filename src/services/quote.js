/* eslint-disable no-useless-constructor */
import Service from '.';
// import { ettClientAuthToken } from '../client/ett.client';


class QuoteService extends Service {
  constructor(Model) {
    super(Model);
  }

  async create(request) {
    const {
      // direction = ['sell', 'buy'][0],
      // amount = 1,
      sellcurrency,
      buycurrency,
    } = request.body;

    // const { forexQuote } = await ettClientAuthToken(request).get('/forex/quote', {
    //   params: {
    //     direction,
    //     amount,
    //     sellcurrency,
    //     buycurrency,
    //   },
    // });


    // const newQuote = new this.Model(forexQuote);
    // await newQuote.validate();
    // await newQuote.save();

    // return forexQuote;

    return await this.Model.findOne({
      sellcurrency,
      buycurrency,
    });
  }

  async findById(id) {
    const quote = await this.Model.find({ id });
    return quote;
  }
}

export default QuoteService;
