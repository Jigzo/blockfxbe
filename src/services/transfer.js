/* eslint-disable no-useless-constructor */
import dayjs from 'dayjs';
import Service from '.';
import { ettClientAuthToken } from '../client/ett.client';


class TransferService extends Service {
  constructor(Model) {
    super(Model);
  }

  async createTransferEtt(request, transaction, accountNumber, forex) {
    const transactionPayload = {
      time: dayjs().unix(),
      currency: transaction.requestcurrency,
      beneficiaryid: transaction.counterpart.id,
      amount: transaction.requestamount,
      narrative: transaction.narrative,
    };

    const requestETT = async (params = transactionPayload) => await ettClientAuthToken(request)
      .post(`/account/${accountNumber ?? transaction.accountNumber}/transfer`, params);

    let ettRes = null;
    try {
      ettRes = await requestETT({ ...transactionPayload, forex: forex ?? transaction.quote.id });
    } catch (e) {
      ettRes = await requestETT(transactionPayload);
    }

    return ettRes;
  }

  async createTransfer(transfer) {
    const newTransfer = new this.Model(transfer);
    await newTransfer.validate();
    return await newTransfer.save();
  }

  async inspectAmountOrCreateTransferEtt(request, transfer = {}, account = {}) {
    if (transfer.status === 'cache' && account.available >= transfer.requestamount) {
      const ettRes = await this.createTransferEtt(request, transfer);

      const transferInspectAmount = {
        ...transfer,
        ...ettRes,
      };

      return await this.Model.updateOne({ id: transfer.id }, transferInspectAmount);
    }

    return transfer;
  }
}

export default TransferService;
