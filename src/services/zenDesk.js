import zenDeskClient from '../client/zenDesk.client';


const getLocalDate = () => new Date().toISOString().replace('T', ' ').substr(0, 19);
const formattedChangeMessage = (
  name, oldData, newData,
) => `
- Please change my ${name}: ${oldData} -
- To new ${name}: (${newData}) -
`;


class ZenDeskService {
  constructor() {
    this.subject = 'Service message from BlockFX: ';
  }

  setSubject(subjectName = 'message') {
    return this.subject + subjectName;
  }

  async message(requester, message, subjectName) {
    const slicingString = '\n****\n';
    const body = `
      - Open request   : ${getLocalDate()}  -
      - Subject        : ${subjectName}     -
      - Customer ID    : ${requester.id}    -
      - Customer Name  : ${requester.name}  -
      - Customer Email : ${requester.email} -
      - Customer Phone : ${requester.phone} -
      ${slicingString}
      ${message}
      ${slicingString}`;
    const payload = {
      request: {
        requester: {
          name: requester.name,
        },
        subject: this.setSubject(subjectName),
        comment: {
          body,
        },
      },
    };
    return await zenDeskClient.post('/requests', payload);
  }

  async change(requester, body, name) {
    return await this.message(requester, body, `Change ${name}`);
  }

  async changePhone(requester, phone) {
    const changeName = 'phone number';
    const body = formattedChangeMessage(changeName, requester.phone, phone);
    return await this.change(requester, body, changeName);
  }

  async changeEmail(requester, email) {
    const changeName = 'email address';
    const body = formattedChangeMessage(changeName, requester.email, email);
    return await this.change(requester, body, changeName);
  }
}

export default ZenDeskService;
