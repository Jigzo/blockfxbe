const WebSocket = require('ws');


const userConnects = {}; // initialize client connect
let socket = null; // initialize socket server

// PATTERN chain of responsibility
class Socket {
  constructor(server) {
    this.listeners = {}; // PATTERN OBSERVER
    this.wss = new WebSocket.Server({ server });
    this.wss.on('connection', (connect) => { // LISTEN connect ws server
      connect.on('message', (rawData) => { // LISTEN message from client
        const { event, data } = JSON.parse(rawData); // get data message
        switch (event) {
          case 'connect':
            // initialize connect client
            userConnects[data] = connect;
            // clear data client and connect if client disconnected
            userConnects[data].on('close', () => delete userConnects[data]);
            // send client status is connect
            this.emit(connect)('message')({
              title: 'external',
              id: 413,
              body: 'Confirmed',
            });
            return connect.send(JSON.stringify({
              event,
              data: true,
            }));
          default:
            // if no listener send error
            if (!this.listeners[event]) this.emit(connect)('error')({ message: `event (${event}): not found!` });
            // start method listener
            else this.listeners[event].forEach((method) => method(data, connect, event));
            return this;
        }
      });
    });

    /**
     * open listener
     * @param {Array<string>} events
     */
    this.listen = (...events) => (...methods) => {
      events.forEach((event) => {
        if (!this.listeners?.[event]?.length) this.listeners[event] = methods;
        else this.listeners[event].concat(methods);
      });
      return this;
    };

    // send event and data from clients
    /**
     * @param {Array<string>| Array<{}>} clients
     */
    this.emit = (...clients) => (event) => (data) => {
      const rawResponse = {
        event,
        data,
      };
      const response = JSON.stringify(rawResponse);
      if (clients.length) {
        clients.forEach((client) => {
          const foundClient = userConnects?.[client];
          foundClient?.send(response);
        });
      } else {
        userConnects.forEach((client) => client.send(response));
      }
      return this;
    };

    /**
     * remove prevent open listener
     *
     * @param {string} event
     * @param {Function} method
     */
    this.removeListen = (event, method) => {
      if (method) {
        this.listeners[event].filter((listen) => listen !== method); // if remove method into listeners
      } else {
        delete this.listeners[event]; // remove listeners
      }
      return this;
    };
  }
}

// SINGLETON
// if is open connection return connection else open connection
export default (server) => {
  if (!socket) socket = new Socket(server);
  return socket;
};
