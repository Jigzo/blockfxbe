const cancelable = (callback, ...args) => {
  let run = callback;
  const wrappedCancelable = (...next) => run(...args, ...next);
  wrappedCancelable.cancel = () => {
    run = () => null;
  };

  return wrappedCancelable;
};
