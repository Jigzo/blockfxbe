import { getRandomStringNumber } from "../src/helpers/generator";


test('generate digital code type', () => {
  const data = getRandomStringNumber(0, 999999);
  expect(typeof data).toBe('string');
});

test('generate digital code 6 characters', () => {
  const data = getRandomStringNumber(0, 999999);
  expect(data.length).toBe(6);
});

test('generate digital code 8 characters', () => {
  const data = getRandomStringNumber(0, 99999999);
  expect(data.length).toBe(8);
});

test('generate digital code 3 characters', () => {
  const data = getRandomStringNumber(0, 999);
  expect(data.length).toBe(3);
});
