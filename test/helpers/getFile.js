
import fs from 'fs/promises';
import path from 'path';
import process from 'process';
import { memoize } from 'lodash';

export const getPath = (pathUrl) => path.resolve(process.env.PWD + '/test', pathUrl);
export const getText = memoize(async (fileName) => await fs.readFile(getPath(`util/text/${fileName}.txt`), 'utf8'));
