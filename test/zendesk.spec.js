import { getText } from "./helpers/getFile";

import {
  getLastMessage,
  getLastMessageText,
  getMessageTextIntoMessage,
  getMetaDataIntoZendesk,
  getMetaDataString
} from "../src/helpers/parser/zendesk";

const messageTextIntoMessage = 'this last message';

test('parse message get string', async () => {
  const msg = await getText('zendeskMessage');
  const data = getMetaDataString(msg, 'Subject');
  expect(typeof data).toBe('string')
});

test('parse message get data', async () => {
  const msg = await getText('zendeskMessage');
  const data = getMetaDataIntoZendesk(msg, 'Subject');
  expect(typeof data).toBe('string');
  expect(data).toBe('Change email address');
});

test('parse last message', async () => {
  const msg = await getText('zendeskMessage');
  const data = getLastMessage(msg);
  expect(typeof data).toBe('string');
});

test('message text into message', () => {
  const message = `
    Denis Oriehov, Aug 19, 2021, 12:53

    ${messageTextIntoMessage}
  `;

  const messageIntoText = getMessageTextIntoMessage(message);
  expect(messageIntoText).toBe(messageTextIntoMessage);
});

test('get last messageText', async () => {
  const msg = await getText('zendeskMessage');
  const data = getLastMessageText(msg);
  expect(typeof data).toBe('string');
  expect(data).toBe(messageTextIntoMessage);
});
