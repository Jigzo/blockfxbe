// Core
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';


export const createBundleAnalyzer = () => ({
  plugins: [
    new BundleAnalyzerPlugin({
      analyzerMode: 'disabled',
      generateStatsFile: true,
      statsFilename: 'build-stats.json',
      openAnalyzer: false,
    }),
  ],
});
