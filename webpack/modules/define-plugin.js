/* eslint-disable import/no-extraneous-dependencies */
// Core
import { DefinePlugin } from 'webpack';
import dotenv from 'dotenv';


console.log(DefinePlugin);

export const createDefinePlugin = () => ({
  plugins: [
    new DefinePlugin({
      'process.env': JSON.stringify(dotenv.config().parsed),
    }),
  ],
});
