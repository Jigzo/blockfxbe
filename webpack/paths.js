// Core
import { resolve } from 'path';


export const entry = resolve(__dirname, '../src/server.js');
export const build = resolve(__dirname, '../build');
