// Core
import excludeNodeModules from 'webpack-node-externals';
// Instruments
import merge from 'webpack-merge';
import { createBundleAnalyzer, createDefinePlugin } from './modules';

// Presets
import { entry, build } from './paths';


export default merge(
  {
    mode: process.env.NODE_ENV || 'development',
    entry,
    target: 'node',
    output: {
      path: build,
      filename: 'index.js',
      chunkFilename: '[id].js',
    },
    externals: [excludeNodeModules()],
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
        },
      ],
    },
  },
  createBundleAnalyzer(),
  createDefinePlugin(),
);
